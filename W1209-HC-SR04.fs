\ The HC-SR04 "trig" pin is connected to the SET key input (PC3). 
\ The "echo" pin is connected to the same GPIO with a 1k resistor  
\ Uses TIM1 with 1µs clock -> 3.4mm resolution.

#require ]B!

\res MCU: STM8S103
\res export PC_DDR PC_ODR PC_CR1
\res export TIM1_PSCRH TIM1_SR1 TIM1_CCMR3 TIM1_CCMR4 TIM1_CCER2 TIM1_CR1
\res export TIM1_CCR3H TIM1_CCR4H

NVM
  
: T1Init (  -- )
   \ Init Timer1 with prescaler ( n=15 -> 1 MHz)
   15 TIM1_PSCRH 2C!
   \ CC3,CC4: pulse width measurement rising and falling edge
   $01 TIM1_CCMR3 C!       \ CC3S=TIM1_CCMR3.1-0=01
   $20 TIM1_CCER2 C!       \ x.1 CC3P=0, x.5 CC4P=1
   $02 TIM1_CCMR4 C!       \ CC4S=TIM1_CCMR4.1-0=10
   $31 TIM1_CCER2 C!       \ x.1 CC3P=0, x.5 CC4P=1, CC3E=1, CC4E=1
   1 TIM1_CR1 C!           \ enable TIM1
    
   [ 1 PC_ODR 3 ]B!        \ initialize PC3 output for "trig" pulse
   ;

: delay ( -- )   \ loop a little
   8 FOR NEXT
   ;

: bgtask ( -- )   \ background task 
   TIM 15 AND 0= IF
      \ 16*5ms = 80ms between measurments
      [ 1 PC_DDR 3 ]B!
      [ 1 PC_CR1 3 ]B!
      delay
      [ 0 PC_CR1 3 ]B!
      [ 0 PC_DDR 3 ]B!
      delay
      0 TIM1_SR1 C!        \ guard CC3 and CC4 
   ELSE   \ wait for CC3 and CC4 capture
      $18 TIM1_SR1 C@ OVER AND = IF
         TIM1_CCR4H 2C@ TIM1_CCR3H 2C@
         - 10 580 */ .     \ scale to cm and print
         0 TIM1_SR1 C!
     THEN
  THEN
  ;

: startup ( -- )
   T1Init
   [ ' bgtask ] LITERAL BG !
   ;

' startup 'BOOT !
RAM 

