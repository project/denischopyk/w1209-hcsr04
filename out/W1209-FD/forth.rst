                                      1 ; STM8EF for STM8S (Value line and Access Line devices)
                                      2 ;
                                      3 ; This is derived work based on
                                      4 ; http://www.forth.org/svfig/kk/07-2010.html
                                      5 ;
                                      6 ; Please refer to LICENSE.md for more information.
                                      7 ;
                                      8 ;--------------------------------------------------------
                                      9 ; Original author, and copyright:
                                     10 ;       STM8EF, Version 2.1, 13jul10cht
                                     11 ;       Copyright (c) 2000
                                     12 ;       Dr. C. H. Ting
                                     13 ;       156 14th Avenue
                                     14 ;       San Mateo, CA 94402
                                     15 ;       (650) 571-7639
                                     16 ;
                                     17 ; Original main description:
                                     18 ;       FORTH Virtual Machine:
                                     19 ;       Subroutine threaded model
                                     20 ;       SP Return stack pointer
                                     21 ;       X Data stack pointer
                                     22 ;       A,Y Scratch pad registers
                                     23 ;
                                     24 ;--------------------------------------------------------
                                     25 ; The latest version of this code is available at
                                     26 ; https://github.com/TG9541/stm8ef
                                     27 ;
                                     28 ;
                                     29 ; Docs for the SDCC integrated assembler are scarce, thus
                                     30 ; SDCC was used to write the skeleton for this file.
                                     31 ; However, the code in this file isn't SDCC code.
                                     32 ;--------------------------------------------------------
                                     33 ; File Created by SDCC : free open source ANSI-C Compiler
                                     34 ; Version 3.6.0 #9615 (Linux)
                                     35 ;--------------------------------------------------------
                                     36 
                                     37         .module forth
                                     38         .optsdcc -mstm8
                                     39 
                                     40 ;--------------------------------------------------------
                                     41 ; Public variables in this module
                                     42 ;--------------------------------------------------------
                                     43 
                                     44         .globl _TRAP_Handler
                                     45         .globl _EXTI0_IRQHandler
                                     46         .globl _EXTI1_IRQHandler
                                     47         .globl _EXTI2_IRQHandler
                                     48         .globl _EXTI3_IRQHandler
                                     49         .globl _TIM2_UO_IRQHandler
                                     50         .globl _TIM4_IRQHandler
                                     51         .globl _forth
                                     52 
                                     53 ;--------------------------------------------------------
                                     54 ; ram data
                                     55 ;--------------------------------------------------------
                                     56         .area DATA
                                     57 
                                     58 ;--------------------------------------------------------
                                     59 ; ram data
                                     60 ;--------------------------------------------------------
                                     61         .area INITIALIZED
                                     62 ;--------------------------------------------------------
                                     63 ; absolute external ram data
                                     64 ;--------------------------------------------------------
                                     65         .area DABS (ABS)
                                     66 ;--------------------------------------------------------
                                     67 ; global & static initialisations
                                     68 ;--------------------------------------------------------
                                     69         .area HOME
                                     70         .area GSINIT
                                     71         .area GSFINAL
                                     72         .area GSINIT
                                     73 ;--------------------------------------------------------
                                     74 ; Home
                                     75 ;--------------------------------------------------------
                                     76         .area HOME
                                     77         .area HOME
                                     78 ;--------------------------------------------------------
                                     79 ; code
                                     80 ;--------------------------------------------------------
                                     81         .area CODE
                                     82 
                                     83         ;************************************
                                     84         ;******  1) General Constants  ******
                                     85         ;************************************
                                     86 
                           00FFFF    87         TRUEE   =     0xFFFF    ; true flag
                           000040    88         COMPO   =     0x40      ; lexicon compile only bit
                           000080    89         IMEDD   =     0x80      ; lexicon immediate bit
                           001F7F    90         MASKK   =     0x1F7F    ; lexicon bit mask
                                     91 
                           000050    92         TIBLENGTH =   80        ; size of TIB (starting at TIBOFFS)
                           000050    93         PADOFFS =     80        ; offset text buffer above dictionary
                           000002    94         CELLL   =      2        ; size of a cell
                           00000A    95         BASEE   =     10        ; default radix
                           000008    96         BKSPP   =      8        ; backspace
                           00000A    97         LF      =     10        ; line feed
                           00000B    98         PACE    =     11        ; pace character for host handshake (ASCII VT)
                           00000D    99         CRR     =     13        ; carriage return
                           00001B   100         ERR     =     27        ; error escape
                           000027   101         TIC     =     39        ; tick
                                    102 
                           000081   103         EXIT_OPC =    0x81      ; RET opcode
                           000083   104         DOLIT_OPC =   0x83      ; TRAP opcode as DOLIT
                           0000AD   105         CALLR_OPC =   0xAD      ; CALLR opcode for relative addressing
                           0000CC   106         BRAN_OPC =    0xCC      ; JP opcode
                           0000CD   107         CALL_OPC =    0xCD      ; CALL opcode
                                    108 
                           000067   109         STM8S003F3       = 103  ; 8K flash, 1K RAM, 128 EEPROM, UART1
                           000067   110         STM8S103F3       = 103  ; like STM8S003F3, 640 EEPROM
                           000069   111         STM8S105K4       = 105  ; 16K flash, 2K RAM, 1K EEPROM, UART2
                           000069   112         STM8S105C6       = 105  ; 32K flash, 2K RAM, 1K EEPROM, UART2
                                    113 
                                    114 
                                    115         ;********************************************
                                    116         ;******  2) Device hardware addresses  ******
                                    117         ;********************************************
                                    118 
                                    119         ;******  STM8S memory addresses ******
                           000000   120         RAMBASE =       0x0000  ; STM8S RAM start
                           004000   121         EEPROMBASE =    0x4000  ; STM8S EEPROM start
                                    122 
                                    123         ; STM8 device specific include (provided by file in board folder)
                                    124         ; sets "TARGET" and memory layout
                                    125         .include        "target.inc"
                                      1 ;       STM8S003F3 device and memory layout configuration
                                      2 
                           000067     3         TARGET = STM8S003F3
                                      4 
                           0003FF     5         RAMEND =        0x03FF  ; system (return) stack, growing down
                           00407F     6         EEPROMEND =     0x407F  ; STM8S003F3: 128 bytes EEPROM (maybe more, YMMV)
                           009FFF     7         FLASHEND =      0x9FFF  ; 8K devices
                                      8 
                           000040     9         FORTHRAM =      0x0040  ; Start of RAM controlled by Forth
                           000060    10         UPPLOC  =       0x0060  ; UPP (user/system area) location for 1K RAM
                           000080    11         CTOPLOC =       0x0080  ; CTOP (user dictionary) location for 1K RAM
                           000350    12         SPPLOC  =       0x0350  ; SPP (data stack top), TIB start
                           0003FF    13         RPPLOC  =       RAMEND  ; RPP (return stack top)
                                     14 
                                    126 
                                    127         ; STM8 unified register addresses (depends on "TARGET")
                                    128         .include        "stm8device.inc"
                                      1 ; STM8S register addresses
                                      2 
                                      3 ; ***** 6.2.1 I/O port hardware register map
                                      4 
                           005000     5         PORTA        = PA_ODR
                           005005     6         PORTB        = PB_ODR
                           00500A     7         PORTC        = PC_ODR
                           00500F     8         PORTD        = PD_ODR
                           005014     9         PORTE        = PE_ODR
                           005019    10         PORTF        = PF_ODR
                           00501E    11         PORTG        = PG_ODR
                           005023    12         PORTH        = PH_ODR
                           005028    13         PORTI        = PI_ODR
                           000000    14         PORTX        = 0
                                     15 
                           000000    16         ODR          = 0
                           000001    17         IDR          = 1
                           000002    18         DDR          = 2
                           000003    19         CR1          = 3
                           000004    20         CR2          = 4
                                     21 
                                     22 ; ***** Option Bytes in EEPROM
                                     23 
                           004803    24         OPT2         = 0x4803   ; Alternate Function Mapping            (0x00)
                           004804    25         NOPT2        = 0x4804   ; Alternate Function Mapping            (0xFF)
                                     26 
                                     27 ; ***** Port A
                                     28 
                           005000    29         PA_ODR       = 0x5000   ; Port A data output latch register     (0x00)
                           005001    30         PA_IDR       = 0x5001   ; Port A input pin value register       (0xXX)
                           005002    31         PA_DDR       = 0x5002   ; Port A data direction register        (0x00)
                           005003    32         PA_CR1       = 0x5003   ; Port A control register 1             (0x00)
                           005004    33         PA_CR2       = 0x5004   ; Port A control register 2             (0x00)
                                     34 
                                     35 ; ***** Port B
                                     36 
                           005005    37         PB_ODR       = 0x5005   ; Port B data output latch register     (0x00)
                           005006    38         PB_IDR       = 0x5006   ; Port B input pin value register       (0xXX)
                           005007    39         PB_DDR       = 0x5007   ; Port B data direction register        (0x00)
                           005008    40         PB_CR1       = 0x5008   ; Port B control register 1             (0x00)
                           005009    41         PB_CR2       = 0x5009   ; Port B control register 2             (0x00)
                                     42 
                                     43 ; ***** Port C
                                     44 
                           00500A    45         PC_ODR       = 0x500A   ; Port C data output latch register     (0x00)
                           00500B    46         PC_IDR       = 0x500B   ; Port C input pin value register       (0xXX)
                           00500C    47         PC_DDR       = 0x500C   ; Port C data direction register        (0x00)
                           00500D    48         PC_CR1       = 0x500D   ; Port C control register 1             (0x00)
                           00500E    49         PC_CR2       = 0x500E   ; Port C control register 2             (0x00)
                                     50 
                                     51 ; ***** Port D
                                     52 
                           00500F    53         PD_ODR       = 0x500F   ; Port D data output latch register     (0x00)
                           005010    54         PD_IDR       = 0x5010   ; Port D input pin value register       (0xXX)
                           005011    55         PD_DDR       = 0x5011   ; Port D data direction register        (0x00)
                           005012    56         PD_CR1       = 0x5012   ; Port D control register 1             (0x02)
                           005013    57         PD_CR2       = 0x5013   ; Port D control register 2             (0x00)
                                     58 
                                     59 ; ***** Port E
                                     60 ;.ifne  0
                           005014    61         PE_ODR       = 0x5014   ; Port E data output latch register     (0x00)
                           005015    62         PE_IDR       = 0x5015   ; Port E input pin value register       (0xXX)
                           005016    63         PE_DDR       = 0x5016   ; Port E data direction register        (0x00)
                           005017    64         PE_CR1       = 0x5017   ; Port E control register 1             (0x00)
                           005018    65         PE_CR2       = 0x5018   ; Port E control register 2             (0x00)
                                     66 ;.endif
                                     67 
                                     68 ; ***** Port F
                                     69 ;.ifne  0
                           005019    70         PF_ODR       = 0x5019   ; Port F data output latch register     (0x00)
                           00501A    71         PF_IDR       = 0x501A   ; Port F input pin value register       (0xXX)
                           00501B    72         PF_DDR       = 0x501B   ; Port F data direction register        (0x00)
                           00501C    73         PF_CR1       = 0x501C   ; Port F control register 1             (0x00)
                           00501D    74         PF_CR2       = 0x501D   ; Port F control register 2             (0x00)
                                     75 ;.endif
                                     76 
                                     77 ; ***** Port G
                                     78 ;.ifne  0
                           00501E    79         PG_ODR       = 0x501E   ; Port G data output latch register     (0x00)
                           00501F    80         PG_IDR       = 0x501F   ; Port G input pin value register       (0xXX)
                           005020    81         PG_DDR       = 0x5020   ; Port G data direction register        (0x00)
                           005021    82         PG_CR1       = 0x5021   ; Port G control register 1             (0x00)
                           005022    83         PG_CR2       = 0x5022   ; Port G control register 2             (0x00)
                                     84 ;.endif
                                     85 
                                     86 ; ***** Port H
                                     87 ;.ifne  0
                           005023    88         PH_ODR       = 0x5023   ; Port H data output latch register     (0x00)
                           005024    89         PH_IDR       = 0x5024   ; Port H input pin value register       (0xXX)
                           005025    90         PH_DDR       = 0x5025   ; Port H data direction register        (0x00)
                           005026    91         PH_CR1       = 0x5026   ; Port H control register 1             (0x00)
                           005027    92         PH_CR2       = 0x5027   ; Port H control register 2             (0x00)
                                     93 ;.endif
                                     94 
                                     95 ; ***** Port I
                                     96 ;.ifne  0
                           005028    97         PI_ODR       = 0x5028   ; Port I data output latch register     (0x00)
                           005029    98         PI_IDR       = 0x5029   ; Port I input pin value register       (0xXX)
                           00502A    99         PI_DDR       = 0x502A   ; Port I data direction register        (0x00)
                           00502B   100         PI_CR1       = 0x502B   ; Port I control register 1             (0x00)
                           00502C   101         PI_CR2       = 0x502C   ; Port I control register 2             (0x00)
                                    102 ;.endif
                                    103 
                                    104 ; ***** 6.2.2 General hardware register map
                                    105 
                                    106 ; ***** Flash
                                    107 ;.ifne  0
                           00505A   108         FLASH_CR1    = 0x505A   ; Flash control register 1              (0x00)
                           00505B   109         FLASH_CR2    = 0x505B   ; Flash control register 2              (0x00)
                           00505C   110         FLASH_NCR2   = 0x505C   ; Flash complementary control register 2 (0xFF)
                           00505D   111         FLASH_FPR    = 0x505D   ; Flash protection register             (0x00)
                           00505E   112         FLASH_NFPR   = 0x505E   ; Flash complementary protection register (0xFF)
                           00505F   113         FLASH_IAPSR  = 0x505F   ; Flash in-application programming status register (0x00)
                           005062   114         FLASH_PUKR   = 0x5062   ; Flash Program memory unprotection register (0x00)
                           005064   115         FLASH_DUKR   = 0x5064   ; Data EEPROM unprotection register     (0x00)
                                    116 ;.endif
                                    117 
                                    118 ; ***** ITC
                                    119 
                                    120 ;.ifne  0
                           0050A0   121         EXTI_CR1     = 0x50A0   ; External interrupt control register 1 (0x00)
                           0050A1   122         EXTI_CR2     = 0x50A1   ; External interrupt control register 2 (0x00)
                                    123 ;.endif
                                    124 
                                    125 ; ***** RST
                                    126 
                           0050B3   127         RST_SR       = 0x50B3   ; Reset status register                 (0xXX)
                                    128 
                                    129 ; ***** CLK
                                    130 
                                    131 ;.ifne  0
                           0050C0   132         CLK_ICKR     = 0x50C0   ; Internal clock control register       (0x01)
                           0050C1   133         CLK_ECKR     = 0x50C1   ; External clock control register       (0x00)
                           0050C3   134         CLK_CMSR     = 0x50C3   ; Clock master status register          (0xE1)
                           0050C4   135         CLK_SWR      = 0x50C4   ; Clock master switch register          (0xE1)
                           0050C5   136         CLK_SWCR     = 0x50C5   ; Clock switch control register         (0xXX)
                           0050C6   137         CLK_CKDIVR   = 0x50C6   ; Clock divider register                (0x18)
                           0050C7   138         CLK_PCKENR1  = 0x50C7   ; Peripheral clock gating register 1    (0xFF)
                           0050C8   139         CLK_CSSR     = 0x50C8   ; Clock security system register        (0x00)
                           0050C9   140         CLK_CCOR     = 0x50C9   ; Configurable clock control register   (0x00)
                           0050CA   141         CLK_PCKENR2  = 0x50CA   ; Peripheral clock gating register 2    (0xFF)
                           0050CC   142         CLK_HSITRIMR = 0x50CC   ; HSI clock calibration trimming register (0x00)
                           0050CD   143         CLK_SWIMCCR  = 0x50CD   ; SWIM clock control register     (0bXXXXXXX0)
                                    144 ;.endif
                                    145 
                                    146 ; ***** SWIM
                                    147 
                                    148 ;.ifne  0
                           0050CD   149         CLK_SWIMCCR  = 0x50CD   ; clock control register                (0bXXXXXXX0)
                                    150 ;.endif
                                    151 
                                    152 ; ***** WWDG
                                    153 
                                    154 ;.ifne  0
                           0050D1   155         WWDG_CR      = 0x50D1   ; WWDG control register                 (0x7F)
                           0050D2   156         WWDG_WR      = 0x50D2   ; WWDR window register                  (0x7F)
                                    157 ;.endif
                                    158 
                                    159 ; ***** IWDG
                                    160 
                           0050E0   161         IWDG_KR      = 0x50E0   ; IWDG key register                     (0xXX)
                           0050E1   162         IWDG_PR      = 0x50E1   ; IWDG prescaler register               (0x00)
                           0050E2   163         IWDG_RLR     = 0x50E2   ; IWDG reload register                  (0xFF)
                                    164 
                                    165 ; ***** AWU
                                    166 
                           0050F0   167         AWU_CSR1     = 0x50F0   ; AWU control/status register 1         (0x00)
                           0050F1   168         AWU_APR      = 0x50F1   ; AWU asynchronous prescaler buffer register (0x3F)
                           0050F2   169         AWU_TBR      = 0x50F2   ; AWU timebase selection register       (0x00)
                                    170 
                                    171 ; ***** BEEP
                                    172 
                           0050F3   173         BEEP_CSR     = 0x50F3   ; BEEP control/status register          (0x1F)
                                    174 
                                    175 ; ***** SPI
                                    176 
                           005200   177         SPI_CR1      = 0x5200   ; SPI control register 1                (0x00)
                           005201   178         SPI_CR2      = 0x5201   ; SPI control register 2                (0x00)
                           005202   179         SPI_ICR      = 0x5202   ; SPI interrupt control register        (0x00)
                           005203   180         SPI_SR       = 0x5203   ; SPI status register                   (0x02)
                           005204   181         SPI_DR       = 0x5204   ; SPI data register                     (0x00)
                           005205   182         SPI_CRCPR    = 0x5205   ; SPI CRC polynomial register           (0x07)
                           005206   183         SPI_RXCRCR   = 0x5206   ; SPI Rx CRC register                   (0x00)
                           005207   184         SPI_TXCRCR   = 0x5207   ; SPI Tx CRC register                   (0x00)
                                    185 
                                    186 ; ***** I2C
                                    187 
                           005210   188         I2C_CR1      = 0x5210   ; I2C control register 1                (0x00)
                           005211   189         I2C_CR2      = 0x5211   ; I2C control register 2                (0x00)
                           005212   190         I2C_FREQR    = 0x5212   ; I2C frequency register                (0x00)
                           005213   191         I2C_OARL     = 0x5213   ; I2C own address register low          (0x00)
                           005214   192         I2C_OARH     = 0x5214   ; I2C own address register high         (0x00)
                           005216   193         I2C_DR       = 0x5216   ; I2C data register                     (0x00)
                           005217   194         I2C_SR1      = 0x5217   ; I2C status register 1                 (0x00)
                           005218   195         I2C_SR2      = 0x5218   ; I2C status register 2                 (0x00)
                           005219   196         I2C_SR3      = 0x5219   ; I2C status register 3                 (0x00)
                           00521A   197         I2C_ITR      = 0x521A   ; I2C interrupt control register        (0x00)
                           00521B   198         I2C_CCRL     = 0x521B   ; I2C clock control register low        (0x00)
                           00521C   199         I2C_CCRH     = 0x521C   ; I2C clock control register high       (0x00)
                           00521D   200         I2C_TRISER   = 0x521D   ; I2C TRISE register                    (0x02)
                           00521E   201         I2C_PECR     = 0x521E   ; I2C packet error checking register    (0x00)
                                    202 
                                    203 ; ***** UART1
                                    204 ;.ifeq HAS_TXUART+HAS_RXUART
                                    205 
                           005230   206         UART1_SR     = 0x5230   ; UART1 status register                 (0xC0)
                           005231   207         UART1_DR     = 0x5231   ; UART1 data register                   (0xXX)
                           005232   208         UART1_BRR1   = 0x5232   ; UART1 baud rate register 1            (0x00)
                           005233   209         UART1_BRR2   = 0x5233   ; UART1 baud rate register 2            (0x00)
                           005234   210         UART1_CR1    = 0x5234   ; UART1 control register 1              (0x00)
                           005235   211         UART1_CR2    = 0x5235   ; UART1 control register 2              (0x00)
                           005236   212         UART1_CR3    = 0x5236   ; UART1 control register 3              (0x00)
                           005237   213         UART1_CR4    = 0x5237   ; UART1 control register 4              (0x00)
                           005238   214         UART1_CR5    = 0x5238   ; UART1 control register 5              (0x00)
                           005239   215         UART1_GTR    = 0x5239   ; UART1 guard time register             (0x00)
                           00523A   216         UART1_PSCR   = 0x523A   ; UART1 prescaler register              (0x00)
                                    217 
                           005240   218         UART2_SR     = 0x5240   ; UART1 status register                 (0xC0)
                           005241   219         UART2_DR     = 0x5241   ; UART1 data register                   (0xXX)
                           005242   220         UART2_BRR1   = 0x5242   ; UART1 baud rate register 1            (0x00)
                           005243   221         UART2_BRR2   = 0x5243   ; UART1 baud rate register 2            (0x00)
                           005244   222         UART2_CR1    = 0x5244   ; UART1 control register 1              (0x00)
                           005245   223         UART2_CR2    = 0x5245   ; UART1 control register 2              (0x00)
                           005246   224         UART2_CR3    = 0x5246   ; UART1 control register 3              (0x00)
                           005247   225         UART2_CR4    = 0x5247   ; UART1 control register 4              (0x00)
                           005248   226         UART2_CR5    = 0x5248   ; UART1 control register 5              (0x00)
                           005249   227         UART2_CR6    = 0x5249   ; UART1 control register 6              (0x00)
                           00524A   228         UART2_GTR    = 0x524A   ; UART1 guard time register             (0x00)
                           00524B   229         UART2_PSCR   = 0x524B   ; UART1 prescaler register              (0x00)
                                    230 
                           000000   231         .ifeq   (TARGET - STM8S105K4)
                                    232         UART_DR   = UART2_DR
                                    233         UART_SR   = UART2_SR
                                    234         UART_BRR1 = UART2_BRR1
                                    235         UART_CR2  = UART2_CR2
                           000001   236         .else
                           005231   237         UART_DR   = UART1_DR
                           005230   238         UART_SR   = UART1_SR
                           005232   239         UART_BRR1 = UART1_BRR1
                           005235   240         UART_CR2  = UART1_CR2
                                    241         .endif
                                    242 ;.endif
                                    243 
                                    244 ; ***** TIM1
                                    245 
                           005250   246         TIM1_CR1     = 0x5250   ; TIM1 control register 1               (0x00)
                           005251   247         TIM1_CR2     = 0x5251   ; TIM1 control register 2               (0x00)
                           005252   248         TIM1_SMCR    = 0x5252   ; TIM1 slave mode control register      (0x00)
                           005253   249         TIM1_ETR     = 0x5253   ; TIM1 external trigger register        (0x00)
                           005254   250         TIM1_IER     = 0x5254   ; TIM1 Interrupt enable register        (0x00)
                           005255   251         TIM1_SR1     = 0x5255   ; TIM1 status register 1                (0x00)
                           005256   252         TIM1_SR2     = 0x5256   ; TIM1 status register 2                (0x00)
                           005257   253         TIM1_EGR     = 0x5257   ; TIM1 event generation register        (0x00)
                           005258   254         TIM1_CCMR1   = 0x5258   ; TIM1 capture/compare mode register 1  (0x00)
                           005259   255         TIM1_CCMR2   = 0x5259   ; TIM1 capture/compare mode register 2  (0x00)
                           00525A   256         TIM1_CCMR3   = 0x525A   ; TIM1 capture/compare mode register 3  (0x00)
                           00525B   257         TIM1_CCMR4   = 0x525B   ; TIM1 capture/compare mode register 4  (0x00)
                           00525C   258         TIM1_CCER1   = 0x525C   ; TIM1 capture/compare enable register 1 (0x00)
                           00525D   259         TIM1_CCER2   = 0x525D   ; TIM1 capture/compare enable register 2 (0x00)
                           00525E   260         TIM1_CNTRH   = 0x525E   ; TIM1 counter high                     (0x00)
                           00525F   261         TIM1_CNTRL   = 0x525F   ; TIM1 counter low                      (0x00)
                           005260   262         TIM1_PSCRH   = 0x5260   ; TIM1 prescaler register high          (0x00)
                           005261   263         TIM1_PSCRL   = 0x5261   ; TIM1 prescaler register low           (0x00)
                           005262   264         TIM1_ARRH    = 0x5262   ; TIM1 auto-reload register high        (0xFF)
                           005263   265         TIM1_ARRL    = 0x5263   ; TIM1 auto-reload register low         (0xFF)
                           005264   266         TIM1_RCR     = 0x5264   ; TIM1 repetition counter register      (0x00)
                           005265   267         TIM1_CCR1H   = 0x5265   ; TIM1 capture/compare register 1 high  (0x00)
                           005266   268         TIM1_CCR1L   = 0x5266   ; TIM1 capture/compare register 1 low   (0x00)
                           005267   269         TIM1_CCR2H   = 0x5267   ; TIM1 capture/compare register 2 high  (0x00)
                           005268   270         TIM1_CCR2L   = 0x5268   ; TIM1 capture/compare register 2 low   (0x00)
                           005269   271         TIM1_CCR3H   = 0x5269   ; TIM1 capture/compare register 3 high  (0x00)
                           00526A   272         TIM1_CCR3L   = 0x526A   ; TIM1 capture/compare register 3 low   (0x00)
                           00526B   273         TIM1_CCR4H   = 0x526B   ; TIM1 capture/compare register 4 high  (0x00)
                           00526C   274         TIM1_CCR4L   = 0x526C   ; TIM1 capture/compare register 4 low   (0x00)
                           00526D   275         TIM1_BKR     = 0x526D   ; TIM1 break register                   (0x00)
                           00526E   276         TIM1_DTR     = 0x526E   ; TIM1 dead-time register               (0x00)
                           00526F   277         TIM1_OISR    = 0x526F   ; TIM1 output idle state register       (0x00)
                                    278 
                                    279 ; ***** TIM2
                                    280 ;.ifeq HAS_BACKGROUND
                                    281 
                           005300   282         TIM2_CR1     = 0x5300   ; TIM2 control register 1               (0x00)
                           000000   283         .ifeq   (TARGET - STM8S105K4)
                                    284         TIM2_IER     = 0x5301   ; TIM2 interrupt enable register        (0x00)
                                    285         TIM2_SR1     = 0x5302   ; TIM2 status register 1                (0x00)
                                    286         TIM2_SR2     = 0x5303   ; TIM2 status register 2                (0x00)
                                    287         TIM2_EGR     = 0x5304   ; TIM2 event generation register        (0x00)
                                    288         TIM2_CCMR1   = 0x5305   ; TIM2 capture/compare mode register 1  (0x00)
                                    289         TIM2_CCMR2   = 0x5306   ; TIM2 capture/compare mode register 2  (0x00)
                                    290         TIM2_CCMR3   = 0x5307   ; TIM2 capture/compare mode register 3  (0x00)
                                    291         TIM2_CCER1   = 0x5308   ; TIM2 capture/compare enable register 1 (0x00)
                                    292         TIM2_CCER2   = 0x5309   ; TIM2 capture/compare enable register 2 (0x00)
                                    293         TIM2_CNTRH   = 0x530A   ; TIM2 counter high                     (0x00)
                                    294         TIM2_CNTRL   = 0x530B   ; TIM2 counter low                      (0x00)
                                    295         TIM2_PSCR    = 0x530C   ; TIM2 prescaler register               (0x00)
                                    296         TIM2_ARRH    = 0x530D   ; TIM2 auto-reload register high        (0xFF)
                                    297         TIM2_ARRL    = 0x530E   ; TIM2 auto-reload register low         (0xFF)
                                    298         TIM2_CCR1H   = 0x530F   ; TIM2 capture/compare register 1 high  (0x00)
                                    299         TIM2_CCR1L   = 0x5310   ; TIM2 capture/compare register 1 low   (0x00)
                                    300         TIM2_CCR2H   = 0x5311   ; TIM2 capture/compare reg. 2 high      (0x00)
                                    301         TIM2_CCR2L   = 0x5312   ; TIM2 capture/compare register 2 low   (0x00)
                                    302         TIM2_CCR3H   = 0x5313   ; TIM2 capture/compare register 3 high  (0x00)
                                    303         TIM2_CCR3L   = 0x5314   ; TIM2 capture/compare register 3 low   (0x00)
                           000001   304         .else
                           005303   305         TIM2_IER     = 0x5303   ; TIM2 interrupt enable register        (0x00)
                           005304   306         TIM2_SR1     = 0x5304   ; TIM2 status register 1                (0x00)
                           005305   307         TIM2_SR2     = 0x5305   ; TIM2 status register 2                (0x00)
                           005306   308         TIM2_EGR     = 0x5306   ; TIM2 event generation register        (0x00)
                           005307   309         TIM2_CCMR1   = 0x5307   ; TIM2 capture/compare mode register 1  (0x00)
                           005308   310         TIM2_CCMR2   = 0x5308   ; TIM2 capture/compare mode register 2  (0x00)
                           005309   311         TIM2_CCMR3   = 0x5309   ; TIM2 capture/compare mode register 3  (0x00)
                           00530A   312         TIM2_CCER1   = 0x530A   ; TIM2 capture/compare enable register 1 (0x00)
                           00530B   313         TIM2_CCER2   = 0x530B   ; TIM2 capture/compare enable register 2 (0x00)
                           00530C   314         TIM2_CNTRH   = 0x530C   ; TIM2 counter high                     (0x00)
                           00530D   315         TIM2_CNTRL   = 0x530D   ; TIM2 counter low                      (0x00)
                           00530E   316         TIM2_PSCR    = 0x530E   ; TIM2 prescaler register               (0x00)
                           00530F   317         TIM2_ARRH    = 0x530F   ; TIM2 auto-reload register high        (0xFF)
                           005310   318         TIM2_ARRL    = 0x5310   ; TIM2 auto-reload register low         (0xFF)
                           005311   319         TIM2_CCR1H   = 0x5311   ; TIM2 capture/compare register 1 high  (0x00)
                           005312   320         TIM2_CCR1L   = 0x5312   ; TIM2 capture/compare register 1 low   (0x00)
                           005313   321         TIM2_CCR2H   = 0x5313   ; TIM2 capture/compare reg. 2 high      (0x00)
                           005314   322         TIM2_CCR2L   = 0x5314   ; TIM2 capture/compare register 2 low   (0x00)
                           005315   323         TIM2_CCR3H   = 0x5315   ; TIM2 capture/compare register 3 high  (0x00)
                           005316   324         TIM2_CCR3L   = 0x5316   ; TIM2 capture/compare register 3 low   (0x00)
                                    325         .endif
                                    326 ;.endif
                                    327 
                                    328 ; ***** TIM4
                                    329 
                                    330 ;.ifeq HAS_TXSIM+HAS_RXSIM
                                    331 
                           005340   332         TIM4_CR1     = 0x5340   ; TIM4 control register 1               (0x00)
                           000000   333         .ifeq   (TARGET - STM8S105K4)
                                    334         TIM4_IER     = 0x5341   ; TIM4 interrupt enable register        (0x00)
                                    335         TIM4_SR      = 0x5342   ; TIM4 status register                  (0x00)
                                    336         TIM4_EGR     = 0x5343   ; TIM4 event generation register        (0x00)
                                    337         TIM4_CNTR    = 0x5344   ; TIM4 counter                          (0x00)
                                    338         TIM4_PSCR    = 0x5345   ; TIM4 prescaler register               (0x00)
                                    339         TIM4_ARR     = 0x5346   ; TIM4 auto-reload register             (0xFF)
                           000001   340         .else
                           005343   341         TIM4_IER     = 0x5343   ; TIM4 interrupt enable register        (0x00)
                           005344   342         TIM4_SR      = 0x5344   ; TIM4 status register                  (0x00)
                           005345   343         TIM4_EGR     = 0x5345   ; TIM4 event generation register        (0x00)
                           005346   344         TIM4_CNTR    = 0x5346   ; TIM4 counter                          (0x00)
                           005347   345         TIM4_PSCR    = 0x5347   ; TIM4 prescaler register               (0x00)
                           005348   346         TIM4_ARR     = 0x5348   ; TIM4 auto-reload register             (0xFF)
                                    347         .endif
                                    348 ;.endif
                                    349 
                                    350 ; ***** ADC1
                                    351 
                                    352 ;.ifeq HAS_ADC
                                    353 
                           0053E0   354         ADC_DBxR     = 0x53E0   ; ADC data buffer registers 0x53E0 to 0x53F3 (0x00)
                           005400   355         ADC_CSR      = 0x5400   ; ADC control/status register           (0x00)
                           005401   356         ADC_CR1      = 0x5401   ; ADC configuration register 1          (0x00)
                           005402   357         ADC_CR2      = 0x5402   ; ADC configuration register 2          (0x00)
                           005403   358         ADC_CR3      = 0x5403   ; ADC configuration register 3          (0x00)
                           005404   359         ADC_DRH      = 0x5404   ; ADC data register high                (0xXX)
                           005405   360         ADC_DRL      = 0x5405   ; ADC data register low                 (0xXX)
                           005406   361         ADC_TDRH     = 0x5406   ; ADC Schmitt trigger disable register high (0x00)
                           005407   362         ADC_TDRL     = 0x5407   ; ADC Schmitt trigger disable register low (0x00)
                           005408   363         ADC_HTRH     = 0x5408   ; ADC high threshold register high      (0x03)
                           005409   364         ADC_HTRL     = 0x5409   ; ADC high threshold register low       (0xFF)
                           00540A   365         ADC_LTRH     = 0x540A   ; ADC low threshold register high       (0x00)
                           00540B   366         ADC_LTRL     = 0x540B   ; ADC low threshold register low        (0x00)
                           00540C   367         ADC_AWSRH    = 0x540C   ; ADC analog watchdog status register high (0x00)
                           00540D   368         ADC_AWSRL    = 0x540D   ; ADC analog watchdog status register low (0x00)
                           00540E   369         ADC_AWCRH    = 0x540E   ; ADC analog watchdog control register high (0x00)
                           00540F   370         ADC_AWCRL    = 0x540F   ; ADC analog watchdog control register low (0x00)
                                    371 ;.endif
                                    372 
                                    373 ; ***** 6.2.3 CPU/SWIM/debug module/interrupt controller registers
                                    374 
                                    375 
                                    376 ; ***** CPU
                                    377 
                                    378 ;.ifne  0
                           007F00   379         CPU_A        = 0x7F00   ; Accumulator                           (0x00)
                           007F01   380         CPU_PCE      = 0x7F01   ; Program counter extended              (0x00)
                           007F02   381         CPU_PCH      = 0x7F02   ; Program counter high                  (0x00)
                           007F03   382         CPU_PCL      = 0x7F03   ; Program counter low                   (0x00)
                           007F04   383         CPU_XH       = 0x7F04   ; X index register high                 (0x00)
                           007F05   384         CPU_XL       = 0x7F05   ; X index register low                  (0x00)
                           007F06   385         CPU_YH       = 0x7F06   ; Y index register high                 (0x00)
                           007F07   386         CPU_YL       = 0x7F07   ; Y index register low                  (0x00)
                           007F08   387         CPU_SPH      = 0x7F08   ; Stack pointer high                    (0x03)
                           007F09   388         CPU_SPL      = 0x7F09   ; Stack pointer low                     (0xFF)
                           007F0A   389         CPU_CCR      = 0x7F0A   ; Condition code register               (0x28)
                                    390 ;.endif
                                    391 
                                    392 ; ***** CFG
                                    393 
                           007F60   394         CFG_GCR      = 0x7F60   ; Global configuration register         (0x00)
                                    395 
                                    396 ; ***** ITC
                                    397 
                                    398 ;.ifne  HAS_CPNVM
                           007F70   399         ITC_SPR1     = 0x7F70   ; Interrupt software priority register 1 (0xFF)
                           007F71   400         ITC_SPR2     = 0x7F71   ; Interrupt software priority register 2 (0xFF)
                           007F72   401         ITC_SPR3     = 0x7F72   ; Interrupt software priority register 3 (0xFF)
                           007F73   402         ITC_SPR4     = 0x7F73   ; Interrupt software priority register 4 (0xFF)
                           007F74   403         ITC_SPR5     = 0x7F74   ; Interrupt software priority register 5 (0xFF)
                           007F75   404         ITC_SPR6     = 0x7F75   ; Interrupt software priority register 6 (0xFF)
                           007F76   405         ITC_SPR7     = 0x7F76   ; Interrupt software priority register 7 (0xFF)
                           007F77   406         ITC_SPR8     = 0x7F77   ; Interrupt software priority register 8 (0xFF)
                                    407 ;.endif
                                    408 
                                    409 ; ***** SWIM
                                    410 
                                    411 ;.ifne  0
                           007F80   412         SWIM_CSR     = 0x7F80   ; SWIM control status register          (0x00)
                                    413 ;.endif
                                    414 
                                    415 ; ***** DM
                                    416 
                                    417 ;.ifne  0
                           007F90   418         DM_BK1RE     = 0x7F90   ; DM breakpoint 1 register extended byte (0xFF)
                           007F91   419         DM_BK1RH     = 0x7F91   ; DM breakpoint 1 register high byte    (0xFF)
                           007F92   420         DM_BK1RL     = 0x7F92   ; DM breakpoint 1 register low byte     (0xFF)
                           007F93   421         DM_BK2RE     = 0x7F93   ; DM breakpoint 2 register extended byte (0xFF)
                           007F94   422         DM_BK2RH     = 0x7F94   ; DM breakpoint 2 register high byte    (0xFF)
                           007F95   423         DM_BK2RL     = 0x7F95   ; DM breakpoint 2 register low byte     (0xFF)
                           007F96   424         DM_CR1       = 0x7F96   ; DM debug module control register 1    (0x00)
                           007F97   425         DM_CR2       = 0x7F97   ; DM debug module control register 2    (0x00)
                           007F98   426         DM_CSR1      = 0x7F98   ; DM debug module control/status register 1 (0x10)
                           007F99   427         DM_CSR2      = 0x7F99   ; DM debug module control/status register 2 (0x00)
                           007F9A   428         DM_ENFCTR    = 0x7F9A   ; DM enable function register           (0xFF)
                                    429 ;.endif
                                    129 
                                    130         ;**********************************
                                    131         ;******  3) Global defaults  ******
                                    132         ;**********************************
                                    133         ; Note: add defaults for new features here
                                    134         ;       and configure them in globconf.inc
                                    135 
                                    136         .include  "defconf.inc"
                                      1 ;--------------------------------------------------------
                                      2 ;       STM8EF for STM8S (Value line and Access Line devices)
                                      3 ;       Default settings for all kinds of options
                                      4 ;--------------------------------------------------------
                           000002     5         RELVER1          = 2    ; Revision digit 1
                           000000     6         RELVER0          = 0    ; Revision digit 0
                                      7 
                           000001     8         TERM_LINUX       = 1    ; LF terminates line
                           000000     9         HALF_DUPLEX      = 0    ; Use EMIT/?KEY in half duplex mode
                           000001    10         HAS_TXUART       = 1    ; Enable UART TXD, word TX!
                           000001    11         HAS_RXUART       = 1    ; Enable UART RXD, word ?RX
                           000000    12         HAS_TXSIM        = 0    ; Enable TxD via GPIO/TIM4, word TXGP!
                           000000    13         HAS_RXSIM        = 0    ; Enable RxD via GPIO/TIM4, word ?RXGP
                           000000    14         PSIM     = PORTX        ; Port for UART simulation
                           000001    15         PNRX             = 1    ; Port GPIO# for HAS_RXDSIM
                           000001    16         PNTX             = 1    ; Port GPIO# for HAS_TXDSIM
                                     17 
                           0003CB    18         EMIT_BG  = DROP         ; TODO: vectored NUL background EMIT vector
                           0004DB    19         QKEY_BG  = ZERO         ; TODO: NUL background QKEY vector
                                     20 
                           000000    21         HAS_LED7SEG      = 0    ; 7-seg LED display, number of groups (0: none)
                           000003    22         LEN_7SGROUP      = 3    ; default: 3 dig. 7-seg LED
                                     23 
                           000000    24         HAS_KEYS         = 0    ; Board has keys
                           000000    25         HAS_OUTPUTS      = 0    ; Board outputs, e.g. relays
                           000000    26         HAS_INPUTS       = 0    ; Board digital inputs
                           000000    27         HAS_ADC          = 0    ; Board analog inputs
                                     28 
                           000000    29         HAS_BACKGROUND   = 0    ; Background Forth task (TIM2 ticker)
                           0026DE    30         BG_TIM2_REL = 0x26DE    ; Reload value for TIM2 background ticker (default 0x26DE @ 5ms HSE)
                           000000    31         BG_RUNMASK  =      0    ; BG task runs if "(BG_RUNMASK AND TICKCNT) equals 0"
                           000000    32         HAS_CPNVM        = 0    ; Can compile to Flash, always interpret to RAM
                           000000    33         HAS_DOES         = 0    ; DOES> extension
                           000000    34         HAS_DOLOOP       = 0    ; DO .. LOOP extension: DO LEAVE LOOP +LOOP
                           000001    35         HAS_ALIAS        = 1    ; NAME> resolves "alias" (RigTig style), aliases can be in RAM
                           000000    36         HAS_FILEHAND     = 0    ; FILE and HAND for file upload
                           000000    37         HAS_OLDOK        = 0    ; Disable e4thcom file upload support
                                     38 
                           000000    39         USE_CALLDOLIT    = 0    ; use CALL DOLIT instead of the DOLIT TRAP handler (deprecated)
                           000000    40         CASEINSENSITIVE  = 0    ; Case insensitive dictionary search
                           000000    41         SPEEDOVERSIZE    = 0    ; Speed-over-size in core words ROT - = < -1 0 1
                           000000    42         BAREBONES        = 0    ; Removes words: '?KEY 'EMIT EXIT EXG @EXECUTE ERASE
                                     43                                 ;   Drops headers: ?RX TX! ?RXP ?RX TXP! TX! LAST DEPTH COUNT
                                     44                                 ;     SPACES .R NAME> ABORT" AHEAD
                                     45                                 ; Drops support for entry of binary (%) and decimal (&)
                           000000    46         BOOTSTRAP        = 0    ; Remove words: (+loop) EXIT 2! 2/ UM+ OR = MAX MIN U. . ? .(
                                     47                                 ;  [COMPILE] FOR DO BEGIN WHILE ABORT" ." _TYPE dm+ DUMP .S
                                     48                                 ;  .ID >CHAR <
                           000000    49         UNLINKCORE       = 0    ; Drops headers on everything except: (TODO)
                                     50                                 ;  ABORT" AFT AGAIN AHEAD BEGIN DO DUMP ELSE EXG FOR IF LEAVE
                                     51                                 ;  LOOP MAX MIN NEXT OR REPEAT SEE SPACES THEN U. U.R UM+
                                     52                                 ;  UNTIL WHILE WORDS [COMPILE] _TYPE dm+
                           000000    53         NO_VARIABLE      = 0    ; Disable VARIABLE and feature "VARIABLE in Flash allocates RAM"
                                     54 
                           000000    55         WORDS_LINKINTER  = 0    ; Link interpreter words
                           000000    56         WORDS_LINKCOMP   = 0    ; Link compiler words: cp last OVERT $,n ?UNIQUE $COMPILE
                           000000    57         WORDS_LINKRUNTI  = 0    ; Link runtime words: doLit do$ doVAR donxt dodoes ?branch branch
                                     58                                 ;    (+loop) $"| ."|
                           000000    59         WORDS_LINKCHAR   = 0    ; Link char out words: DIGIT <# # #S SIGN #> str hld HOLD PACK$
                           000000    60         WORDS_LINKMISC   = 0    ; Link composing words of: >CHAR _TYPE dm+ .ID >NAME
                                     61 
                           000000    62         WORDS_EXTRASTACK = 0    ; Link/include stack core words: rp@ rp! sp! sp@
                           000000    63         WORDS_EXTRADEBUG = 0    ; Extra debug words: >NAME
                           000000    64         WORDS_EXTRACORE  = 0    ; Extra core words: 0= I
                           000000    65         WORDS_EXTRAMEM   = 0    ; Extra memory words: B! 2C@ 2C!
                           000000    66         WORDS_EXTRAEEPR  = 0    ; Extra EEPROM lock/unlock words: LOCK ULOCK ULOCKF LOCKF
                           000000    67         WORDS_HWREG      = 0    ; Peripheral Register words
                                     68 
                                     69 
                           000000    70         UNLINK_COLD      = 0    ; "COLD"
                           000001    71         UNLINK_TBOOT     = 1    ; "'BOOT"
                           000001    72         UNLINK_HI        = 1    ; "hi"
                           000000    73         UNLINK_QRX       = 0    ; "?RX"
                           000000    74         UNLINK_TXSTOR    = 0    ; "TX!"
                           000000    75         UNLINK_QRXP      = 0    ; "?RXP"
                           000000    76         UNLINK_QRX       = 0    ; "?RX"
                           000000    77         UNLINK_TXPSTOR   = 0    ; "TXP!"
                           000000    78         UNLINK_TXSTOR    = 0    ; "TX!"
                           000000    79         UNLINK_QKEY      = 0    ; "?KEY"
                           000000    80         UNLINK_EMIT      = 0    ; "EMIT"
                           000000    81         UNLINK_DOLIT     = 0    ; "doLit"
                           000001    82         UNLINK_DOPLOOP   = 1    ; "(+loop)"
                           000000    83         UNLINK_LEAVE     = 0    ; "LEAVE"
                           000000    84         UNLINK_DONXT     = 0    ; "donxt"
                           000000    85         UNLINK_QBRAN     = 0    ; "?branch"
                           000000    86         UNLINK_BRAN      = 0    ; "branch"
                           000000    87         UNLINK_EXECU     = 0    ; "EXECUTE"
                           000000    88         UNLINK_EXIT      = 0    ; "EXIT"
                           000000    89         REMOVE_EXIT      = 0    ; remove "EXIT"
                           000000    90         UNLINK_DSTOR     = 0    ; "2!"
                           000000    91         UNLINK_DAT       = 0    ; "2@"
                           000000    92         UNLINK_DCSTOR    = 0    ; "2C!"
                           000000    93         UNLINK_DCAT      = 0    ; "2C@"
                           000000    94         UNLINK_BRSS      = 0    ; "B!"
                           000000    95         UNLINK_AT        = 0    ; "@"
                           000000    96         UNLINK_STORE     = 0    ; "!"
                           000000    97         UNLINK_CAT       = 0    ; "C@"
                           000000    98         UNLINK_CSTOR     = 0    ; "C!"
                           000000    99         UNLINK_IGET      = 0    ; "I"
                           000000   100         UNLINK_RFROM     = 0    ; "R>"
                           000000   101         UNLINK_DOVAR     = 0    ; "doVar"
                           000000   102         UNLINK_RAT       = 0    ; "R@"
                           000000   103         UNLINK_TOR       = 0    ; ">R"
                           000000   104         UNLINK_NIP       = 0    ; "NIP"
                           000000   105         UNLINK_DROP      = 0    ; "DROP"
                           000000   106         UNLINK_DDROP     = 0    ; "2DROP"
                           000000   107         UNLINK_DUPP      = 0    ; "DUP"
                           000000   108         UNLINK_SWAPP     = 0    ; "SWAP"
                           000000   109         UNLINK_OVER      = 0    ; "OVER"
                           000000   110         UNLINK_UPLUS     = 0    ; "UM+"
                           000000   111         UNLINK_PLUS      = 0    ; "+"
                           000000   112         UNLINK_XORR      = 0    ; "XOR"
                           000000   113         UNLINK_ANDD      = 0    ; "AND"
                           000000   114         UNLINK_ORR       = 0    ; "OR"
                           000000   115         UNLINK_ZLESS     = 0    ; "0<"
                           000000   116         UNLINK_SUBB      = 0    ; "-"
                           000001   117         UNLINK_CNTXT     = 1    ; "CONTEXT"
                           000000   118         UNLINK_CPP       = 0    ; "cp"
                           000000   119         UNLINK_BASE      = 0    ; "BASE"
                           000001   120         UNLINK_INN       = 1    ; ">IN"
                           000001   121         UNLINK_NTIB      = 1    ; "#TIB"
                           000001   122         UNLINK_TEVAL     = 1    ; "'eval"
                           000001   123         UNLINK_HLD       = 1    ; "hld"
                           000000   124         UNLINK_TEMIT     = 0    ; "'EMIT"
                           000000   125         UNLINK_TQKEY     = 0    ; "'?KEY"
                           000000   126         UNLINK_LAST      = 0    ; "last"
                           000001   127         UNLINK_TIB       = 1    ; "TIB"
                           000000   128         UNLINK_OUTA      = 0    ; "OUT"
                           000000   129         UNLINK_BLANK     = 0    ; "BL"
                           000000   130         UNLINK_ZERO      = 0    ; "0"
                           000000   131         UNLINK_ONE       = 0    ; "1"
                           000000   132         UNLINK_MONE      = 0    ; "-1"
                           000000   133         UNLINK_TIMM      = 0    ; "TIM"
                           000000   134         UNLINK_BGG       = 0    ; "BG"
                           000001   135         UNLINK_TPROMPT   = 1    ; "'PROMPT"
                           000000   136         UNLINK_HANDD     = 0    ; "HAND"
                           000000   137         UNLINK_FILEE     = 0    ; "FILE"
                           000000   138         UNLINK_QDUP      = 0    ; "?DUP"
                           000000   139         UNLINK_ROT       = 0    ; "ROT"
                           000000   140         UNLINK_DDUP      = 0    ; "2DUP"
                           000000   141         UNLINK_DNEGA     = 0    ; "DNEGATE"
                           000000   142         UNLINK_EQUAL     = 0    ; "="
                           000000   143         UNLINK_ULESS     = 0    ; "U<"
                           000000   144         UNLINK_LESS      = 0    ; "<"
                           000000   145         UNLINK_MAX       = 0    ; "MAX"
                           000000   146         UNLINK_MIN       = 0    ; "MIN"
                           000000   147         UNLINK_WITHI     = 0    ; "WITHIN"
                           000000   148         UNLINK_UMMOD     = 0    ; "UM/MOD"
                           000000   149         UNLINK_MSMOD     = 0    ; "M/MOD"
                           000000   150         UNLINK_SLMOD     = 0    ; "/MOD"
                           000000   151         UNLINK_MMOD      = 0    ; "MOD"
                           000000   152         UNLINK_SLASH     = 0    ; "/"
                           000000   153         UNLINK_UMSTA     = 0    ; "UM*"
                           000000   154         UNLINK_STAR      = 0    ; "*"
                           000000   155         UNLINK_MSTAR     = 0    ; "M*"
                           000000   156         UNLINK_SSMOD     = 0    ; "*/MOD"
                           000000   157         UNLINK_STASL     = 0    ; "*/"
                           000000   158         UNLINK_EXG       = 0    ; "EXG"
                           000000   159         UNLINK_TWOSL     = 0    ; "2/"
                           000000   160         UNLINK_CELLS     = 0    ; "2*"
                           000000   161         UNLINK_CELLM     = 0    ; "2-"
                           000000   162         UNLINK_CELLP     = 0    ; "2+"
                           000000   163         UNLINK_ONEM      = 0    ; "1-"
                           000000   164         UNLINK_ONEP      = 0    ; "1+"
                           000000   165         UNLINK_INVER     = 0    ; "NOT"
                           000000   166         UNLINK_NEGAT     = 0    ; "NEGATE"
                           000000   167         UNLINK_ABSS      = 0    ; "ABS"
                           000000   168         UNLINK_ZEQUAL    = 0    ; "0="
                           000000   169         UNLINK_PICK      = 0    ; "PICK"
                           000000   170         UNLINK_TCHAR     = 0    ; ">CHAR"
                           000000   171         UNLINK_DEPTH     = 0    ; "DEPTH"
                           000000   172         UNLINK_PSTOR     = 0    ; "+!"
                           000000   173         UNLINK_COUNT     = 0    ; "COUNT"
                           000000   174         UNLINK_HERE      = 0    ; "HERE"
                           000001   175         UNLINK_PAD       = 1    ; "PAD"
                           000001   176         UNLINK_ATEXE     = 1    ; "@EXECUTE"
                           000000   177         UNLINK_CMOVE     = 0    ; "CMOVE"
                           000000   178         UNLINK_FILL      = 0    ; "FILL"
                           000000   179         UNLINK_ERASE     = 0    ; "ERASE"
                           000001   180         UNLINK_PACKS     = 1    ; "PACK$"
                           000001   181         UNLINK_DIGIT     = 1    ; "DIGIT"
                           000001   182         UNLINK_EXTRC     = 1    ; "EXTRACT"
                           000000   183         UNLINK_BDIGS     = 0    ; "<#"
                           000000   184         UNLINK_HOLD      = 0    ; "HOLD"
                           000000   185         UNLINK_DIG       = 0    ; "#"
                           000000   186         UNLINK_DIGS      = 0    ; "#S"
                           000000   187         UNLINK_SIGN      = 0    ; "SIGN"
                           000000   188         UNLINK_EDIGS     = 0    ; "#>"
                           000001   189         UNLINK_STR       = 1    ; "str"
                           000000   190         UNLINK_HEX       = 0    ; "HEX"
                           000000   191         UNLINK_DECIM     = 0    ; "DECIMAL"
                           000001   192         UNLINK_NUMBQ     = 1    ; "NUMBER?"
                           000001   193         UNLINK_DIGTQ     = 1    ; "DIGIT?"
                           000000   194         UNLINK_KEY       = 0    ; "KEY"
                           000000   195         UNLINK_NUFQ      = 0    ; "NUF?"
                           000001   196         REMOVE_NUFQ      = 1    ; remove "NUF?"
                           000000   197         UNLINK_SPACE     = 0    ; "SPACE"
                           000000   198         UNLINK_SPACS     = 0    ; "SPACES"
                           000000   199         UNLINK_CR        = 0    ; "CR"
                           000001   200         UNLINK_DOSTR     = 1    ; "do$"
                           000001   201         UNLINK_STRQP     = 1    ; '$"|'
                           000001   202         UNLINK_DOTQP     = 1    ; '."|'
                           000000   203         UNLINK_DOTR      = 0    ; ".R"
                           000000   204         UNLINK_UDOTR     = 0    ; "U.R"
                           000000   205         UNLINK_TYPES     = 0    ; "TYPE"
                           000000   206         UNLINK_UDOT      = 0    ; "U."
                           000000   207         UNLINK_DOT       = 0    ; "."
                           000000   208         UNLINK_QUEST     = 0    ; "?"
                           000001   209         UNLINK_PARS      = 1    ; "pars"
                           000001   210         UNLINK_PARSE     = 1    ; "PARSE"
                           000000   211         UNLINK_DOTPR     = 0    ; ".("
                           000000   212         UNLINK_PAREN     = 0    ; "("
                           000000   213         UNLINK_BKSLA     = 0    ; "\"
                           000001   214         UNLINK_WORDD     = 1    ; "WORD"
                           000001   215         UNLINK_TOKEN     = 1    ; "TOKEN"
                           000000   216         UNLINK_NAMET     = 0    ; "NAME>"
                           000001   217         UNLINK_SAMEQ     = 1    ; "SAME?"
                           000001   218         UNLINK_CUPPER    = 1    ; "CUPPER"
                           000001   219         UNLINK_NAMEQ     = 1    ; "NAME?"
                           000001   220         UNLINK_FIND      = 1    ; "find"
                           000001   221         UNLINK_BKSP      = 1    ; "^h"
                           000001   222         UNLINK_TAP       = 1    ; "TAP"
                           000001   223         UNLINK_KTAP      = 1    ; "kTAP"
                           000001   224         UNLINK_ACCEP     = 1    ; "ACCEPT"
                           000001   225         UNLINK_QUERY     = 1    ; "QUERY"
                           000001   226         UNLINK_ABORT     = 1    ; "ABORT"
                           000001   227         UNLINK_ABORQ     = 1    ; "aborq"
                           000001   228         UNLINK_PRESE     = 1    ; "PRESET"
                           000001   229         UNLINK_INTER     = 1    ; "$INTERPRET"
                           000000   230         UNLINK_LBRAC     = 0    ; "["
                           000001   231         UNLINK_DOTOK     = 1    ; ".OK"
                           000001   232         UNLINK_QSTAC     = 1    ; "?STACK"
                           000001   233         UNLINK_EVAL      = 1    ; "EVAL"
                           000001   234         UNLINK_QUIT      = 1    ; "QUIT"
                           000000   235         UNLINK_TICK      = 0    ; "'"
                           000000   236         UNLINK_JSRC      = 0    ; "CALL,"
                           000000   237         UNLINK_LITER     = 0    ; "LITERAL"
                           000000   238         UNLINK_BCOMP     = 0    ; "[COMPILE]"
                           000000   239         UNLINK_COMPI     = 0    ; "COMPILE"
                           000001   240         UNLINK_STRCQ     = 1    ; "$,""
                           000000   241         UNLINK_FOR       = 0    ; "FOR"
                           000000   242         UNLINK_NEXT      = 0    ; "NEXT"
                           000000   243         UNLINK_DOO       = 0    ; "DO"
                           000000   244         UNLINK_LOOP      = 0    ; "LOOP"
                           000000   245         UNLINK_PLOOP     = 0    ; "+LOOP"
                           000000   246         UNLINK_BEGIN     = 0    ; "BEGIN"
                           000000   247         UNLINK_UNTIL     = 0    ; "UNTIL"
                           000000   248         UNLINK_AGAIN     = 0    ; "AGAIN"
                           000000   249         UNLINK_IFF       = 0    ; "IF"
                           000000   250         UNLINK_THENN     = 0    ; "THEN"
                           000000   251         UNLINK_ELSE      = 0    ; "ELSE"
                           000000   252         UNLINK_AHEAD     = 0    ; "AHEAD"
                           000000   253         UNLINK_WHILE     = 0    ; "WHILE"
                           000000   254         UNLINK_REPEA     = 0    ; "REPEAT"
                           000000   255         UNLINK_AFT       = 0    ; "AFT"
                           000000   256         UNLINK_ABRTQ     = 0    ; 'ABORT"'
                           000000   257         UNLINK_STRQ      = 0    ; '$"'
                           000000   258         UNLINK_DOTQ      = 0    ; '."'
                           000000   259         UNLINK_UNIQU     = 0    ; "?UNIQUE"
                           000001   260         UNLINK_SNAME     = 1    ; "$,n"
                           000001   261         UNLINK_SCOMP     = 1    ; "$COMPILE"
                           000000   262         UNLINK_OVERT     = 0    ; "OVERT"
                           000000   263         UNLINK_COLON     = 0    ; ":"
                           000000   264         UNLINK_RBRAC     = 0    ; "]"
                           000000   265         UNLINK_DOESS     = 0    ; "DOES>"
                           000000   266         UNLINK_DODOES    = 0    ; "dodoes"
                           000000   267         UNLINK_CREAT     = 0    ; "CREATE"
                           000000   268         UNLINK_CONST     = 0    ; "CONSTANT"
                           000001   269         UNLINK_DOCON     = 1    ; "docon"
                           000000   270         UNLINK_VARIA     = 0    ; "VARIABLE"
                           000000   271         UNLINK_ALLOT     = 0    ; "ALLOT"
                           000000   272         UNLINK_IMMED     = 0    ; "IMMEDIATE"
                           000001   273         UNLINK_UTYPE     = 1    ; "_TYPE"
                           000001   274         UNLINK_DUMPP     = 1    ; "dm+"
                           000000   275         UNLINK_DUMP      = 0    ; "DUMP"
                           000000   276         REMOVE_DUMP      = 0    ; remove "DUMP"
                           000000   277         UNLINK_DOTS      = 0    ; ".S"
                           000000   278         REMOVE_DOTS      = 0    ; remove ".S"
                           000000   279         UNLINK_DOTID     = 0    ; ".ID"
                           000001   280         UNLINK_TNAME     = 1    ; ">NAME"
                           000001   281         REMOVE_TNAME     = 1    ; remove ">TNAME"
                           000000   282         UNLINK_WORDS     = 0    ; "WORDS"
                           000000   283         UNLINK_EMIT7S    = 0    ; "E7S"
                           000000   284         UNLINK_PUT7S     = 0    ; "P7S"
                           000000   285         UNLINK_QKEYB     = 0    ; "?KEYB"
                           000000   286         UNLINK_ADCSTOR   = 0    ; "ADC!"
                           000000   287         UNLINK_ADCAT     = 0    ; "ADC@"
                           000000   288         UNLINK_SPSTO     = 0    ; "sp!"
                           000000   289         UNLINK_SPAT      = 0    ; "sp@"
                           000000   290         UNLINK_RPAT      = 0    ; "rp@"
                           000000   291         UNLINK_RPSTO     = 0    ; "rp!"
                           000000   292         UNLINK_ULOCK     = 0    ; "ULOCK"
                           000000   293         UNLINK_LOCK      = 0    ; "LOCK"
                           000001   294         UNLINK_UNLOCK_FLASH = 1 ; "ULOCKF"
                           000001   295         UNLINK_LOCK_FLASH = 1   ; "LOCKF"
                           000000   296         UNLINK_NVMM      = 0    ; "NVM"
                           000000   297         UNLINK_RAMM      = 0    ; "RAM"
                           000000   298         UNLINK_RESETT    = 0    ; "RESET"
                           000000   299         UNLINK_SAVEC     = 0    ; "SAVEC"
                           000000   300         UNLINK_RESTC     = 0    ; "IRET"
                                    137 
                                    138         ;********************************************
                                    139         ;******  4) Device dependent features  ******
                                    140         ;********************************************
                                    141         ; Define memory location for device dependent features here
                                    142 
                                    143         .include "globconf.inc"
                                      1 ; STM8EF Global Configuration File
                                      2 ; Config for W1209 Thermostat Module
                                      3 ; Clock: HSI (no crystal)
                                      4 
                           000000     5         HALF_DUPLEX      = 0    ; Use EMIT/?KEY in half duplex mode
                           000000     6         HAS_TXUART       = 0    ; No UART TXD, word TX!
                           000000     7         HAS_RXUART       = 0    ; No UART RXD, word ?RX
                           00500A     8         PSIM     = PORTC        ; Port for UART simulation
                           000001     9         HAS_TXSIM        = 1    ; Enable TxD via GPIO/TIM4, word TXGP!
                           000005    10         PNTX             = 5    ; Port GPIO# for HAS_TXDSIM
                           000001    11         HAS_RXSIM        = 1    ; Enable RxD via GPIO/TIM4, word ?RXGP
                           000004    12         PNRX             = 4    ; Port GPIO# for HAS_RXDSIM
                                     13 
                           001279    14         EMIT_BG  = EMIT7S       ; 7S-LED background EMIT vector
                           0012DE    15         QKEY_BG  = QKEYB        ; Board keys background QKEY vector
                                     16 
                           000001    17         HAS_LED7SEG      = 1    ; yes, 1*3 dig. 7-seg LED on module
                                     18 
                           000003    19         HAS_KEYS         = 3    ; yes, 3 keys on module
                           000001    20         HAS_OUTPUTS      = 1    ; yes, one LED
                           000001    21         HAS_ADC          = 1    ; Analog input words
                                     22 
                           000001    23         HAS_BACKGROUND   = 1    ; Background Forth task (TIM2 ticker)
                           000001    24         HAS_CPNVM        = 1    ; Can compile to Flash, always interpret to RAM
                           000001    25         HAS_DOES         = 1    ; CREATE-DOES> extension
                           000001    26         HAS_DOLOOP       = 1    ; DO .. LOOP extension: DO LEAVE LOOP +LOOP
                                     27 
                                     28 
                           000001    29         CASEINSENSITIVE  = 1    ; Case insensitive dictionary search
                           000000    30         SPEEDOVERSIZE    = 0    ; Speed-over-size in core words: ROT - = <
                           000000    31         BAREBONES        = 0    ; Remove or unlink some more: hi HERE .R U.R SPACES @EXECUTE AHEAD CALL, EXIT COMPILE [COMPILE]
                                     32 
                           000000    33         WORDS_LINKINTER  = 0    ; Link interpreter words: ACCEPT QUERY TAP kTAP hi 'BOOT tmp >IN 'TIB #TIB eval CONTEXT pars PARSE NUMBER? DIGIT? WORD TOKEN NAME> SAME? find ABORT aborq $INTERPRET INTER? .OK ?STACK EVAL PRESET QUIT $COMPILE
                           000000    34         WORDS_LINKCOMP   = 0    ; Link compiler words: cp last OVERT $"| ."| $,n
                           000000    35         WORDS_LINKRUNTI  = 0    ; Link runtime words: doLit do$ doVAR donxt dodoes ?branch branch
                           000001    36         WORDS_LINKCHAR   = 1    ; Link char out words: DIGIT <# # #S SIGN #> str hld HOLD
                           000000    37         WORDS_LINKMISC   = 0    ; Link composing words of SEE DUMP WORDS: >CHAR _TYPE dm+ .ID >NAME
                                     38 
                           000000    39         WORDS_EXTRASTACK = 0    ; Link/include stack core words: rp@ rp! sp! sp@ DEPTH
                           000000    40         WORDS_EXTRADEBUG = 0    ; Extra debug words: SEE
                           000001    41         WORDS_EXTRACORE  = 1    ; Extra core words: =0 I
                           000001    42         WORDS_EXTRAMEM   = 1    ; Extra memory words: B! 2C@ 2C!
                           000001    43         WORDS_EXTRAEEPR  = 1    ; Extra EEPROM lock/unlock words: LOCK ULOCK ULOCKF LOCKF
                                    144 
                                    145         .include "linkopts.inc"
                           000001     1         .ifne  HAS_CPNVM
                           000000     2         UNLINK_TBOOT     = 0    ; "'BOOT"
                           000000     3         UNLINK_HI        = 0    ; "hi"
                                      4         .endif
                                      5 
                           000000     6         .ifne WORDS_LINKINTER
                                      7         UNLINK_TBOOT     = 0    ; "'BOOT"   ; INIT
                                      8         UNLINK_HI        = 0    ; "hi"      ; INIT
                                      9         UNLINK_CNTXT     = 0    ; "CONTEXT" ; parser
                                     10         UNLINK_INN       = 0    ; ">IN"     ; charInput
                                     11         UNLINK_NTIB      = 0    ; "#TIB"    ; charInput
                                     12         UNLINK_TEVAL     = 0    ; "'eval"   ; REPL
                                     13         UNLINK_TPROMPT   = 0    ; "'PROMPT"
                                     14         UNLINK_NUMBQ     = 0    ; "NUMBER?" ; parser
                                     15         UNLINK_DIGITQ    = 0    ; "DIGIT?"  ; parser
                                     16         UNLINK_PARS      = 0    ; "pars"    ; parser
                                     17         UNLINK_PARSE     = 0    ; "PARSE"   ; parser
                                     18         UNLINK_WORDD     = 0    ; "WORD"    ; parser
                                     19         UNLINK_TOKEN     = 0    ; "TOKEN"   ; parser
                                     20         UNLINK_SAMEQ     = 0    ; "SAME?"   ; parser
                                     21         UNLINK_CUPPER    = 0    ; "CUPPER"  ; parser
                                     22         UNLINK_NAMEQ     = 0    ; "NAME?"   ; parser
                                     23         UNLINK_FIND      = 0    ; "find"    ; parser
                                     24         UNLINK_BKSP      = 0    ; "^h"     ; charInput
                                     25         UNLINK_TAP       = 0    ; "TAP"    ; charInput
                                     26         UNLINK_KTAP      = 0    ; "kTAP"   ; charInput 
                                     27         UNLINK_ACCEP     = 0    ; "ACCEPT" ; charInput
                                     28         UNLINK_QUERY     = 0    ; "QUERY"  ; charInput
                                     29         UNLINK_ABORT     = 0    ; "ABORT"  ; REPL
                                     30         UNLINK_ABORQ     = 0    ; "aborq"  ; REPL -
                                     31         UNLINK_PRESE     = 0    ; "PRESET" ; REPL
                                     32         UNLINK_INTER     = 0    ; "$INTERPRET" ; REPL -
                                     33         UNLINK_SCOMP     = 0    ; "$COMPILE" ; REPL -
                                     34         UNLINK_DOTOK     = 0    ; ".OK" ; REPL
                                     35         UNLINK_QSTAC     = 0    ; "?STACK" ; REPL  
                                     36         UNLINK_EVAL      = 0    ; "EVAL"  ; REPL 
                                     37         UNLINK_QUIT      = 0    ; "QUIT"  ; REPL
                                     38         .endif
                                     39 
                                     40 
                                    146 
                                    147         ;**************************************
                                    148         ;******  5) Board Driver Memory  ******
                                    149         ;**************************************
                                    150         ; Memory for board related code, e.g. interrupt routines
                                    151 
                           000040   152         RAMPOOL =    FORTHRAM   ; RAM for variables (growing up)
                                    153 
                                    154         .macro  RamByte varname
                                    155         varname = RAMPOOL
                                    156         RAMPOOL = RAMPOOL + 1
                                    157         .endm
                                    158 
                                    159         .macro  RamWord varname
                                    160         varname = RAMPOOL
                                    161         RAMPOOL = RAMPOOL + 2
                                    162         .endm
                                    163 
                                    164         .macro  RamBlck varname, size
                                    165         varname = RAMPOOL
                                    166         RAMPOOL = RAMPOOL + size
                                    167         .endm
                                    168 
                                    169         ;******  Board variables  ******
                           000001   170         .ifne   HAS_OUTPUTS
      00808B                        171         RamWord OUTPUTS         ; outputs, like relays, LEDs, etc. (16 bit)
                           000040     1         OUTPUTS = RAMPOOL
                           000042     2         RAMPOOL = RAMPOOL + 2
                                    172         .endif
                                    173 
                           000001   174         .ifne   HAS_BACKGROUND
                                    175 
                           000001   176         .ifne   HAS_LED7SEG
                           000000   177         .if     gt,(HAS_LED7SEG-1)
                                    178         RamByte LED7GROUP       ; index [0..(HAS_LED7SEG-1)] of 7-SEG digit group
                                    179         .endif
                                    180 
                           000003   181         DIGITS = HAS_LED7SEG*LEN_7SGROUP
      000000                        182         RamBlck LED7FIRST,DIGITS ; leftmost 7S-LED digit
                           000042     1         LED7FIRST = RAMPOOL
                           000045     2         RAMPOOL = RAMPOOL + DIGITS
                           000044   183         LED7LAST = RAMPOOL-1    ; save memory location of rightmost 7S-LED digit
                                    184         .endif
                                    185 
      000000                        186         RamWord BGADDR          ; address of background routine (0: off)
                           000045     1         BGADDR = RAMPOOL
                           000047     2         RAMPOOL = RAMPOOL + 2
      000000                        187         RamWord TICKCNT         ; 16 bit ticker (counts up)
                           000047     1         TICKCNT = RAMPOOL
                           000049     2         RAMPOOL = RAMPOOL + 2
                                    188 
                           000001   189         .ifne   HAS_KEYS
      000000                        190         RamByte KEYREPET        ; board key repetition control (8 bit)
                           000049     1         KEYREPET = RAMPOOL
                           00004A     2         RAMPOOL = RAMPOOL + 1
                                    191         .endif
                                    192 
                           000020   193         BSPPSIZE  =     32      ; Size of data stack for background tasks
                           00005F   194         PADBG     =     0x5F    ; PAD in background task growing down from here
                           000000   195         .else
                                    196         BSPPSIZE  =     0       ;  no background, no extra data stack
                                    197         .endif
                                    198 
                                    199 
                                    200         ;**************************************************
                                    201         ;******  6) General User & System Variables  ******
                                    202         ;**************************************************
                                    203 
                                    204         ; ****** Indirect variables for code in NVM *****
                           000001   205         .ifne   HAS_CPNVM
                           000010   206         ISPPSIZE  =     16      ; Size of data stack for interrupt tasks
                           000000   207         .else
                                    208         ISPPSIZE  =     0       ; no interrupt tasks without NVM
                                    209         .endif
                                    210 
                           000060   211         UPP   = UPPLOC          ; offset user area
                           000080   212         CTOP  = CTOPLOC         ; dictionary start, growing up
                                    213                                 ; note: PAD is inbetween CTOP and SPP
                           000320   214         SPP   = ISPP-ISPPSIZE   ; data stack, growing down (with SPP-1 first)
                           000330   215         ISPP  = SPPLOC-BSPPSIZE ; "ISPP" Interrupt data stack, growing down
                           000350   216         BSPP  = SPPLOC          ; "BSPP" Background data stack, growing down
                           000350   217         TIBB  = SPPLOC          ; "TIBB" Term. Input Buf. TIBLENGTH between SPPLOC and RPP
                           0003FF   218         RPP   = RPPLOC          ; "RPP" return stack, growing down
                                    219 
                                    220         ; Core variables (same order as 'BOOT initializer block)
                                    221 
                           000060   222         USRRAMINIT = USREMIT
                                    223 
                           000060   224         USREMIT  =   UPP+0      ; "'EMIT" execution vector of EMIT
                           000062   225         USRQKEY =    UPP+2      ; "'?KEY" execution vector of QKEY
                           000064   226         USRBASE =    UPP+4      ; "BASE" radix base for numeric I/O
                           000066   227         USREVAL =    UPP+6      ; "'EVAL" execution vector of EVAL
                           000068   228         USRPROMPT =  UPP+8      ; "'PROMPT" point to prompt word (default .OK)
                           00006A   229         USRCP   =    UPP+10     ; "CP" point to top of dictionary
                           00006C   230         USRLAST =    UPP+12     ; "LAST" currently last name in dictionary (init: to LASTN)
                           00006E   231         NVMCP   =    UPP+14     ; point to top of dictionary in Non Volatile Memory
                                    232 
                                    233         ; Null initialized core variables (growing down)
                                    234 
                           000070   235         USRCTOP  =   UPP+16     ; "CTOP" point to the start of RAM dictionary
                           000072   236         USRVAR  =    UPP+18     ; "VAR" point to next free USR RAM location
                           000074   237         NVMCONTEXT = UPP+20     ; point to top of dictionary in Non Volatile Memory
                           000076   238         USRCONTEXT = UPP+22     ; "CONTEXT" start vocabulary search
                           000078   239         USRHLD  =    UPP+24     ; "HLD" hold a pointer of output string
                           00007A   240         USRNTIB =    UPP+26     ; "#TIB" count in terminal input buffer
                           00007C   241         USR_IN  =    UPP+28     ; ">IN" hold parsing pointer
                           00007E   242         YTEMP   =    UPP+30     ; extra working register for core words
                                    243 
                                    244         ;***********************
                                    245         ;******  7) Code  ******
                                    246         ;***********************
                                    247 
                                    248 ;        ==============================================
                                    249 ;        Forth header macros
                                    250 ;        Macro support in SDCC's assembler "SDAS" has some quirks:
                                    251 ;          * strings with "," and ";" arn't allowed in parameters
                                    252 ;          * after include files, the first macro call may fail
                                    253 ;            unless it's preceded by unconditional code
                                    254 ;         ==============================================
                                    255 
                           000000   256         LINK =          0       ;
                                    257 
                                    258         .macro  HEADER Label wName
                                    259         .ifeq   UNLINK_'Label
                                    260         .dw     LINK
                                    261         LINK    = .
                                    262         .db      (102$ - 101$)
                                    263 101$:
                                    264         .ascii  wName
                                    265 102$:
                                    266         .endif
                                    267 ;'Label:
                                    268         .endm
                                    269 
                                    270         .macro  HEADFLG Label wName wFlag
                                    271         .ifeq   UNLINK_'Label
                                    272         .dw     LINK
                                    273         LINK    = .
                                    274         .db      ((102$ - 101$) + wFlag)
                                    275 101$:
                                    276         .ascii  wName
                                    277 102$:
                                    278         .endif
                                    279 ;'Label:
                                    280         .endm
                                    281 
                                    282 ;         ==============================================
                                    283 ;               Low level code
                                    284 ;         ==============================================
                                    285 
                                    286 ;       TRAP handler for DOLIT
                                    287 ;       Push the inline literal following the TRAP instruction
      000000                        288 _TRAP_Handler:
                           000001   289         .ifeq  USE_CALLDOLIT
      00808B 5A               [ 2]  290         DECW    X
      00808C 5A               [ 2]  291         DECW    X
      00808D 1F 03            [ 2]  292         LDW     (3,SP),X               ; XH,XL
      00808F 51               [ 1]  293         EXGW    X,Y
      008090 1E 08            [ 2]  294         LDW     X,(8,SP)               ; PC MSB/LSB
      008092 FE               [ 2]  295         LDW     X,(X)
      008093 90 FF            [ 2]  296         LDW     (Y),X
      008095 1F 05            [ 2]  297         LDW     (5,SP),X               ; YH,YL
      008097 1E 08            [ 2]  298         LDW     X,(8,SP)
      008099 5C               [ 1]  299         INCW    X
      00809A 5C               [ 1]  300         INCW    X
      00809B 1F 08            [ 2]  301         LDW     (8,SP),X
      00809D 80               [11]  302         IRET
                                    303 
                                    304 ;       Macros for inline literals using the TRAP approach
                                    305 
                                    306         .macro DoLitC c
                                    307         TRAP
                                    308         .dw     c
                                    309         .endm
                                    310 
                                    311         .macro DoLitW w
                                    312         TRAP
                                    313         .dw     w
                                    314         .endm
                                    315 
                           000000   316         .else
                                    317 
                                    318 ;       Macros for inline literals using CALL DOLIT / CALL DOLITC
                                    319         .macro DoLitC c
                                    320         call    DOLITC
                                    321         .db     c
                                    322         .endm
                                    323 
                                    324         .macro DoLitW w
                                    325         call    DOLIT
                                    326         .dw     w
                                    327         .endm
                                    328 
                                    329         .endif
                                    330 
                                    331 ;       TIM2 interrupt handler for background task
      00809E                        332 _TIM2_UO_IRQHandler:
                           000001   333         .ifne   (HAS_LED7SEG + HAS_BACKGROUND)
      00809E 72 11 53 04      [ 1]  334         BRES    TIM2_SR1,#0     ; clear TIM2 UIF
                                    335 
                           000001   336         .ifne   HAS_LED7SEG
      0080A2 CD 81 9C         [ 4]  337         CALL    LED_MPX         ; board dependent code for 7Seg-LED-Displays
                                    338         .endif
                                    339 
                                    340 ;       Background operation saves & restores the context of the interactive task
                                    341 ;       Cyclic context reset of Forth background task (stack, BASE, HLD, I/O vector)
                           000001   342         .ifne   HAS_BACKGROUND
      0080A5 BE 47            [ 2]  343         LDW     X,TICKCNT
      0080A7 5C               [ 1]  344         INCW    X
      0080A8 BF 47            [ 2]  345         LDW     TICKCNT,X
                                    346         ; fall through
                                    347 
                           000000   348         .ifne   BG_RUNMASK
                                    349         LD      A,XL            ; Background task runs if "(BG_RUNMASK AND TICKCNT) equals 0"
                                    350         AND     A,#BG_RUNMASK
                                    351         JRNE    TIM2IRET
                                    352         .endif
                                    353 
      0080AA 90 BE 45         [ 2]  354         LDW     Y,BGADDR        ; address of background task
      0080AD 90 5D            [ 2]  355         TNZW    Y               ; 0: background operation off
      0080AF 27 36            [ 1]  356         JREQ    TIM2IRET
                                    357 
      0080B1 BE 7E            [ 2]  358         LDW     X,YTEMP         ; Save context
      0080B3 89               [ 2]  359         PUSHW   X
                                    360 
      0080B4 3B 00 65         [ 1]  361         PUSH    USRBASE+1       ; 8bit since BASE should be < 36
      0080B7 35 0A 00 65      [ 1]  362         MOV     USRBASE+1,#10
                                    363 
      0080BB BE 60            [ 2]  364         LDW     X,USREMIT       ; save EMIT exection vector
      0080BD 89               [ 2]  365         PUSHW   X
      0080BE AE 93 04         [ 2]  366         LDW     X,#(EMIT_BG)
      0080C1 BF 60            [ 2]  367         LDW     USREMIT,X
                                    368 
      0080C3 BE 62            [ 2]  369         LDW     X,USRQKEY       ; save QKEY exection vector
      0080C5 89               [ 2]  370         PUSHW   X
      0080C6 AE 93 69         [ 2]  371         LDW     X,#(QKEY_BG)
      0080C9 BF 62            [ 2]  372         LDW     USRQKEY,X
                                    373 
      0080CB BE 78            [ 2]  374         LDW     X,USRHLD
      0080CD 89               [ 2]  375         PUSHW   X
      0080CE AE 00 5F         [ 2]  376         LDW     X,#(PADBG)      ; in background task, alway start with an empty PAD
      0080D1 BF 78            [ 2]  377         LDW     USRHLD,X
                                    378 
      0080D3 AE 03 50         [ 2]  379         LDW     X,#(BSPP)       ; init data stack for background task to BSPP
      0080D6 90 FD            [ 4]  380         CALL    (Y)
                                    381 
      0080D8 85               [ 2]  382         POPW    X
      0080D9 BF 78            [ 2]  383         LDW     USRHLD,X
                                    384 
      0080DB 85               [ 2]  385         POPW    X
      0080DC BF 62            [ 2]  386         LDW     USRQKEY,X
                                    387 
      0080DE 85               [ 2]  388         POPW    X
      0080DF BF 60            [ 2]  389         LDW     USREMIT,X
                                    390 
      0080E1 32 00 65         [ 1]  391         POP     USRBASE+1       ; this may not work in uCsim
                                    392 
      0080E4 85               [ 2]  393         POPW    X
      0080E5 BF 7E            [ 2]  394         LDW     YTEMP,X
      0080E7                        395 TIM2IRET:
                                    396         .endif
                                    397 
      0080E7 80               [11]  398         IRET
                                    399         .endif
                                    400 
                                    401 
                                    402 ;       ==============================================
                                    403 
                                    404 ;       Main entry points and COLD start data
                                    405 
                                    406 ;       COLD    ( -- )
                                    407 ;       The hilevel cold start sequence.
      00005D                        408         HEADER  COLD "COLD"
                           000001     1         .ifeq   UNLINK_COLD
      0080E8 00 00                    2         .dw     LINK
                           00005F     3         LINK    = .
      0080EA 04                       4         .db      (102$ - 101$)
      0080EB                          5 101$:
      0080EB 43 4F 4C 44              6         .ascii  "COLD"
      0080EF                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                    409 
      0080EF                        410 _forth:                         ; SDCC entry
                                    411         ; Note: no return to main.c possible unless RAMEND equals SP,
                                    412         ; and RPP init skipped
                                    413 
      0080EF                        414 COLD:
      0080EF 9B               [ 1]  415         SIM                     ; disable interrupts
      0080F0 35 00 50 C6      [ 1]  416         MOV     CLK_CKDIVR,#0   ; Clock divider register
                                    417 
      0080F4 AE 03 BF         [ 2]  418         LDW     X,#(RAMEND-FORTHRAM)
      0080F7 6F 40            [ 1]  419 1$:     CLR     (FORTHRAM,X)
      0080F9 5A               [ 2]  420         DECW    X
      0080FA 2A FB            [ 1]  421         JRPL    1$
                                    422 
      0080FC AE 03 FF         [ 2]  423         LDW     X,#RPP          ; initialize return stack
      0080FF 94               [ 1]  424         LDW     SP,X
                                    425 
      008100 AD 79            [ 4]  426         CALLR   BOARDINIT       ; Board initialization (see "boardcore.inc")
                                    427 
                           000001   428         .ifne   HAS_BACKGROUND
                                    429         ; init BG timer interrupt
      008102 35 F7 7F 73      [ 1]  430         MOV     ITC_SPR4,#0xF7  ; Interrupt prio. low for TIM2 (Int13)
      008106 35 03 53 0E      [ 1]  431         MOV     TIM2_PSCR,#0x03 ; prescaler 1/8
      00810A 35 26 53 0F      [ 1]  432         MOV     TIM2_ARRH,#(BG_TIM2_REL/256)  ; reload H
      00810E 35 DE 53 10      [ 1]  433         MOV     TIM2_ARRL,#(BG_TIM2_REL%256)  ;        L
      008112 35 01 53 00      [ 1]  434         MOV     TIM2_CR1,#0x01  ; enable TIM2
      008116 35 01 53 03      [ 1]  435         MOV     TIM2_IER,#0x01  ; enable TIM2 interrupt
                                    436         .endif
                                    437 
                           000000   438         .ifne   HAS_RXUART+HAS_TXUART
                                    439         ; Init RS232 communication port
                                    440         ; STM8S[01]003F3 init UART
                                    441         LDW     X,#0x6803       ; 9600 baud
                                    442         LDW     UART_BRR1,X
                                    443         .ifne   HAS_RXUART*HAS_TXUART
                                    444         MOV     UART_CR2,#0x0C  ; Use UART1 full duplex
                                    445         .ifne   HALF_DUPLEX
                                    446         MOV     UART1_CR5,#0x08 ; UART1 Half-Duplex
                                    447         .endif
                                    448         .else
                                    449         .ifne   HAS_TXUART
                                    450         MOV     UART_CR2,#0x08  ; UART1 enable tx
                                    451         .endif
                                    452         .ifne   HAS_RXUART
                                    453         MOV     UART_CR2,#0x04  ; UART1 enable rx
                                    454         .endif
                                    455         .endif
                                    456         .endif
                                    457 
                           000001   458         .ifne   HAS_RXSIM+HAS_TXSIM
                                    459         ; TIM4 based RXD or TXD: initialize timer
                           0000CF   460         TIM4RELOAD = 0xCF       ; reload 0.104 ms (9600 baud)
      00811A 35 CF 53 48      [ 1]  461         MOV     TIM4_ARR,#TIM4RELOAD
      00811E 35 03 53 47      [ 1]  462         MOV     TIM4_PSCR,#0x03 ; prescaler 1/8
      008122 35 01 53 40      [ 1]  463         MOV     TIM4_CR1,#0x01  ; enable TIM4
                                    464         .endif
                                    465 
                           000001   466         .ifne   HAS_TXSIM*((PNRX-PNTX)+(1-HAS_RXSIM))
                                    467         ; init TxD through GPIO if not shared pin with PNRX
      008126 72 1A 50 0A      [ 1]  468         BSET    PSIM+ODR,#PNTX    ; PNTX GPIO high
      00812A 72 1A 50 0C      [ 1]  469         BSET    PSIM+DDR,#PNTX    ; PNTX GPIO output
      00812E 72 1A 50 0D      [ 1]  470         BSET    PSIM+CR1,#PNTX    ; enable PNTX push-pull
                                    471         .endif
                                    472 
                           000001   473         .ifne   (HAS_RXSIM)
                                    474         ; init RxD through GPIO
                                    475 
                           000000   476         .ifeq   (PSIM-PORTA)
                                    477         BSET    EXTI_CR1,#1     ; External interrupt Port A falling edge
                           000001   478         .else
                                    479 
                           000000   480         .ifeq   (PSIM-PORTB)
                                    481         BSET    EXTI_CR1,#3     ; External interrupt Port B falling edge
                           000001   482         .else
                                    483 
                           000001   484         .ifeq   (PSIM-PORTC)
      008132 72 1A 50 A0      [ 1]  485         BSET    EXTI_CR1,#5     ; External interrupt Port C falling edge
                           000000   486         .else
                                    487         BSET    EXTI_CR1,#7     ; External interrupt Port D falling edge
                                    488         .endif
                                    489 
                                    490         .endif
                                    491         .endif
      008136 72 19 50 0C      [ 1]  492         BRES    PSIM+DDR,#PNRX    ; 0: input (default)
      00813A 72 18 50 12      [ 1]  493         BSET    PD_CR1,#PNRX    ; enable PNRX pull-up
      00813E 72 18 50 0E      [ 1]  494         BSET    PSIM+CR2,#PNRX    ; enable PNRX external interrupt
                                    495         .endif
                                    496 
      008142 CD 8D 85         [ 4]  497         CALL    PRESE           ; initialize data stack, TIB
                                    498 
      0000BA                        499         DoLitW  UZERO
      008145 83               [ 9]    1         TRAP
      008146 82 2D                    2         .dw     UZERO
      0000BD                        500         DoLitC  USRRAMINIT
      008148 83               [ 9]    1         TRAP
      008149 00 60                    2         .dw     USRRAMINIT
      0000C0                        501         DoLitC  (ULAST-UZERO)
      00814B 83               [ 9]    1         TRAP
      00814C 00 10                    2         .dw     (ULAST-UZERO)
      00814E CD 88 87         [ 4]  502         CALL    CMOVE           ; initialize user area
                                    503 
                           000001   504         .ifne  HAS_CPNVM
      008151 51               [ 1]  505         EXGW    X,Y
      008152 BE 6A            [ 2]  506         LDW     X,USRCP         ; reserve some space for user variable
      008154 BF 72            [ 2]  507         LDW     USRVAR,X
      008156 1C 00 20         [ 2]  508         ADDW    X,#32
      008159 BF 6A            [ 2]  509         LDW     USRCP,X
      00815B BF 70            [ 2]  510         LDW     USRCTOP,X       ; store new CTOP
      00815D 51               [ 1]  511         EXGW    X,Y
                                    512         .endif
                                    513 
                           000001   514         .ifne   HAS_OUTPUTS
      00815E CD 85 66         [ 4]  515         CALL    ZERO
      008161 CD 81 FA         [ 4]  516         CALL    OUTSTOR
                                    517         .endif
                                    518 
                           000001   519         .ifne   HAS_LED7SEG
                                    520 
                           000000   521         .if     gt,(HAS_LED7SEG-1)
                                    522         MOV     LED7GROUP,#0     ; one of position HAS_LED7SEG 7-SEG digit groups
                                    523         .endif
                                    524 
      008164 35 66 00 42      [ 1]  525         MOV     LED7FIRST  ,#0x66 ; 7S LEDs 4..
      008168 35 78 00 43      [ 1]  526         MOV     LED7FIRST+1,#0x78 ; 7S LEDs .t.
      00816C 35 74 00 44      [ 1]  527         MOV     LED7FIRST+2,#0x74 ; 7S LEDs ..h
                                    528 
                                    529         .endif
                                    530 
                                    531         ; Hardware initialization complete
      008170 9A               [ 1]  532         RIM                     ; enable interrupts
                                    533 
      008171 72 CD 82 2B      [ 6]  534         CALL    [TBOOT+3]       ; application boot
      008175 CD 90 B1         [ 4]  535         CALL    OVERT           ; initialize CONTEXT from USRLAST
      008178 CC 8E 1E         [ 2]  536         JP      QUIT            ; start interpretation
                                    537 
                                    538 
                                    539 ;       ##############################################
                                    540 ;       Include for board support code
                                    541 ;       Board I/O initialization and E/E mapping code
                                    542 ;       Hardware dependent words, e.g.  BKEY, OUT!
                                    543         .include "boardcore.inc"
                                      1 ; XH-W1209 STM8S device dependent HW routines
                                      2 
                                      3 
                                      4 ;       BOARDINIT  ( -- )
                                      5 ;       Init board GPIO (except COM ports)
      00817B                          6 BOARDINIT:
                                      7         ; Board I/O initialization
                                      8 
                                      9         ; W1209 STM8S003F3 init GPIO
      00817B 35 0E 50 02      [ 1]   10         MOV     PA_DDR,#0b00001110 ; relay,B,F
      00817F 35 0E 50 03      [ 1]   11         MOV     PA_CR1,#0b00001110
      008183 35 30 50 07      [ 1]   12         MOV     PB_DDR,#0b00110000 ; d2,d3
      008187 35 30 50 08      [ 1]   13         MOV     PB_CR1,#0b00110000
      00818B 35 C0 50 0C      [ 1]   14         MOV     PC_DDR,#0b11000000 ; G,C
      00818F 35 F8 50 0D      [ 1]   15         MOV     PC_CR1,#0b11111000 ; G,C-+S... Key pullups
      008193 35 3E 50 11      [ 1]   16         MOV     PD_DDR,#0b00111110 ; A,DP,D,d1,A
      008197 35 3E 50 12      [ 1]   17         MOV     PD_CR1,#0b00111110
      00819B 81               [ 4]   18         RET
                                     19 
                                     20 ;===============================================================
                                     21 
                                     22 ;      Dummy labels for PSIM interrupts declared in main.c
                                     23 
                           000001    24         .ifne   PSIM-PORTA
                                     25 ;       Dummy label for _EXTIA_IRQHandler
      00819C                         26 _EXTI0_IRQHandler:
                                     27         .endif
                                     28 
                           000001    29         .ifne   PSIM-PORTB
                                     30 ;       Dummy label for _EXTIB_IRQHandler
      00819C                         31 _EXTI1_IRQHandler:
                                     32         .endif
                                     33 
                           000000    34         .ifne   PSIM-PORTC
                                     35 ;       Dummy label for _EXTIC_IRQHandler
                                     36 _EXTI2_IRQHandler:
                                     37         .endif
                                     38 
                           000001    39         .ifne   PSIM-PORTD
                                     40 ;       Dummy label for _EXTID_IRQHandler
      00819C                         41 _EXTI3_IRQHandler:
                                     42         .endif
                                     43 
                                     44 
                                     45 ;===============================================================
                                     46 
                           000001    47         .ifne   HAS_LED7SEG
                                     48 ;       LED_MPX driver ( -- )
                                     49 ;       Output bit pattern in A to 7S-LED digit hardware
                                     50 
      00819C                         51 LED_MPX:
      00819C 72 18 50 0F      [ 1]   52         BSET    PD_ODR,#4       ; Digit .3..
      0081A0 72 1A 50 05      [ 1]   53         BSET    PB_ODR,#5       ; Digit ..2.
      0081A4 72 18 50 05      [ 1]   54         BSET    PB_ODR,#4       ; Digit ...1
                                     55 
      0081A8 B6 48            [ 1]   56         LD      A,TICKCNT+1
      0081AA A4 03            [ 1]   57         AND     A,#0x03         ; 3 digits MPX
                                     58 
      0081AC 26 06            [ 1]   59         JRNE    1$
      0081AE 72 19 50 0F      [ 1]   60         BRES    PD_ODR,#4       ; digit .3..
      0081B2 20 12            [ 2]   61         JRA     3$
                                     62 
      0081B4 A1 01            [ 1]   63 1$:     CP      A,#1
      0081B6 26 06            [ 1]   64         JRNE    2$
      0081B8 72 1B 50 05      [ 1]   65         BRES    PB_ODR,#5       ; digit ..2.
      0081BC 20 08            [ 2]   66         JRA     3$
                                     67 
      0081BE A1 02            [ 1]   68 2$:     CP      A,#2
      0081C0 26 30            [ 1]   69         JRNE    4$
      0081C2 72 19 50 05      [ 1]   70         BRES    PB_ODR,#4       ; digit ...1
                                     71         ; fall through
                                     72 
      0081C6 5F               [ 1]   73 3$:     CLRW    X
      0081C7 97               [ 1]   74         LD      XL,A
      0081C8 E6 42            [ 1]   75         LD      A,(LED7LAST-2,X)
                                     76 
                                     77         ; W1209 7S LED display row
                                     78         ; bit 76453210 input (parameter A)
                                     79         ;  PA .....FB.
                                     80         ;  PC CG......
                                     81         ;  PD ..A.DPE.
      0081CA 46               [ 1]   82         RRC     A
      0081CB 90 1B 50 0F      [ 1]   83         BCCM    PD_ODR,#5       ; A
      0081CF 46               [ 1]   84         RRC     A
      0081D0 90 15 50 00      [ 1]   85         BCCM    PA_ODR,#2       ; B
      0081D4 46               [ 1]   86         RRC     A
      0081D5 90 1F 50 0A      [ 1]   87         BCCM    PC_ODR,#7       ; C
      0081D9 46               [ 1]   88         RRC     A
      0081DA 90 17 50 0F      [ 1]   89         BCCM    PD_ODR,#3       ; D
      0081DE 46               [ 1]   90         RRC     A
      0081DF 90 13 50 0F      [ 1]   91         BCCM    PD_ODR,#1       ; E
      0081E3 46               [ 1]   92         RRC     A
      0081E4 90 13 50 00      [ 1]   93         BCCM    PA_ODR,#1       ; F
      0081E8 46               [ 1]   94         RRC     A
      0081E9 90 1D 50 0A      [ 1]   95         BCCM    PC_ODR,#6       ; G
      0081ED 46               [ 1]   96         RRC     A
      0081EE 90 15 50 0F      [ 1]   97         BCCM    PD_ODR,#2       ; P
                                     98 
      0081F2 81               [ 4]   99 4$:     RET
                                    100         .endif
                                    101 
                                    102 ;===============================================================
                                    103 
                           000001   104         .ifne   HAS_OUTPUTS
                                    105 ;       OUT!  ( c -- )
                                    106 ;       Put c to board outputs, storing a copy in OUTPUTS
      0081F3 80 EA                  107         .dw     LINK
                                    108 
                           00016A   109         LINK =  .
      0081F5 04                     110         .db     (4)
      0081F6 4F 55 54 21            111         .ascii  "OUT!"
      0081FA                        112 OUTSTOR:
      0081FA 5C               [ 1]  113         INCW    X
      0081FB F6               [ 1]  114         LD      A,(X)
      0081FC B7 41            [ 1]  115         LD      OUTPUTS+1,A
      0081FE 5C               [ 1]  116         INCW    X
      0081FF 46               [ 1]  117         RRC     A
      008200 90 17 50 00      [ 1]  118         BCCM    PA_ODR,#3       ; W1209 relay
      008204 81               [ 4]  119         RET
                                    120         .endif
                                    121 
                                    122 ;===============================================================
                                    123 
                           000001   124         .ifne   HAS_KEYS
                                    125 ;       BKEY  ( -- c )     ( TOS STM8: -- A,Z,N )
                                    126 ;       Read board key state as a bitfield
      008205 81 F5                  127         .dw     LINK
                                    128 
                           00017C   129         LINK =  .
      008207 04                     130         .db     (4)
      008208 42 4B 45 59            131         .ascii  "BKEY"
      00820C                        132 BKEY:
                                    133         ; Keys "set" (1), "+" (2), and "-" (4) on PC.3:5
      00820C C6 50 0B         [ 1]  134         LD      A,PC_IDR
      00820F 48               [ 1]  135         SLA     A
      008210 4E               [ 1]  136         SWAP    A
      008211 43               [ 1]  137         CPL     A
      008212 A4 07            [ 1]  138         AND     A,#0x07
      008214 CC 85 43         [ 2]  139         JP      ASTOR
                                    140 
                                    141 ;       BKEYC  ( -- c )   ( TOS STM8: -- A,Z,N )
                                    142 ;       Read and translate board dependent key bitmap into char
                                    143 
      008217                        144 BKEYCHAR:
      008217 AD F3            [ 4]  145         CALLR   BKEY
      008219 27 04            [ 1]  146         JREQ    1$
      00821B AB 40            [ 1]  147         ADD     A,#'@'
      00821D E7 01            [ 1]  148         LD      (1,X),A
      00821F 81               [ 4]  149 1$:     RET
                                    150        .endif
                                    151 
                                    544 ;       ##############################################
                                    545 
                                    546 ;       'BOOT   ( -- a )
                                    547 ;       The application startup vector and NVM USR setting array
                                    548 
      000195                        549         HEADER  TBOOT "'BOOT"
                           000001     1         .ifeq   UNLINK_TBOOT
      008220 82 07                    2         .dw     LINK
                           000197     3         LINK    = .
      008222 05                       4         .db      (102$ - 101$)
      008223                          5 101$:
      008223 27 42 4F 4F 54           6         .ascii  "'BOOT"
      008228                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008228                        550 TBOOT:
      008228 CD 84 26         [ 4]  551         CALL    DOVAR
                           0001A0   552         UBOOT = .
      00822B 8D EA                  553         .dw     HI              ;application to boot
                                    554 
                                    555         ; COLD start initiates these variables.
                           0001A2   556         UZERO = .
                           000000   557         .ifge   (HAS_TXUART-HAS_TXSIM)
                                    558         .dw     TXSTOR          ; TX! as EMIT vector
                                    559         .dw     QRX             ; ?KEY as ?KEY vector
                           000001   560         .else
      00822D 82 62                  561         .dw     TXPSTOR         ; TXP! as EMIT vector if (HAS_TXSIM > HAS_TXUART)
      00822F 82 55                  562         .dw     QRXP            ; ?RXP as ?KEY vector
                                    563         .endif
      008231 00 0A                  564         .dw     BASEE           ; BASE
      008233 8D 8C                  565         .dw     INTER           ; 'EVAL
      008235 8D D8                  566         .dw     DOTOK           ; 'PROMPT
                           0001AC   567         COLDCTOP = .
      008237 00 80                  568         .dw     CTOP            ; CP in RAM
                           0001AE   569         COLDCONTEXT = .
      008239 94 6F                  570         .dw     LASTN           ; USRLAST
                           000001   571         .ifne   HAS_CPNVM
                           0001B0   572         COLDNVMCP = .
      00823B 94 79                  573         .dw     END_SDCC_FLASH  ; CP in NVM
                           0001B2   574         ULAST = .
                                    575 
                                    576         ; Second copy of USR setting for NVM reset
                           0001B2   577         UDEFAULTS = .
      00823D 8D EA                  578         .dw     HI              ; 'BOOT
                           000000   579         .ifge   (HAS_TXUART-HAS_TXSIM)
                                    580         .dw     TXSTOR          ; TX! as EMIT vector
                                    581         .dw     QRX             ; ?KEY as ?KEY vector
                           000001   582         .else
      00823F 82 62                  583         .dw     TXPSTOR         ; TXP! as EMIT vector
      008241 82 55                  584         .dw     QRXP            ; ?RXP as ?KEY vector
                                    585         .endif
      008243 00 0A                  586         .dw     BASEE           ; BASE
      008245 8D 8C                  587         .dw     INTER           ; 'EVAL
      008247 8D D8                  588         .dw     DOTOK           ; 'PROMPT
      008249 00 80                  589         .dw     CTOP            ; CP in RAM
      00824B 94 6F                  590         .dw     LASTN           ; CONTEXT pointer
      00824D 94 79                  591         .dw     END_SDCC_FLASH  ; CP in NVM
                           000000   592         .else
                                    593         ULAST = .
                                    594         .endif
                                    595 
                                    596 ; ==============================================
                                    597 ;       Device dependent I/O
                                    598 
                           000000   599         .ifne   HAS_RXUART
                                    600 ;       ?RX     ( -- c T | F )  ( TOS STM8: -- Y,Z,N )
                                    601 ;       Return serial interface input char from and true, or false.
                                    602 
                                    603         .ifeq   BAREBONES
                                    604         HEADER  QRX "?RX"
                                    605         .endif
                                    606 QRX:
                                    607         CLR     A               ; A: flag false
                                    608         BTJF    UART_SR,#5,1$
                                    609         LD      A,UART_DR      ; get char in A
                                    610 1$:     JP      ATOKEY          ; push char or flag false
                                    611         .endif
                                    612 
                                    613 
                           000000   614         .ifne   HAS_TXUART
                                    615 ;       TX!     ( c -- )
                                    616 ;       Send character c to the serial interface.
                                    617 
                                    618         .ifeq   BAREBONES
                                    619         HEADER  TXSTOR "TX!"
                                    620         .endif
                                    621 TXSTOR:
                                    622         INCW    X
                                    623         LD      A,(X)
                                    624         INCW    X
                                    625 
                                    626         .ifne   HALF_DUPLEX * (1-HAS_TXSIM)
                                    627         ; HALF_DUPLEX with normal UART (e.g. wired-or Rx and Tx)
                                    628 1$:     BTJF    UART_SR,#7,1$  ; loop until tdre
                                    629         BRES    UART_CR2,#2    ; disable rx
                                    630         LD      UART_DR,A      ; send A
                                    631 2$:     BTJF    UART_SR,#6,2$  ; loop until tc
                                    632         BSET    UART_CR2,#2    ; enable rx
                                    633         .else                  ; not HALF_DUPLEX
                                    634 1$:     BTJF    UART_SR,#7,1$  ; loop until tdre
                                    635         LD      UART_DR,A      ; send A
                                    636         .endif
                                    637         RET
                                    638         .endif
                                    639 
                                    640 ;       Simulated serial I/O
                                    641 ;       either full or half duplex
                                    642 
                           000000   643         .ifeq  HAS_TXSIM + HAS_RXSIM
                                    644 _TIM4_IRQHandler:
                                    645         ; dummy for linker - can be overwritten by Forth application
                           000001   646         .else
                                    647         ; include required serial I/O code
                           000001   648         .ifne  PNRX^PNTX
                                    649         .include "sser_fdx.inc" ; Full Duplex serial
                                      1 ;--------------------------------------------------------
                                      2 ;       STM8EF for STM8S (Value line and Access Line devices)
                                      3 ;       Simulated serial I/O - two GPIOs - Full Duplex
                                      4 ;--------------------------------------------------------
                                      5 
                           000001     6         .ifne   HAS_TXSIM ;+ HAS_RXSIM
      0001C4                          7         RamByte TIM4RCNT        ; TIM4 RX interrupt counter
                           00004A     1         TIM4RCNT = RAMPOOL
                           00004B     2         RAMPOOL = RAMPOOL + 1
      0001C4                          8         RamByte TIM4TCNT        ; TIM4 TX interrupt counter
                           00004B     1         TIM4TCNT = RAMPOOL
                           00004C     2         RAMPOOL = RAMPOOL + 1
      0001C4                          9         RamByte TIM4TXREG       ; TIM4 TX transmit buffer and shift register
                           00004C     1         TIM4TXREG = RAMPOOL
                           00004D     2         RAMPOOL = RAMPOOL + 1
      0001C4                         10         RamByte TIM4RXREG       ; TIM4 RX shift register
                           00004D     1         TIM4RXREG = RAMPOOL
                           00004E     2         RAMPOOL = RAMPOOL + 1
      0001C4                         11         RamByte TIM4RXBUF       ; TIM4 RX receive buffer
                           00004E     1         TIM4RXBUF = RAMPOOL
                           00004F     2         RAMPOOL = RAMPOOL + 1
                                     12         .endif
                                     13 
                           000001    14         .ifne   HAS_RXSIM
                                     15 ;       ?RXP     ( -- c T | F )  ( TOS STM8: -- Y,Z,N )
                                     16 ;       Return char from a simulated serial interface and true, or false.
                                     17 
                           000001    18         .ifeq   BAREBONES
                           000000    19         .ifne   HAS_RXUART
                                     20         HEADER  QRXP "?RXP"
                           000001    21         .else
      0001C4                         22         HEADER  QRX "?RX"
                           000001     1         .ifeq   UNLINK_QRX
      00824F 82 22                    2         .dw     LINK
                           0001C6     3         LINK    = .
      008251 03                       4         .db      (102$ - 101$)
      008252                          5 101$:
      008252 3F 52 58                 6         .ascii  "?RX"
      008255                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                     23         .endif
                                     24         .endif
                           000001    25         .ifeq   HAS_RXUART
      008255                         26 QRX:
                                     27         .endif
      008255                         28 QRXP:
      008255 4F               [ 1]   29         CLR     A
      008256 31 00 4E         [ 3]   30         EXG     A,TIM4RXBUF     ; read and consume char
      008259 CC 85 4A         [ 2]   31         JP      ATOKEY
                                     32         .endif
                                     33 
                           000001    34         .ifne   HAS_TXSIM
                                     35 ;       TXP!     ( c -- )
                                     36 ;       Send character c to a simulated serial interface.
                                     37 
                           000001    38         .ifeq   BAREBONES
                           000000    39         .ifne   HAS_TXUART
                                     40         HEADER  TXPSTOR "TXP!"
                           000001    41         .else
      0001D1                         42         HEADER  TXSTOR "TX!"
                           000001     1         .ifeq   UNLINK_TXSTOR
      00825C 82 51                    2         .dw     LINK
                           0001D3     3         LINK    = .
      00825E 03                       4         .db      (102$ - 101$)
      00825F                          5 101$:
      00825F 54 58 21                 6         .ascii  "TX!"
      008262                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                     43         .endif
                                     44         .endif
                                     45 
                           000001    46         .ifeq   HAS_TXUART
      008262                         47 TXSTOR:
                                     48         .endif
      008262                         49 TXPSTOR:
      008262 5C               [ 1]   50         INCW    X
      008263 F6               [ 1]   51         LD      A,(X)
      008264 5C               [ 1]   52         INCW    X
                                     53 
      008265 3D 4B            [ 1]   54 1$:     TNZ     TIM4TCNT
      008267 26 FC            [ 1]   55         JRNE    1$              ; wait for TIM4 TX complete
                                     56 
      008269 B7 4C            [ 1]   57         LD      TIM4TXREG,A     ; char to TXSIM output register
      00826B 35 0A 00 4B      [ 1]   58         MOV     TIM4TCNT,#10    ; init next transfer
      00826F 3D 4A            [ 1]   59         TNZ     TIM4RCNT        ; test if RX already uses TIM4
      008271 26 08            [ 1]   60         JRNE    2$
      008273 72 5F 53 46      [ 1]   61         CLR     TIM4_CNTR       ; reset TIM4, trigger update interrupt
      008277 72 10 53 43      [ 1]   62         BSET    TIM4_IER,#0     ; enable TIM4 interrupt
      00827B                         63 2$:
      00827B 81               [ 4]   64         RET
                                     65         .endif
                                     66 
                                     67 ;       RxD through GPIO start-bit interrupt handler
                                     68 
                           000001    69         .ifne   HAS_RXSIM
                                     70 
                           000000    71         .ifeq   PSIM-PORTA
                                     72 _EXTI0_IRQHandler:
                                     73         .endif
                                     74 
                           000000    75         .ifeq   PSIM-PORTB
                                     76 _EXTI1_IRQHandler:
                                     77         .endif
                                     78 
                           000001    79         .ifeq   PSIM-PORTC
      00827C                         80 _EXTI2_IRQHandler:
                                     81         .endif
                                     82 
                           000000    83         .ifeq   PSIM-PORTD
                                     84 _EXTI3_IRQHandler:
                                     85         .endif
                                     86 
      00827C 72 19 50 0E      [ 1]   87         BRES    PSIM+CR2,#PNRX  ; disable PNRX external interrupt
                                     88 
      008280 35 09 00 4A      [ 1]   89         MOV     TIM4RCNT,#9     ; set sequence counter for RX
                                     90 
                                     91         ; Set-up Rx sampling at quarter bit time (compromise with TX)
      008284 35 33 53 46      [ 1]   92         MOV     TIM4_CNTR,#(TIM4RELOAD/4)
      008288 72 11 53 44      [ 1]   93         BRES    TIM4_SR,#0      ; clear TIM4 UIF
      00828C 72 10 53 43      [ 1]   94         BSET    TIM4_IER,#0     ; enable TIM4 interrupt
      008290 80               [11]   95         IRET
                                     96         .endif
                                     97 
      008291                         98 _TIM4_IRQHandler:
                                     99         ; TIM4 interrupt handler for software Rx/Tx
      008291 72 11 53 44      [ 1]  100         BRES    TIM4_SR,#0      ; clear TIM4 UIF
                                    101 
      008295 B6 4A            [ 1]  102         LD      A,TIM4RCNT      ; test receive step counter
      008297 27 12            [ 1]  103         JREQ    TIM4_TESTTRANS  ; nothing to do - check for transmit
                                    104 
                                    105         ; Receive a bit
      008299 72 08 50 0B 00   [ 2]  106         BTJT    PSIM+IDR,#PNRX,1$ ; dummy branch, copy GPIO to CF
      00829E 36 4D            [ 1]  107 1$:     RRC     TIM4RXREG
      0082A0 3A 4A            [ 1]  108         DEC     TIM4RCNT
      0082A2 26 07            [ 1]  109         JRNE    TIM4_TESTTRANS
                                    110 
                                    111         ; Receive sequence complete
      0082A4 45 4D 4E         [ 1]  112         MOV     TIM4RXBUF,TIM4RXREG ; save result
      0082A7 72 18 50 0E      [ 1]  113         BSET    PSIM+CR2,#PNRX  ; enable PNRX external interrupt
                                    114         ; fall through
                                    115 
      0082AB                        116 TIM4_TESTTRANS:
      0082AB B6 4B            [ 1]  117         LD      A,TIM4TCNT      ; test transmit step counter
      0082AD 27 10            [ 1]  118         JREQ    TIM4_TESTOFF
                                    119         ; fall through
                                    120 
      0082AF                        121 TIM4_TRANS:
      0082AF A1 0A            [ 1]  122         CP      A,#10           ; startbit? (also sets CF)
      0082B1 26 02            [ 1]  123         JRNE    TIM4_TRANSSER
      0082B3 20 02            [ 2]  124         JRA     TIM4_TRANSBIT        ; emit start bit (CF=0 from "CP A")
                                    125 
      0082B5                        126         TIM4_TRANSSER:
      0082B5 36 4C            [ 1]  127         RRC     TIM4TXREG       ; get data bit, shift in stop bit (CF=1 from "CP A")
                                    128         ; fall through
                                    129 
      0082B7                        130 TIM4_TRANSBIT:
      0082B7 90 1B 50 0A      [ 1]  131         BCCM    PSIM+ODR,#PNTX  ; Set GPIO to CF
      0082BB 3A 4B            [ 1]  132         DEC     TIM4TCNT        ; next TXD TIM4 state
      0082BD 26 0C            [ 1]  133         JRNE    TIM4_END        ; not complete unless TIM4TCNT is zero
                                    134         ; fall through
                                    135 
      0082BF                        136 TIM4_TESTOFF:
      0082BF B6 4A            [ 1]  137         LD      A,TIM4RCNT
      0082C1 26 08            [ 1]  138         JRNE    TIM4_END
      0082C3 72 1A 50 0A      [ 1]  139         BSET    PSIM+ODR,#PNTX  ; set TX GPIO to STOP
      0082C7 72 11 53 43      [ 1]  140         BRES    TIM4_IER,#0     ; disable TIM4 interrupt
                                    141         ; fall through
                                    142 
      0082CB                        143 TIM4_END:
      0082CB 80               [11]  144         IRET
                                    145 
                           000000   650         .else
                                    651         .include "sser_hdx.inc" ; Half Duplex serial
                                    652         .endif
                                    653         .endif
                                    654 
                                    655 ; ==============================================
                                    656 ;       Device independent I/O
                                    657 
                                    658 ;       ?KEY    ( -- c T | F )  ( TOS STM8: -- Y,Z,N )
                                    659 ;       Return input char and true, or false.
      000241                        660         HEADER  QKEY "?KEY"
                           000001     1         .ifeq   UNLINK_QKEY
      0082CC 82 5E                    2         .dw     LINK
                           000243     3         LINK    = .
      0082CE 04                       4         .db      (102$ - 101$)
      0082CF                          5 101$:
      0082CF 3F 4B 45 59              6         .ascii  "?KEY"
      0082D3                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0082D3                        661 QKEY:
      0082D3 92 CC 62         [ 5]  662         JP      [USRQKEY]
                                    663 
                                    664 ;       EMIT    ( c -- )
                                    665 ;       Send character c to output device.
                                    666 
      00024B                        667         HEADER  EMIT "EMIT"
                           000001     1         .ifeq   UNLINK_EMIT
      0082D6 82 CE                    2         .dw     LINK
                           00024D     3         LINK    = .
      0082D8 04                       4         .db      (102$ - 101$)
      0082D9                          5 101$:
      0082D9 45 4D 49 54              6         .ascii  "EMIT"
      0082DD                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0082DD                        668 EMIT:
      0082DD 92 CC 60         [ 5]  669         JP      [USREMIT]
                                    670 
                                    671 ; ==============================================
                                    672 ; The kernel
                                    673 
                                    674 ;       PUSHLIT ( - C )
                                    675 ;       Subroutine for DOLITC and CCOMMALIT
      0082E0                        676 PUSHLIT:
      0082E0 16 03            [ 2]  677         LDW     Y,(3,SP)
      0082E2 5A               [ 2]  678         DECW    X               ; LSB = literal
      0082E3 90 F6            [ 1]  679         LD      A,(Y)
      0082E5 F7               [ 1]  680         LD      (X),A
      0082E6 5A               [ 2]  681         DECW    X               ; MSB = 0
      0082E7 4F               [ 1]  682         CLR     A
      0082E8 F7               [ 1]  683         LD      (X),A
      0082E9 81               [ 4]  684         RET
                                    685 
                                    686 ;       CCOMMALIT ( - )
                                    687 ;       Compile inline literall byte into code dictionary.
      0082EA                        688 CCOMMALIT:
      0082EA AD F4            [ 4]  689         CALLR   PUSHLIT
      0082EC CD 8E 61         [ 4]  690         CALL    CCOMMA
      0082EF                        691 CSKIPRET:
      0082EF 90 85            [ 2]  692         POPW    Y
      0082F1 90 EC 01         [ 2]  693         JP      (1,Y)
                                    694 
                           000000   695         .ifne   USE_CALLDOLIT
                                    696 
                                    697 ;       DOLITC  ( - C )
                                    698 ;       Push an inline literal character (8 bit).
                                    699 DOLITC:
                                    700         CALLR   PUSHLIT
                                    701         JRA     CSKIPRET
                                    702 
                                    703 ;       doLit   ( -- w )
                                    704 ;       Push an inline literal.
                                    705 
                                    706         .ifne   WORDS_LINKRUNTI
                                    707         HEADFLG DOLIT "doLit" COMPO
                                    708         .endif
                                    709 DOLIT:
                                    710         DECW    X               ;SUBW   X,#2
                                    711         DECW    X
                                    712 
                                    713         LDW     Y,(1,SP)
                                    714         LDW     Y,(Y)
                                    715         LDW     (X),Y
                                    716         JRA     POPYJPY
                                    717         .endif
                                    718 
                           000001   719         .ifeq   BOOTSTRAP
                           000001   720         .ifne   HAS_DOLOOP
                                    721 ;       (+loop) ( +n -- )
                                    722 ;       Add n to index R@ and test for lower than limit (R-CELL)@.
                                    723 
                           000000   724         .ifne   WORDS_LINKRUNTI
                                    725         HEADFLG DOPLOOP "(+loop)" COMPO
                                    726         .endif
      0082F4                        727 DOPLOOP:
      0082F4 16 05            [ 2]  728         LDW     Y,(5,SP)
      0082F6 90 BF 7E         [ 2]  729         LDW     YTEMP,Y
      0082F9 90 93            [ 1]  730         LDW     Y,X
      0082FB 90 FE            [ 2]  731         LDW     Y,(Y)
      0082FD 90 9E            [ 1]  732         LD      A,YH
      0082FF 5C               [ 1]  733         INCW    X
      008300 5C               [ 1]  734         INCW    X
      008301 72 F9 03         [ 2]  735         ADDW    Y,(3,SP)
      008304 90 B3 7E         [ 2]  736         CPW     Y,YTEMP
      008307 8A               [ 1]  737         PUSH    CC
      008308 4D               [ 1]  738         TNZ     A
      008309 2B 05            [ 1]  739         JRMI    1$
      00830B 86               [ 1]  740         POP     CC
      00830C 2E 11            [ 1]  741         JRSGE   LEAVE
      00830E 20 03            [ 2]  742         JRA     2$
      008310 86               [ 1]  743 1$:     POP     CC
      008311 2F 0C            [ 1]  744         JRSLT   LEAVE
      008313 17 03            [ 2]  745 2$:     LDW     (3,SP),Y
      008315 20 32            [ 2]  746         JRA     BRAN
                                    747 
                                    748 ;       LEAVE   ( -- )
                                    749 ;       Leave a DO .. LOOP/+LOOP loop.
                                    750 
      00028C                        751         HEADFLG LEAVE "LEAVE" COMPO
                           000001     1         .ifeq   UNLINK_LEAVE
      008317 82 D8                    2         .dw     LINK
                           00028E     3         LINK    = .
      008319 45                       4         .db      ((102$ - 101$) + COMPO)
      00831A                          5 101$:
      00831A 4C 45 41 56 45           6         .ascii  "LEAVE"
      00831F                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00831F                        752 LEAVE:
      00831F 5B 06            [ 2]  753         ADDW    SP,#6
      008321 90 85            [ 2]  754         POPW    Y               ; DO leaves the address of +loop on the R-stack
      008323 90 EC 02         [ 2]  755         JP      (2,Y)
                                    756         .endif
                                    757         .endif
                                    758 
                                    759 ;       next    ( -- )
                                    760 ;       Code for single index loop.
                                    761 
                           000000   762         .ifne   WORDS_LINKRUNTI
                                    763         HEADFLG DONXT "donxt" COMPO
                                    764         .endif
      008326                        765 DONXT:
      008326 16 03            [ 2]  766         LDW     Y,(3,SP)
      008328 90 5A            [ 2]  767         DECW    Y
      00832A 2A 07            [ 1]  768         JRPL    NEX1
      00832C 90 85            [ 2]  769         POPW    Y
      00832E 84               [ 1]  770         POP     A
      00832F 84               [ 1]  771         POP     A
      008330 90 EC 02         [ 2]  772         JP      (2,Y)
      008333 17 03            [ 2]  773 NEX1:   LDW     (3,SP),Y
      008335 20 12            [ 2]  774         JRA     BRAN
                                    775 
                                    776 ;       QDQBRAN     ( n - n )
                                    777 ;       QDUP QBRANCH phrase
      008337                        778 QDQBRAN:
      008337 CD 85 8F         [ 4]  779         CALL    QDUP
      00833A 20 00            [ 2]  780         JRA     QBRAN
                                    781 
                                    782 ;       ?branch ( f -- )
                                    783 ;       Branch if flag is zero.
                                    784 
                           000000   785         .ifne   WORDS_LINKRUNTI
                                    786         HEADFLG QBRAN "?branch" COMPO
                                    787         .endif
      00833C                        788 QBRAN:
      00833C 90 93            [ 1]  789         LDW     Y,X
      00833E 5C               [ 1]  790         INCW    X
      00833F 5C               [ 1]  791         INCW    X
      008340 90 FE            [ 2]  792         LDW     Y,(Y)
      008342 27 05            [ 1]  793         JREQ    BRAN
      008344                        794 POPYJPY:
      008344 90 85            [ 2]  795         POPW    Y
      008346 90 EC 02         [ 2]  796         JP      (2,Y)
                                    797 
                                    798 ;       branch  ( -- )
                                    799 ;       Branch to an inline address.
                                    800 
                           000000   801         .ifne   WORDS_LINKRUNTI
                                    802         HEADFLG BRAN "branch" COMPO
                                    803         .endif
      008349                        804 BRAN:
      008349 90 85            [ 2]  805         POPW    Y
      00834B                        806 YJPIND:
      00834B 90 FE            [ 2]  807         LDW     Y,(Y)
      00834D 90 FC            [ 2]  808         JP      (Y)
                                    809 
                                    810 
                                    811 ;       EXECUTE ( ca -- )
                                    812 ;       Execute word at ca.
                                    813 
      0002C4                        814         HEADER  EXECU "EXECUTE"
                           000001     1         .ifeq   UNLINK_EXECU
      00834F 83 19                    2         .dw     LINK
                           0002C6     3         LINK    = .
      008351 07                       4         .db      (102$ - 101$)
      008352                          5 101$:
      008352 45 58 45 43 55 54 45     6         .ascii  "EXECUTE"
      008359                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008359                        815 EXECU:
      008359 90 93            [ 1]  816         LDW     Y,X
      00835B 5C               [ 1]  817         INCW    X
      00835C 5C               [ 1]  818         INCW    X
      00835D 20 EC            [ 2]  819         JRA     YJPIND
                                    820 
                           000001   821         .ifeq   REMOVE_EXIT
                                    822 ;       EXIT    ( -- )
                                    823 ;       Terminate a colon definition.
                                    824 
      0002D4                        825         HEADER  EXIT "EXIT"
                           000001     1         .ifeq   UNLINK_EXIT
      00835F 83 51                    2         .dw     LINK
                           0002D6     3         LINK    = .
      008361 04                       4         .db      (102$ - 101$)
      008362                          5 101$:
      008362 45 58 49 54              6         .ascii  "EXIT"
      008366                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008366                        826 EXIT:
      008366 90 85            [ 2]  827         POPW    Y
      008368 81               [ 4]  828         RET
                                    829         .endif
                                    830 
                           000001   831         .ifeq   BOOTSTRAP
                                    832 ;       2!      ( d a -- )      ( TOS STM8: -- Y,Z,N )
                                    833 ;       Store double integer to address a.
                                    834 
      0002DE                        835         HEADER  DSTOR "2!"
                           000001     1         .ifeq   UNLINK_DSTOR
      008369 83 61                    2         .dw     LINK
                           0002E0     3         LINK    = .
      00836B 02                       4         .db      (102$ - 101$)
      00836C                          5 101$:
      00836C 32 21                    6         .ascii  "2!"
      00836E                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00836E                        836 DSTOR:
      00836E CD 84 7C         [ 4]  837         CALL    SWAPP
      008371 CD 84 91         [ 4]  838         CALL    OVER
      008374 AD 6E            [ 4]  839         CALLR   STORE
      008376 CD 87 A0         [ 4]  840         CALL    CELLP
      008379 20 69            [ 2]  841         JRA     STORE
                                    842         .endif
                                    843 
                           000001   844         .ifeq   BOOTSTRAP
                                    845 ;       2@      ( a -- d )
                                    846 ;       Fetch double integer from address a.
                                    847 
      0002F0                        848         HEADER  DAT "2@"
                           000001     1         .ifeq   UNLINK_DAT
      00837B 83 6B                    2         .dw     LINK
                           0002F2     3         LINK    = .
      00837D 02                       4         .db      (102$ - 101$)
      00837E                          5 101$:
      00837E 32 40                    6         .ascii  "2@"
      008380                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008380                        849 DAT:
      008380 CD 84 6F         [ 4]  850         CALL    DUPP
      008383 CD 87 A0         [ 4]  851         CALL    CELLP
      008386 AD 51            [ 4]  852         CALLR   AT
      008388 CD 84 7C         [ 4]  853         CALL    SWAPP
      00838B 20 4C            [ 2]  854         JRA     AT
                                    855         .endif
                                    856 
                                    857 
                           000001   858         .ifne   WORDS_EXTRAMEM
                                    859 ;       2C!  ( n b -- )
                                    860 ;       Store word C-wise to 16 bit HW registers "MSB first"
                                    861 
      000302                        862         HEADER  DCSTOR "2C!"
                           000001     1         .ifeq   UNLINK_DCSTOR
      00838D 83 7D                    2         .dw     LINK
                           000304     3         LINK    = .
      00838F 03                       4         .db      (102$ - 101$)
      008390                          5 101$:
      008390 32 43 21                 6         .ascii  "2C!"
      008393                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008393                        863 DCSTOR:
      008393 CD 85 B1         [ 4]  864         CALL    DDUP
      008396 E6 02            [ 1]  865         LD      A,(2,X)
      008398 E7 03            [ 1]  866         LD      (3,X),A
      00839A AD 68            [ 4]  867         CALLR   CSTOR
      00839C CD 87 B3         [ 4]  868         CALL    ONEP
      00839F 20 63            [ 2]  869         JRA     CSTOR
                                    870 
                                    871 
                                    872 ;       2C@  ( a -- n )
                                    873 ;       Fetch word C-wise from 16 bit HW config. registers "MSB first"
                                    874 
      000316                        875         HEADER  DCAT "2C@"
                           000001     1         .ifeq   UNLINK_DCAT
      0083A1 83 8F                    2         .dw     LINK
                           000318     3         LINK    = .
      0083A3 03                       4         .db      (102$ - 101$)
      0083A4                          5 101$:
      0083A4 32 43 40                 6         .ascii  "2C@"
      0083A7                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0083A7                        876 DCAT:
      0083A7 CD 87 B7         [ 4]  877         CALL    DOXCODE
      0083AA 90 93            [ 1]  878         LDW     Y,X
      0083AC F6               [ 1]  879         LD      A,(X)
      0083AD 95               [ 1]  880         LD      XH,A
      0083AE 90 E6 01         [ 1]  881         LD      A,(1,Y)
      0083B1 97               [ 1]  882         LD      XL,A
      0083B2 81               [ 4]  883         RET
                                    884 
                                    885 ;       B! ( t a u -- )
                                    886 ;       Set/reset bit #u (0..7) in the byte at address a to bool t
                                    887 ;       Note: creates/executes BSER/BRES + RET code on Data Stack
      000328                        888         HEADER  BRSS "B!"
                           000001     1         .ifeq   UNLINK_BRSS
      0083B3 83 A3                    2         .dw     LINK
                           00032A     3         LINK    = .
      0083B5 02                       4         .db      (102$ - 101$)
      0083B6                          5 101$:
      0083B6 42 21                    6         .ascii  "B!"
      0083B8                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0083B8                        889 BRSS:
      0083B8 A6 72            [ 1]  890         LD      A,#0x72         ; Opcode BSET/BRES
      0083BA F7               [ 1]  891         LD      (X),A
      0083BB E6 01            [ 1]  892         LD      A,(1,X)         ; 2nd byte of BSET/BRES
      0083BD 48               [ 1]  893         SLA     A               ; n *= 2 -> A
      0083BE AA 10            [ 1]  894         OR      A,#0x10
      0083C0 90 93            [ 1]  895         LDW     Y,X
      0083C2 90 EE 04         [ 2]  896         LDW     Y,(4,Y)         ; bool b (0..15) -> Z
      0083C5 26 01            [ 1]  897         JRNE    1$              ; b!=0: BSET
      0083C7 4C               [ 1]  898         INC     A               ; b==0: BRES
      0083C8 E7 01            [ 1]  899 1$:     LD      (1,X),A
      0083CA A6 81            [ 1]  900         LD      A,#EXIT_OPC     ; Opcode RET
      0083CC E7 04            [ 1]  901         LD      (4,X),A
      0083CE 90 93            [ 1]  902         LDW     Y,X
      0083D0 1C 00 06         [ 2]  903         ADDW    X,#6
      0083D3 90 FC            [ 2]  904         JP      (Y)
                                    905 
                                    906         .endif
                                    907 
                                    908 
                                    909 ;       @       ( a -- w )      ( TOS STM8: -- Y,Z,N )
                                    910 ;       Push memory location to stack.
                                    911 
      00034A                        912         HEADER  AT "@"
                           000001     1         .ifeq   UNLINK_AT
      0083D5 83 B5                    2         .dw     LINK
                           00034C     3         LINK    = .
      0083D7 01                       4         .db      (102$ - 101$)
      0083D8                          5 101$:
      0083D8 40                       6         .ascii  "@"
      0083D9                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0083D9                        913 AT:
      0083D9 90 93            [ 1]  914         LDW     Y,X
      0083DB FE               [ 2]  915         LDW     X,(X)
      0083DC FE               [ 2]  916         LDW     X,(X)
      0083DD 51               [ 1]  917         EXGW    X,Y
      0083DE FF               [ 2]  918         LDW     (X),Y
      0083DF 81               [ 4]  919         RET
                                    920 
                                    921 ;       !       ( w a -- )      ( TOS STM8: -- Y,Z,N )
                                    922 ;       Pop data stack to memory.
                                    923 
      000355                        924         HEADER  STORE "!"
                           000001     1         .ifeq   UNLINK_STORE
      0083E0 83 D7                    2         .dw     LINK
                           000357     3         LINK    = .
      0083E2 01                       4         .db      (102$ - 101$)
      0083E3                          5 101$:
      0083E3 21                       6         .ascii  "!"
      0083E4                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0083E4                        925 STORE:
      0083E4 90 93            [ 1]  926         LDW     Y,X
      0083E6 90 FE            [ 2]  927         LDW     Y,(Y)
      0083E8 89               [ 2]  928         PUSHW   X
      0083E9 EE 02            [ 2]  929         LDW     X,(2,X)
      0083EB 90 FF            [ 2]  930         LDW     (Y),X
      0083ED 85               [ 2]  931         POPW    X
      0083EE 20 75            [ 2]  932         JRA     DDROP
                                    933 
                                    934 ;       C@      ( b -- c )      ( TOS STM8: -- A,Z,N )
                                    935 ;       Push byte in memory to stack.
                                    936 ;       STM8: Z,N
                                    937 
      000365                        938         HEADER  CAT "C@"
                           000001     1         .ifeq   UNLINK_CAT
      0083F0 83 E2                    2         .dw     LINK
                           000367     3         LINK    = .
      0083F2 02                       4         .db      (102$ - 101$)
      0083F3                          5 101$:
      0083F3 43 40                    6         .ascii  "C@"
      0083F5                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0083F5                        939 CAT:
      0083F5 90 93            [ 1]  940         LDW     Y,X             ; Y=b
      0083F7 90 FE            [ 2]  941         LDW     Y,(Y)
      0083F9                        942 YCAT:
      0083F9 90 F6            [ 1]  943         LD      A,(Y)
      0083FB 7F               [ 1]  944         CLR     (X)
      0083FC E7 01            [ 1]  945         LD      (1,X),A
      0083FE 81               [ 4]  946         RET
                                    947 
                                    948 ;       C!      ( c b -- )
                                    949 ;       Pop     data stack to byte memory.
                                    950 
      000374                        951         HEADER  CSTOR "C!"
                           000001     1         .ifeq   UNLINK_CSTOR
      0083FF 83 F2                    2         .dw     LINK
                           000376     3         LINK    = .
      008401 02                       4         .db      (102$ - 101$)
      008402                          5 101$:
      008402 43 21                    6         .ascii  "C!"
      008404                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008404                        952 CSTOR:
      008404 90 93            [ 1]  953         LDW     Y,X
      008406 90 FE            [ 2]  954         LDW     Y,(Y)           ; Y=b
      008408 E6 03            [ 1]  955         LD      A,(3,X)         ; D = c
      00840A 90 F7            [ 1]  956         LD      (Y),A           ; store c at b
      00840C 20 57            [ 2]  957         JRA     DDROP
                                    958 
                                    959 ;       R>      ( -- w )     ( TOS STM8: -- Y,Z,N )
                                    960 ;       Pop return stack to data stack.
                                    961 
      000383                        962         HEADFLG RFROM "R>" COMPO
                           000001     1         .ifeq   UNLINK_RFROM
      00840E 84 01                    2         .dw     LINK
                           000385     3         LINK    = .
      008410 42                       4         .db      ((102$ - 101$) + COMPO)
      008411                          5 101$:
      008411 52 3E                    6         .ascii  "R>"
      008413                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008413                        963 RFROM:
      008413 90 85            [ 2]  964         POPW    Y               ; save return addr
      008415 90 BF 7E         [ 2]  965         LDW     YTEMP,Y
      008418 90 85            [ 2]  966         POPW    Y
      00841A 5A               [ 2]  967         DECW    X
      00841B 5A               [ 2]  968         DECW    X
      00841C FF               [ 2]  969         LDW     (X),Y
      00841D 92 CC 7E         [ 5]  970         JP      [YTEMP]
                                    971 
                                    972 
                           000001   973         .ifne  HAS_CPNVM
                                    974 ;       doVARPTR ( - a )    ( TOS STM8: - Y,Z,N )
      008420                        975 DOVARPTR:
      008420 90 85            [ 2]  976         POPW    Y               ; get return addr (pfa)
      008422 90 FE            [ 2]  977         LDW     Y,(Y)
      008424 20 02            [ 2]  978         JRA     YSTOR
                                    979         .endif
                                    980 
                                    981 ;       doVAR   ( -- a )     ( TOS STM8: -- Y,Z,N )
                                    982 ;       Code for VARIABLE and CREATE.
                                    983 
                           000000   984         .ifne   WORDS_LINKRUNTI
                                    985         HEADFLG DOVAR "doVar" COMPO
                                    986         .endif
      008426                        987 DOVAR:
      008426 90 85            [ 2]  988         POPW    Y               ; get return addr (pfa)
                                    989         ; fall through
                                    990 
                                    991 ;       YSTOR core ( - n )     ( TOS STM8: - Y,Z,N )
                                    992 ;       push Y to stack
      008428                        993 YSTOR:
      008428 5A               [ 2]  994         DECW    X               ; SUBW  X,#2
      008429 5A               [ 2]  995         DECW    X
      00842A FF               [ 2]  996         LDW     (X),Y           ; push on stack
      00842B 81               [ 4]  997         RET                     ; go to RET of EXEC
                                    998 
                                    999 ;       R@      ( -- w )        ( TOS STM8: -- Y,Z,N )
                                   1000 ;       Copy top of return stack to stack (or the FOR - NEXT index value).
                                   1001 
      0003A1                       1002         HEADER  RAT "R@"
                           000001     1         .ifeq   UNLINK_RAT
      00842C 84 10                    2         .dw     LINK
                           0003A3     3         LINK    = .
      00842E 02                       4         .db      (102$ - 101$)
      00842F                          5 101$:
      00842F 52 40                    6         .ascii  "R@"
      008431                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008431                       1003 RAT:
      008431 16 03            [ 2] 1004         LDW     Y,(3,SP)
      008433 20 F3            [ 2] 1005         JRA     YSTOR
                                   1006 
                                   1007 ;       >R      ( w -- )      ( TOS STM8: -- Y,Z,N )
                                   1008 ;       Push data stack to return stack.
                                   1009 
      0003AA                       1010         HEADFLG TOR ">R" COMPO
                           000001     1         .ifeq   UNLINK_TOR
      008435 84 2E                    2         .dw     LINK
                           0003AC     3         LINK    = .
      008437 42                       4         .db      ((102$ - 101$) + COMPO)
      008438                          5 101$:
      008438 3E 52                    6         .ascii  ">R"
      00843A                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00843A                       1011 TOR:
      00843A 51               [ 1] 1012         EXGW    X,Y
      00843B 1E 01            [ 2] 1013         LDW     X,(1,SP)
      00843D 89               [ 2] 1014         PUSHW   X
      00843E 93               [ 1] 1015         LDW     X,Y
      00843F FE               [ 2] 1016         LDW     X,(X)
      008440 51               [ 1] 1017         EXGW    X,Y
      008441 17 03            [ 2] 1018         LDW     (3,SP),Y
      008443 20 11            [ 2] 1019         JRA     DROP
                                   1020 
                                   1021 
                                   1022 ;       NIP     ( n1 n2 -- n2 )
                                   1023 ;       Drop 2nd item on the stack
                                   1024 
      0003BA                       1025         HEADER  NIP "NIP"
                           000001     1         .ifeq   UNLINK_NIP
      008445 84 37                    2         .dw     LINK
                           0003BC     3         LINK    = .
      008447 03                       4         .db      (102$ - 101$)
      008448                          5 101$:
      008448 4E 49 50                 6         .ascii  "NIP"
      00844B                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00844B                       1026 NIP:
      00844B AD 2F            [ 4] 1027         CALLR   SWAPP
      00844D 20 07            [ 2] 1028         JRA     DROP
                                   1029 
                                   1030 ;       DROP    ( w -- )        ( TOS STM8: -- Y,Z,N )
                                   1031 ;       Discard top stack item.
                                   1032 
      0003C4                       1033         HEADER  DROP "DROP"
                           000001     1         .ifeq   UNLINK_DROP
      00844F 84 47                    2         .dw     LINK
                           0003C6     3         LINK    = .
      008451 04                       4         .db      (102$ - 101$)
      008452                          5 101$:
      008452 44 52 4F 50              6         .ascii  "DROP"
      008456                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008456                       1034 DROP:
      008456 5C               [ 1] 1035         INCW    X               ; ADDW   X,#2
      008457 5C               [ 1] 1036         INCW    X
      008458 90 93            [ 1] 1037         LDW     Y,X
      00845A 90 FE            [ 2] 1038         LDW     Y,(Y)
      00845C 81               [ 4] 1039         RET
                                   1040 
                                   1041 ;       2DROP   ( w w -- )       ( TOS STM8: -- Y,Z,N )
                                   1042 ;       Discard two items on stack.
                                   1043 
      0003D2                       1044         HEADER  DDROP "2DROP"
                           000001     1         .ifeq   UNLINK_DDROP
      00845D 84 51                    2         .dw     LINK
                           0003D4     3         LINK    = .
      00845F 05                       4         .db      (102$ - 101$)
      008460                          5 101$:
      008460 32 44 52 4F 50           6         .ascii  "2DROP"
      008465                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008465                       1045 DDROP:
      008465 5C               [ 1] 1046         INCW    X
      008466 5C               [ 1] 1047         INCW    X
      008467 20 ED            [ 2] 1048         JRA     DROP
                                   1049 
                                   1050 ;       DUP     ( w -- w w )    ( TOS STM8: -- Y,Z,N )
                                   1051 ;       Duplicate top stack item.
                                   1052 
      0003DE                       1053         HEADER  DUPP "DUP"
                           000001     1         .ifeq   UNLINK_DUPP
      008469 84 5F                    2         .dw     LINK
                           0003E0     3         LINK    = .
      00846B 03                       4         .db      (102$ - 101$)
      00846C                          5 101$:
      00846C 44 55 50                 6         .ascii  "DUP"
      00846F                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00846F                       1054 DUPP:
      00846F 90 93            [ 1] 1055         LDW     Y,X
      008471 90 FE            [ 2] 1056         LDW     Y,(Y)
      008473 20 B3            [ 2] 1057         JRA     YSTOR
                                   1058 
                                   1059 ;       SWAP ( w1 w2 -- w2 w1 ) ( TOS STM8: -- Y,Z,N )
                                   1060 ;       Exchange top two stack items.
                                   1061 
      0003EA                       1062         HEADER  SWAPP "SWAP"
                           000001     1         .ifeq   UNLINK_SWAPP
      008475 84 6B                    2         .dw     LINK
                           0003EC     3         LINK    = .
      008477 04                       4         .db      (102$ - 101$)
      008478                          5 101$:
      008478 53 57 41 50              6         .ascii  "SWAP"
      00847C                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00847C                       1063 SWAPP:
      00847C 90 93            [ 1] 1064         LDW     Y,X
      00847E EE 02            [ 2] 1065         LDW     X,(2,X)
      008480 89               [ 2] 1066         PUSHW   X
      008481 93               [ 1] 1067         LDW     X,Y
      008482 FE               [ 2] 1068         LDW     X,(X)
      008483 51               [ 1] 1069         EXGW    X,Y
      008484 EF 02            [ 2] 1070         LDW     (2,X),Y
      008486 90 85            [ 2] 1071         POPW    Y
      008488 FF               [ 2] 1072         LDW     (X),Y
      008489 81               [ 4] 1073         RET
                                   1074 
                                   1075 ;       OVER    ( w1 w2 -- w1 w2 w1 ) ( TOS STM8: -- Y,Z,N )
                                   1076 ;       Copy second stack item to top.
                                   1077 
      0003FF                       1078         HEADER  OVER "OVER"
                           000001     1         .ifeq   UNLINK_OVER
      00848A 84 77                    2         .dw     LINK
                           000401     3         LINK    = .
      00848C 04                       4         .db      (102$ - 101$)
      00848D                          5 101$:
      00848D 4F 56 45 52              6         .ascii  "OVER"
      008491                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008491                       1079 OVER:
      008491 90 93            [ 1] 1080         LDW     Y,X
      008493 90 EE 02         [ 2] 1081         LDW     Y,(2,Y)
      008496 20 90            [ 2] 1082         JRA     YSTOR
                                   1083 
                           000001  1084         .ifne   WORDS_EXTRACORE
                                   1085 ;       I       ( -- n )     ( TOS STM8: -- Y,Z,N )
                                   1086 ;       Get inner FOR-NEXT or DO-LOOP index value
      00040D                       1087         HEADER  IGET "I"
                           000001     1         .ifeq   UNLINK_IGET
      008498 84 8C                    2         .dw     LINK
                           00040F     3         LINK    = .
      00849A 01                       4         .db      (102$ - 101$)
      00849B                          5 101$:
      00849B 49                       6         .ascii  "I"
      00849C                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00849C                       1088 IGET:
                           000001  1089         .ifne   HAS_ALIAS
      00849C CC 84 31         [ 2] 1090         JP      RAT             ; CF JP: NAME> resolves I as ' R@"
                           000000  1091         .else
                                   1092         JRA     RAT
                                   1093         .endif
                                   1094         .endif
                                   1095 
                           000001  1096         .ifeq   BOOTSTRAP
                                   1097 ;       UM+     ( u u -- udsum )
                                   1098 ;       Add two unsigned single
                                   1099 ;       and return a double sum.
                                   1100 
      000414                       1101         HEADER  UPLUS "UM+"
                           000001     1         .ifeq   UNLINK_UPLUS
      00849F 84 9A                    2         .dw     LINK
                           000416     3         LINK    = .
      0084A1 03                       4         .db      (102$ - 101$)
      0084A2                          5 101$:
      0084A2 55 4D 2B                 6         .ascii  "UM+"
      0084A5                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0084A5                       1102 UPLUS:
      0084A5 AD 09            [ 4] 1103         CALLR   PLUS
      0084A7 4F               [ 1] 1104         CLR     A
      0084A8 49               [ 1] 1105         RLC     A
      0084A9 CC 85 43         [ 2] 1106         JP      ASTOR
                                   1107         .endif
                                   1108 
                                   1109 ;       +       ( w w -- sum ) ( TOS STM8: -- Y,Z,N )
                                   1110 ;       Add top two items.
                                   1111 
      000421                       1112         HEADER  PLUS "+"
                           000001     1         .ifeq   UNLINK_PLUS
      0084AC 84 A1                    2         .dw     LINK
                           000423     3         LINK    = .
      0084AE 01                       4         .db      (102$ - 101$)
      0084AF                          5 101$:
      0084AF 2B                       6         .ascii  "+"
      0084B0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   1113 
      0084B0                       1114 PLUS:
      0084B0 E6 01            [ 1] 1115         LD      A,(1,X) ;D=w
      0084B2 EB 03            [ 1] 1116         ADD     A,(3,X)
      0084B4 E7 03            [ 1] 1117         LD      (3,X),A
      0084B6 F6               [ 1] 1118         LD      A,(X)
      0084B7 E9 02            [ 1] 1119         ADC     A,(2,X)
      0084B9                       1120 LDADROP:
      0084B9 E7 02            [ 1] 1121         LD      (2,X),A
      0084BB 20 99            [ 2] 1122         JRA     DROP
                                   1123 
                                   1124 ;       XOR     ( w w -- w )    ( TOS STM8: -- Y,Z,N )
                                   1125 ;       Bitwise exclusive OR.
                                   1126 
      000432                       1127         HEADER  XORR "XOR"
                           000001     1         .ifeq   UNLINK_XORR
      0084BD 84 AE                    2         .dw     LINK
                           000434     3         LINK    = .
      0084BF 03                       4         .db      (102$ - 101$)
      0084C0                          5 101$:
      0084C0 58 4F 52                 6         .ascii  "XOR"
      0084C3                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0084C3                       1128 XORR:
      0084C3 E6 01            [ 1] 1129         LD      A,(1,X)         ; D=w
      0084C5 E8 03            [ 1] 1130         XOR     A,(3,X)
      0084C7 E7 03            [ 1] 1131         LD      (3,X),A
      0084C9 F6               [ 1] 1132         LD      A,(X)
      0084CA E8 02            [ 1] 1133         XOR     A,(2,X)
      0084CC 20 EB            [ 2] 1134         JRA     LDADROP
                                   1135 
                                   1136 ;       AND     ( w w -- w )    ( TOS STM8: -- Y,Z,N )
                                   1137 ;       Bitwise AND.
                                   1138 
      000443                       1139         HEADER  ANDD "AND"
                           000001     1         .ifeq   UNLINK_ANDD
      0084CE 84 BF                    2         .dw     LINK
                           000445     3         LINK    = .
      0084D0 03                       4         .db      (102$ - 101$)
      0084D1                          5 101$:
      0084D1 41 4E 44                 6         .ascii  "AND"
      0084D4                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0084D4                       1140 ANDD:
      0084D4 E6 01            [ 1] 1141         LD      A,(1,X)         ; D=w
      0084D6 E4 03            [ 1] 1142         AND     A,(3,X)
      0084D8 E7 03            [ 1] 1143         LD      (3,X),A
      0084DA F6               [ 1] 1144         LD      A,(X)
      0084DB E4 02            [ 1] 1145         AND     A,(2,X)
      0084DD 20 DA            [ 2] 1146         JRA     LDADROP
                                   1147 
                           000001  1148         .ifeq   BOOTSTRAP
                                   1149 ;       OR      ( w w -- w )    ( TOS STM8: -- immediate Y,Z,N )
                                   1150 ;       Bitwise inclusive OR.
                                   1151 
      000454                       1152         HEADER  ORR "OR"
                           000001     1         .ifeq   UNLINK_ORR
      0084DF 84 D0                    2         .dw     LINK
                           000456     3         LINK    = .
      0084E1 02                       4         .db      (102$ - 101$)
      0084E2                          5 101$:
      0084E2 4F 52                    6         .ascii  "OR"
      0084E4                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0084E4                       1153 ORR:
      0084E4 E6 01            [ 1] 1154         LD      A,(1,X)         ; D=w
      0084E6 EA 03            [ 1] 1155         OR      A,(3,X)
      0084E8 E7 03            [ 1] 1156         LD      (3,X),A
      0084EA F6               [ 1] 1157         LD      A,(X)
      0084EB EA 02            [ 1] 1158         OR      A,(2,X)
      0084ED 20 CA            [ 2] 1159         JRA     LDADROP
                                   1160         .endif
                                   1161 
                                   1162 ;       0<      ( n -- t ) ( TOS STM8: -- A,Z )
                                   1163 ;       Return true if n is negative.
                                   1164 
      000464                       1165         HEADER  ZLESS "0<"
                           000001     1         .ifeq   UNLINK_ZLESS
      0084EF 84 E1                    2         .dw     LINK
                           000466     3         LINK    = .
      0084F1 02                       4         .db      (102$ - 101$)
      0084F2                          5 101$:
      0084F2 30 3C                    6         .ascii  "0<"
      0084F4                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0084F4                       1166 ZLESS:
      0084F4 4F               [ 1] 1167         CLR     A
      0084F5 7D               [ 1] 1168         TNZ     (X)
      0084F6 2A 01            [ 1] 1169         JRPL    ZL1
      0084F8 43               [ 1] 1170         CPL     A               ; true
      0084F9 F7               [ 1] 1171 ZL1:    LD      (X),A
      0084FA E7 01            [ 1] 1172         LD      (1,X),A
      0084FC 81               [ 4] 1173         RET
                                   1174 
                                   1175 ;       -   ( n1 n2 -- n1-n2 )  ( TOS STM8: -- Y,Z,N )
                                   1176 ;       Subtraction.
                                   1177 
      000472                       1178         HEADER  SUBB "-"
                           000001     1         .ifeq   UNLINK_SUBB
      0084FD 84 F1                    2         .dw     LINK
                           000474     3         LINK    = .
      0084FF 01                       4         .db      (102$ - 101$)
      008500                          5 101$:
      008500 2D                       6         .ascii  "-"
      008501                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   1179 
      008501                       1180 SUBB:
                           000000  1181         .ifne   SPEEDOVERSIZE
                                   1182         LDW     Y,X
                                   1183         LDW     Y,(Y)
                                   1184         LDW     YTEMP,Y
                                   1185         INCW    X
                                   1186         INCW    X
                                   1187         LDW     Y,X
                                   1188         LDW     Y,(Y)
                                   1189         SUBW    Y,YTEMP
                                   1190         LDW     (X),Y
                                   1191         RET                     ; 18 cy
                           000001  1192         .else
      008501 CD 87 D6         [ 4] 1193         CALL    NEGAT           ; (15 cy)
      008504 20 AA            [ 2] 1194         JRA     PLUS            ; 25 cy (15+10)
                                   1195         .endif
                                   1196 
                                   1197 
                                   1198 ;       CONTEXT ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   1199 ;       Start vocabulary search.
                                   1200 
      008506                       1201         HEADER  CNTXT "CONTEXT"
                           000000     1         .ifeq   UNLINK_CNTXT
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "CONTEXT"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      00047B                       1202 CNTXT:
                           000001  1203         .ifne  HAS_CPNVM
      008506 CD 93 F8         [ 4] 1204         CALL    NVMQ
      008509 27 04            [ 1] 1205         JREQ    1$              ; link NVM to NVM
      00850B A6 74            [ 1] 1206         LD      A,#(NVMCONTEXT)
      00850D 20 34            [ 2] 1207         JRA     ASTOR
      00850F                       1208 1$:
                                   1209         .endif
      00850F                       1210 CNTXT_ALIAS:
      00850F A6 76            [ 1] 1211         LD      A,#(USRCONTEXT)
      008511 20 30            [ 2] 1212         JRA     ASTOR
                                   1213 
                                   1214 
                                   1215 ;       CP      ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   1216 ;       Point to top of dictionary.
                                   1217 
                           000000  1218         .ifne   WORDS_LINKCOMP
                                   1219         HEADER  CPP "cp"
                                   1220         .endif
      008513                       1221 CPP:
      008513 A6 6A            [ 1] 1222         LD      A,#(USRCP)
      008515 20 2C            [ 2] 1223         JRA     ASTOR
                                   1224 
                                   1225 ; System and user variables
                                   1226 
                                   1227 ;       BASE    ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   1228 ;       Radix base for numeric I/O.
                                   1229 
      00048C                       1230         HEADER  BASE "BASE"
                           000001     1         .ifeq   UNLINK_BASE
      008517 84 FF                    2         .dw     LINK
                           00048E     3         LINK    = .
      008519 04                       4         .db      (102$ - 101$)
      00851A                          5 101$:
      00851A 42 41 53 45              6         .ascii  "BASE"
      00851E                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00851E                       1231 BASE:
      00851E A6 64            [ 1] 1232         LD      A,#(USRBASE)
      008520 20 21            [ 2] 1233         JRA     ASTOR
                                   1234 
                           000000  1235         .ifeq    UNLINK_INN
                                   1236 ;       >IN     ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   1237 ;       Hold parsing pointer.
                                   1238 
                                   1239         HEADER  INN ">IN"
                                   1240 INN:
                                   1241         LD      A,#(USR_IN)
                                   1242         JRA     ASTOR
                                   1243         .endif
                                   1244 
                           000000  1245         .ifeq    UNLINK_NTIB
                                   1246 ;       #TIB    ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   1247 ;       Count in terminal input buffer.
                                   1248 
                                   1249         HEADER  NTIB "#TIB"
                                   1250 NTIB:
                                   1251         LD      A,#(USRNTIB)
                                   1252         JRA     ASTOR
                                   1253         .endif
                                   1254 
                           000000  1255         .ifeq    UNLINK_TEVAL
                                   1256 ;       'eval   ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   1257 ;       Execution vector of EVAL.
                                   1258 
                                   1259         HEADER  TEVAL "'eval"
                                   1260 TEVAL:
                                   1261         LD      A,#(USREVAL)
                                   1262         JRA     ASTOR
                                   1263         .endif
                                   1264 
                           000000  1265         .ifeq    UNLINK_HLD
                                   1266 ;       HLD     ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   1267 ;       Hold a pointer of output string.
                                   1268 
                                   1269         HEADER  HLD "hld"
                                   1270 HLD:
                                   1271         LD      A,#(USRHLD)
                                   1272         JRA     ASTOR
                                   1273         .endif
                                   1274 
                                   1275 ;       'EMIT   ( -- a )     ( TOS STM8: -- A,Z,N )
                                   1276 ;
                           000001  1277         .ifeq   BAREBONES
      000497                       1278         HEADER  TEMIT "'EMIT"
                           000001     1         .ifeq   UNLINK_TEMIT
      008522 85 19                    2         .dw     LINK
                           000499     3         LINK    = .
      008524 05                       4         .db      (102$ - 101$)
      008525                          5 101$:
      008525 27 45 4D 49 54           6         .ascii  "'EMIT"
      00852A                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00852A                       1279 TEMIT:
      00852A A6 60            [ 1] 1280         LD      A,#(USREMIT)
      00852C 20 15            [ 2] 1281         JRA     ASTOR
                                   1282         .endif
                                   1283 
                                   1284 ;       '?KEY   ( -- a )     ( TOS STM8: -- A,Z,N )
                                   1285 ;
                           000001  1286         .ifeq   BAREBONES
      0004A3                       1287         HEADER  TQKEY "'?KEY"
                           000001     1         .ifeq   UNLINK_TQKEY
      00852E 85 24                    2         .dw     LINK
                           0004A5     3         LINK    = .
      008530 05                       4         .db      (102$ - 101$)
      008531                          5 101$:
      008531 27 3F 4B 45 59           6         .ascii  "'?KEY"
      008536                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008536                       1288 TQKEY:
      008536 A6 62            [ 1] 1289         LD      A,#(USRQKEY)
      008538 20 09            [ 2] 1290         JRA     ASTOR
                                   1291         .endif
                                   1292 
                                   1293 ;       LAST    ( -- a )        ( TOS STM8: -- Y,Z,N )
                                   1294 ;       Point to last name in dictionary
                                   1295 
                           000001  1296         .ifeq   BAREBONES
      0004AF                       1297         HEADER  LAST "last"
                           000001     1         .ifeq   UNLINK_LAST
      00853A 85 30                    2         .dw     LINK
                           0004B1     3         LINK    = .
      00853C 04                       4         .db      (102$ - 101$)
      00853D                          5 101$:
      00853D 6C 61 73 74              6         .ascii  "last"
      008541                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   1298         .endif
      008541                       1299 LAST:
      008541 A6 6C            [ 1] 1300         LD      A,#(USRLAST)
                                   1301 
                                   1302 ;       ASTOR core ( - n )     ( TOS STM8: - Y,Z,N )
                                   1303 ;       push A to stack
      008543                       1304 ASTOR:
      008543 90 5F            [ 1] 1305         CLRW    Y
      008545 90 97            [ 1] 1306         LD      YL,A
      008547 CC 84 28         [ 2] 1307         JP      YSTOR
                                   1308 
                                   1309 
                                   1310 ;       ATOKEY core ( - c T | f )    ( TOS STM8: - Y,Z,N )
                                   1311 ;       Return input char and true, or false.
      00854A                       1312 ATOKEY:
      00854A 4D               [ 1] 1313         TNZ     A
      00854B 27 04            [ 1] 1314         JREQ    1$
      00854D AD 02            [ 4] 1315         CALLR   1$              ; push char
      00854F 20 1C            [ 2] 1316         JRA     MONE            ; flag true
      008551 20 F0            [ 2] 1317 1$:     JRA     ASTOR           ; push char or flag false
                                   1318 
                                   1319 
                                   1320 ;       TIB     ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   1321 ;       Return address of terminal input buffer.
                                   1322 
                           000000  1323         .ifeq   UNLINK_TIB
                                   1324         HEADER  TIB "TIB"
                                   1325 TIB:
                                   1326         DoLitW  TIBB
                                   1327         RET
                                   1328         .endif
                                   1329 
                           000001  1330         .ifne   HAS_OUTPUTS
                                   1331 ;       OUT     ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   1332 ;       Return address of OUTPUTS register
                                   1333 
      0004C8                       1334         HEADER  OUTA "OUT"
                           000001     1         .ifeq   UNLINK_OUTA
      008553 85 3C                    2         .dw     LINK
                           0004CA     3         LINK    = .
      008555 03                       4         .db      (102$ - 101$)
      008556                          5 101$:
      008556 4F 55 54                 6         .ascii  "OUT"
      008559                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008559                       1335 OUTA:
      008559 A6 40            [ 1] 1336         LD      A,#(OUTPUTS)
      00855B 20 E6            [ 2] 1337         JRA     ASTOR
                                   1338         .endif
                                   1339 
                                   1340 ; Constants
                                   1341 
                                   1342 ;       BL      ( -- 32 )     ( TOS STM8: -- Y,Z,N )
                                   1343 ;       Return 32, blank character.
                                   1344 
      0004D2                       1345         HEADER  BLANK "BL"
                           000001     1         .ifeq   UNLINK_BLANK
      00855D 85 55                    2         .dw     LINK
                           0004D4     3         LINK    = .
      00855F 02                       4         .db      (102$ - 101$)
      008560                          5 101$:
      008560 42 4C                    6         .ascii  "BL"
      008562                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008562                       1346 BLANK:
      008562 A6 20            [ 1] 1347         LD      A,#32
      008564 20 DD            [ 2] 1348         JRA     ASTOR
                                   1349 
                                   1350 ;       0       ( -- 0)     ( TOS STM8: -- Y,Z,N )
                                   1351 ;       Return 0.
                                   1352 
                           000000  1353         .ifne   SPEEDOVERSIZE
                                   1354         HEADER  ZERO "0"
                                   1355         .endif
      008566                       1356 ZERO:
      008566 4F               [ 1] 1357         CLR     A
      008567 20 DA            [ 2] 1358         JRA     ASTOR
                                   1359 
                                   1360 ;       1       ( -- 1)     ( TOS STM8: -- Y,Z,N )
                                   1361 ;       Return 1.
                                   1362 
                           000000  1363         .ifne   SPEEDOVERSIZE
                                   1364         HEADER  ONE "1"
                                   1365         .endif
      008569                       1366 ONE:
      008569 A6 01            [ 1] 1367         LD      A,#1
      00856B 20 D6            [ 2] 1368         JRA     ASTOR
                                   1369 
                                   1370 ;       -1      ( -- -1)     ( TOS STM8: -- Y,Z,N )
                                   1371 ;       Return -1
                                   1372 
                           000000  1373         .ifne   SPEEDOVERSIZE
                                   1374         HEADER  MONE "-1"
                                   1375         .endif
      00856D                       1376 MONE:
      00856D 90 AE FF FF      [ 2] 1377         LDW     Y,#0xFFFF
      008571                       1378 AYSTOR:
      008571 CC 84 28         [ 2] 1379         JP      YSTOR
                                   1380 
                           000001  1381         .ifne   HAS_BACKGROUND
                                   1382 ;       TIM     ( -- T)     ( TOS STM8: -- Y,Z,N )
                                   1383 ;       Return TICKCNT as timer
                                   1384 
      0004E9                       1385         HEADER  TIMM "TIM"
                           000001     1         .ifeq   UNLINK_TIMM
      008574 85 5F                    2         .dw     LINK
                           0004EB     3         LINK    = .
      008576 03                       4         .db      (102$ - 101$)
      008577                          5 101$:
      008577 54 49 4D                 6         .ascii  "TIM"
      00857A                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00857A                       1386 TIMM:
      00857A 90 BE 47         [ 2] 1387         LDW     Y,TICKCNT
      00857D 20 F2            [ 2] 1388         JRA     AYSTOR
                                   1389 
                                   1390 
                                   1391 ;       BG      ( -- a)     ( TOS STM8: -- Y,Z,N )
                                   1392 ;       Return address of BGADDR vector
                                   1393 
      0004F4                       1394         HEADER  BGG "BG"
                           000001     1         .ifeq   UNLINK_BGG
      00857F 85 76                    2         .dw     LINK
                           0004F6     3         LINK    = .
      008581 02                       4         .db      (102$ - 101$)
      008582                          5 101$:
      008582 42 47                    6         .ascii  "BG"
      008584                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008584                       1395 BGG:
      008584 A6 45            [ 1] 1396         LD      A,#(BGADDR)
      008586 20 BB            [ 2] 1397         JRA     ASTOR
                                   1398         .endif
                                   1399 
                                   1400 
                                   1401 ;       'PROMPT ( -- a)     ( TOS STM8: -- Y,Z,N )
                                   1402 ;       Return address of PROMPT vector
                                   1403 
                           000000  1404         .ifeq   UNLINK_TPROMPT
                                   1405         HEADER  TPROMPT "'PROMPT"
                                   1406 TPROMPT:
                                   1407         LD      A,#(USRPROMPT)
                                   1408         JRA     ASTOR
                                   1409         .endif
                                   1410 
                                   1411 
                           000000  1412         .ifne   HAS_FILEHAND
                                   1413 ;       ( -- ) EMIT pace character for handshake in FILE mode
                                   1414 PACEE:
                                   1415         DoLitC  PACE      ; pace character for host handshake
                                   1416         JP      [USREMIT]
                                   1417 
                                   1418 ;       HAND    ( -- )
                                   1419 ;       set PROMPT vector to interactive mode
                                   1420         HEADER  HANDD "HAND"
                                   1421 HANDD:
                                   1422         LDW     Y,#(DOTOK)
                                   1423 YPROMPT:
                                   1424         LDW     USRPROMPT,Y
                                   1425         RET
                                   1426 
                                   1427 ;       FILE    ( -- )
                                   1428 ;       set PROMPT vector to file transfer mode
                                   1429         HEADER  FILEE "FILE"
                                   1430 FILEE:
                                   1431         LDW     Y,#(PACEE)
                                   1432         JRA     YPROMPT
                                   1433         .endif
                                   1434 
                                   1435 
                                   1436 ; Common functions
                                   1437 
                                   1438 
                                   1439 ;       ?DUP    ( w -- w w | 0 )   ( TOS STM8: -- Y,Z,N )
                                   1440 ;       Dup tos if its not zero.
      0004FD                       1441         HEADER  QDUP "?DUP"
                           000001     1         .ifeq   UNLINK_QDUP
      008588 85 81                    2         .dw     LINK
                           0004FF     3         LINK    = .
      00858A 04                       4         .db      (102$ - 101$)
      00858B                          5 101$:
      00858B 3F 44 55 50              6         .ascii  "?DUP"
      00858F                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00858F                       1442 QDUP:
      00858F 90 93            [ 1] 1443         LDW     Y,X
      008591 90 FE            [ 2] 1444         LDW     Y,(Y)
      008593 27 03            [ 1] 1445         JREQ    QDUP1
      008595 5A               [ 2] 1446         DECW    X
      008596 5A               [ 2] 1447         DECW    X
      008597 FF               [ 2] 1448         LDW     (X),Y
      008598 81               [ 4] 1449 QDUP1:  RET
                                   1450 
                                   1451 ;       ROT     ( w1 w2 w3 -- w2 w3 w1 ) ( TOS STM8: -- Y,Z,N )
                                   1452 ;       Rot 3rd item to top.
                                   1453 
      00050E                       1454         HEADER  ROT "ROT"
                           000001     1         .ifeq   UNLINK_ROT
      008599 85 8A                    2         .dw     LINK
                           000510     3         LINK    = .
      00859B 03                       4         .db      (102$ - 101$)
      00859C                          5 101$:
      00859C 52 4F 54                 6         .ascii  "ROT"
      00859F                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00859F                       1455 ROT:
                           000000  1456         .ifne   SPEEDOVERSIZE
                                   1457         LDW     Y,X
                                   1458         LDW     X,(4,X)
                                   1459         PUSHW   X
                                   1460         LDW     X,Y
                                   1461         LDW     X,(2,X)
                                   1462         PUSHW   X
                                   1463         LDW     X,Y
                                   1464         LDW     X,(X)
                                   1465         EXGW    X,Y
                                   1466         LDW     (2,X),Y
                                   1467         POPW    Y
                                   1468         LDW     (4,X),Y
                                   1469         POPW    Y
                                   1470         LDW     (X),Y
                                   1471         RET
                           000001  1472         .else
      00859F CD 84 3A         [ 4] 1473         CALL    TOR
      0085A2 AD 03            [ 4] 1474         CALLR   1$
      0085A4 CD 84 13         [ 4] 1475         CALL    RFROM
      0085A7 CC 84 7C         [ 2] 1476 1$:     JP      SWAPP
                                   1477         .endif
                                   1478 
                                   1479 ;       2DUP    ( w1 w2 -- w1 w2 w1 w2 )
                                   1480 ;       Duplicate top two items.
                                   1481 
      00051F                       1482         HEADER  DDUP "2DUP"
                           000001     1         .ifeq   UNLINK_DDUP
      0085AA 85 9B                    2         .dw     LINK
                           000521     3         LINK    = .
      0085AC 04                       4         .db      (102$ - 101$)
      0085AD                          5 101$:
      0085AD 32 44 55 50              6         .ascii  "2DUP"
      0085B1                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0085B1                       1483 DDUP:
      0085B1 AD 00            [ 4] 1484         CALLR    1$
      0085B3                       1485 1$:
      0085B3 CC 84 91         [ 2] 1486         JP      OVER
                                   1487 
                           000001  1488         .ifeq   UNLINKCORE
                                   1489 ;       DNEGATE ( d -- -d )     ( TOS STM8: -- Y,Z,N )
                                   1490 ;       Two's complement of top double.
                                   1491 
      00052B                       1492         HEADER  DNEGA "DNEGATE"
                           000001     1         .ifeq   UNLINK_DNEGA
      0085B6 85 AC                    2         .dw     LINK
                           00052D     3         LINK    = .
      0085B8 07                       4         .db      (102$ - 101$)
      0085B9                          5 101$:
      0085B9 44 4E 45 47 41 54 45     6         .ascii  "DNEGATE"
      0085C0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0085C0                       1493 DNEGA:
      0085C0 90 93            [ 1] 1494         LDW     Y,X
      0085C2 90 EE 02         [ 2] 1495         LDW     Y,(2,Y)
      0085C5 90 50            [ 2] 1496         NEGW    Y
      0085C7 8A               [ 1] 1497         PUSH    CC
      0085C8 EF 02            [ 2] 1498         LDW     (2,X),Y
      0085CA 90 93            [ 1] 1499         LDW     Y,X
      0085CC 90 FE            [ 2] 1500         LDW     Y,(Y)
      0085CE 90 53            [ 2] 1501         CPLW    Y
      0085D0 86               [ 1] 1502         POP     CC
      0085D1 25 02            [ 1] 1503         JRC     DN1
      0085D3 90 5C            [ 1] 1504         INCW    Y
      0085D5 FF               [ 2] 1505 DN1:    LDW     (X),Y
      0085D6 81               [ 4] 1506         RET
                                   1507         .endif
                                   1508 
                           000001  1509         .ifeq   BOOTSTRAP
                                   1510 ;       =       ( w w -- t )    ( TOS STM8: -- Y,Z,N )
                                   1511 ;       Return true if top two are equal.
                                   1512 
      00054C                       1513         HEADER  EQUAL "="
                           000001     1         .ifeq   UNLINK_EQUAL
      0085D7 85 B8                    2         .dw     LINK
                           00054E     3         LINK    = .
      0085D9 01                       4         .db      (102$ - 101$)
      0085DA                          5 101$:
      0085DA 3D                       6         .ascii  "="
      0085DB                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0085DB                       1514 EQUAL:
                           000000  1515         .ifne   SPEEDOVERSIZE
                                   1516         LD      A,#0x0FF        ;true
                                   1517         LDW     Y,X     ;D = n2
                                   1518         LDW     Y,(Y)
                                   1519         LDW     YTEMP,Y
                                   1520         INCW    X
                                   1521         INCW    X
                                   1522         LDW     Y,X
                                   1523         LDW     Y,(Y)
                                   1524         CPW     Y,YTEMP ;if n2 <> n1
                                   1525         JREQ    EQ1
                                   1526         CLR     A
                                   1527 EQ1:    LD      (X),A
                                   1528         LD      (1,X),A
                                   1529         LDW     Y,X
                                   1530         LDW     Y,(Y)
                                   1531         RET                            ; 24 cy
                           000001  1532         .else
      0085DB CD 84 C3         [ 4] 1533         CALL    XORR
      0085DE CC 87 EB         [ 2] 1534         JP      ZEQUAL                 ; 31 cy= (18+13)
                                   1535         .endif
                                   1536         .endif
                                   1537 
                                   1538 
                                   1539 ;       U<      ( u u -- t )    ( TOS STM8: -- Y,Z,N )
                                   1540 ;       Unsigned compare of top two items.
                                   1541 
      000556                       1542         HEADER  ULESS "U<"
                           000001     1         .ifeq   UNLINK_ULESS
      0085E1 85 D9                    2         .dw     LINK
                           000558     3         LINK    = .
      0085E3 02                       4         .db      (102$ - 101$)
      0085E4                          5 101$:
      0085E4 55 3C                    6         .ascii  "U<"
      0085E6                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0085E6                       1543 ULESS:
      0085E6 4F               [ 1] 1544         CLR     A
      0085E7 AD 13            [ 4] 1545         CALLR   YTEMPCMP
      0085E9 24 01            [ 1] 1546         JRUGE   1$
      0085EB 43               [ 1] 1547         CPL     A
      0085EC 90 97            [ 1] 1548 1$:     LD      YL,A
      0085EE 90 95            [ 1] 1549         LD      YH,A
      0085F0 FF               [ 2] 1550         LDW     (X),Y
      0085F1 81               [ 4] 1551         RET
                                   1552 
                           000001  1553         .ifeq   BOOTSTRAP
                                   1554 ;       <       ( n1 n2 -- t )
                                   1555 ;       Signed compare of top two items.
                                   1556 
      000567                       1557         HEADER  LESS "<"
                           000001     1         .ifeq   UNLINK_LESS
      0085F2 85 E3                    2         .dw     LINK
                           000569     3         LINK    = .
      0085F4 01                       4         .db      (102$ - 101$)
      0085F5                          5 101$:
      0085F5 3C                       6         .ascii  "<"
      0085F6                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0085F6                       1558 LESS:
                           000000  1559         .ifne   SPEEDOVERSIZE
                                   1560         CLR     A
                                   1561         LDW     Y,X
                                   1562         LDW     Y,(Y)
                                   1563         LDW     YTEMP,Y
                                   1564         INCW    X
                                   1565         INCW    X
                                   1566         LDW     Y,X
                                   1567         LDW     Y,(Y)
                                   1568         CPW     Y,YTEMP
                                   1569         JRSGE   1$
                                   1570         CPL     A
                                   1571 1$:     LD      (X),A
                                   1572         LD      (1,X),A
                                   1573         LDW     Y,X
                                   1574         LDW     Y,(Y)
                                   1575         RET                      ; 26 cy
                           000001  1576         .else
      0085F6 CD 85 01         [ 4] 1577         CALL    SUBB             ; (29cy)
      0085F9 CC 84 F4         [ 2] 1578         JP      ZLESS            ; 41 cy (12+29)
                                   1579         .endif
                                   1580         .endif
                                   1581 
                                   1582 ;       YTEMPCMP       ( n n - n )      ( TOS STM8: - Y,Z,N )
                                   1583 ;       Load (TOS) to YTEMP and (TOS-1) to Y, DROP, CMP to STM8 flags
      0085FC                       1584 YTEMPCMP:
      0085FC 90 93            [ 1] 1585         LDW     Y,X
      0085FE 5C               [ 1] 1586         INCW    X
      0085FF 5C               [ 1] 1587         INCW    X
      008600 51               [ 1] 1588         EXGW    X,Y
      008601 FE               [ 2] 1589         LDW     X,(X)
      008602 BF 7E            [ 2] 1590         LDW     YTEMP,X
      008604 93               [ 1] 1591         LDW     X,Y
      008605 FE               [ 2] 1592         LDW     X,(X)
      008606 B3 7E            [ 2] 1593         CPW     X,YTEMP
      008608 51               [ 1] 1594         EXGW    X,Y
      008609 81               [ 4] 1595         RET
                                   1596 
                           000001  1597         .ifeq   BOOTSTRAP
                                   1598 ;       MAX     ( n n -- n )    ( TOS STM8: -- Y,Z,N )
                                   1599 ;       Return greater of two top items.
                                   1600 
      00057F                       1601         HEADER  MAX "MAX"
                           000001     1         .ifeq   UNLINK_MAX
      00860A 85 F4                    2         .dw     LINK
                           000581     3         LINK    = .
      00860C 03                       4         .db      (102$ - 101$)
      00860D                          5 101$:
      00860D 4D 41 58                 6         .ascii  "MAX"
      008610                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008610                       1602 MAX:
      008610 AD EA            [ 4] 1603         CALLR   YTEMPCMP
      008612 2C 04            [ 1] 1604         JRSGT   MMEXIT
      008614                       1605 YTEMPTOS:
      008614 90 BE 7E         [ 2] 1606         LDW     Y,YTEMP
      008617 FF               [ 2] 1607         LDW     (X),Y
      008618                       1608 MMEXIT:
      008618 81               [ 4] 1609         RET
                                   1610         .endif
                                   1611 
                           000001  1612         .ifeq   BOOTSTRAP
                                   1613 ;       MIN     ( n n -- n )    ( TOS STM8: -- Y,Z,N )
                                   1614 ;       Return smaller of top two items.
                                   1615 
      00058E                       1616         HEADER  MIN "MIN"
                           000001     1         .ifeq   UNLINK_MIN
      008619 86 0C                    2         .dw     LINK
                           000590     3         LINK    = .
      00861B 03                       4         .db      (102$ - 101$)
      00861C                          5 101$:
      00861C 4D 49 4E                 6         .ascii  "MIN"
      00861F                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00861F                       1617 MIN:
      00861F AD DB            [ 4] 1618         CALLR   YTEMPCMP
      008621 2F F5            [ 1] 1619         JRSLT   MMEXIT
      008623 20 EF            [ 2] 1620         JRA     YTEMPTOS
                                   1621         .endif
                                   1622 
                           000001  1623         .ifeq   UNLINK_WITHI
                                   1624 ;       WITHIN ( u ul uh -- t ) ( TOS STM8: -- Y,Z,N )
                                   1625 ;       Return true if u is within
                                   1626 ;       range of ul and uh. ( ul <= u < uh )
                                   1627 
      00059A                       1628         HEADER  WITHI "WITHIN"
                           000001     1         .ifeq   UNLINK_WITHI
      008625 86 1B                    2         .dw     LINK
                           00059C     3         LINK    = .
      008627 06                       4         .db      (102$ - 101$)
      008628                          5 101$:
      008628 57 49 54 48 49 4E        6         .ascii  "WITHIN"
      00862E                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00862E                       1629 WITHI:
      00862E CD 84 91         [ 4] 1630         CALL    OVER
      008631 CD 85 01         [ 4] 1631         CALL    SUBB
      008634 CD 84 3A         [ 4] 1632         CALL    TOR
      008637 CD 85 01         [ 4] 1633         CALL    SUBB
      00863A CD 84 13         [ 4] 1634         CALL    RFROM
      00863D 20 A7            [ 2] 1635         JRA     ULESS
                                   1636         .endif
                                   1637 
                                   1638 ; Divide
                                   1639 
                                   1640 ;       UM/MOD  ( udl udh un -- ur uq )
                                   1641 ;       Unsigned divide of a double by a
                                   1642 ;       single. Return mod and quotient.
                                   1643 
      0005B4                       1644         HEADER  UMMOD "UM/MOD"
                           000001     1         .ifeq   UNLINK_UMMOD
      00863F 86 27                    2         .dw     LINK
                           0005B6     3         LINK    = .
      008641 06                       4         .db      (102$ - 101$)
      008642                          5 101$:
      008642 55 4D 2F 4D 4F 44        6         .ascii  "UM/MOD"
      008648                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008648                       1645 UMMOD:
      008648 89               [ 2] 1646         PUSHW   X               ; save stack pointer
      008649 FE               [ 2] 1647         LDW     X,(X)           ; un
      00864A BF 7E            [ 2] 1648         LDW     YTEMP,X         ; save un
      00864C 16 01            [ 2] 1649         LDW     Y,(1,SP)        ; X stack pointer
      00864E 90 EE 04         [ 2] 1650         LDW     Y,(4,Y)         ; Y=udl
      008651 1E 01            [ 2] 1651         LDW     X,(1,SP)        ; X
      008653 EE 02            [ 2] 1652         LDW     X,(2,X)         ; X=udh
      008655 B3 7E            [ 2] 1653         CPW     X,YTEMP
      008657 23 0D            [ 2] 1654         JRULE   MMSM1           ; X is still on the R-stack
      008659 85               [ 2] 1655         POPW    X
      00865A 5C               [ 1] 1656         INCW    X               ; pop off 1 level
      00865B 5C               [ 1] 1657         INCW    X               ; ADDW   X,#2
      00865C 90 AE FF FF      [ 2] 1658         LDW     Y,#0xFFFF
      008660 FF               [ 2] 1659         LDW     (X),Y
      008661 90 5F            [ 1] 1660         CLRW    Y
      008663 EF 02            [ 2] 1661         LDW     (2,X),Y
      008665 81               [ 4] 1662         RET
      008666                       1663 MMSM1:
      008666 A6 11            [ 1] 1664         LD      A,#17           ; loop count
      008668                       1665 MMSM3:
      008668 B3 7E            [ 2] 1666         CPW     X,YTEMP         ; compare udh to un
      00866A 25 04            [ 1] 1667         JRULT   MMSM4           ; can't subtract
      00866C 72 B0 00 7E      [ 2] 1668         SUBW    X,YTEMP         ; can subtract
      008670                       1669 MMSM4:
      008670 8C               [ 1] 1670         CCF                     ; quotient bit
      008671 90 59            [ 2] 1671         RLCW    Y               ; rotate into quotient
      008673 59               [ 2] 1672         RLCW    X               ; rotate into remainder
      008674 4A               [ 1] 1673         DEC     A               ; repeat
      008675 22 F1            [ 1] 1674         JRUGT   MMSM3
      008677 57               [ 2] 1675         SRAW    X
      008678 BF 7E            [ 2] 1676         LDW     YTEMP,X         ; done, save remainder
      00867A 85               [ 2] 1677         POPW    X
      00867B 5C               [ 1] 1678         INCW    X               ; drop
      00867C 5C               [ 1] 1679         INCW    X               ; ADDW   X,#2
      00867D FF               [ 2] 1680         LDW     (X),Y
      00867E 90 BE 7E         [ 2] 1681         LDW     Y,YTEMP         ; save quotient
      008681 EF 02            [ 2] 1682         LDW     (2,X),Y
      008683 81               [ 4] 1683         RET
                                   1684 
                           000001  1685         .ifeq   UNLINKCORE
                                   1686 ;       M/MOD   ( d n -- r q )
                                   1687 ;       Signed floored divide of double by
                                   1688 ;       single. Return mod and quotient.
                                   1689 
      0005F9                       1690         HEADER  MSMOD "M/MOD"
                           000001     1         .ifeq   UNLINK_MSMOD
      008684 86 41                    2         .dw     LINK
                           0005FB     3         LINK    = .
      008686 05                       4         .db      (102$ - 101$)
      008687                          5 101$:
      008687 4D 2F 4D 4F 44           6         .ascii  "M/MOD"
      00868C                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00868C                       1691 MSMOD:
      00868C F6               [ 1] 1692         LD      A,(X)           ; DUPP ZLESS
      00868D 88               [ 1] 1693         PUSH    A               ; DUPP TOR
      00868E 2A 0C            [ 1] 1694         JRPL    MMOD1           ; QBRAN
      008690 CD 87 D6         [ 4] 1695         CALL    NEGAT
      008693 CD 84 3A         [ 4] 1696         CALL    TOR
      008696 CD 85 C0         [ 4] 1697         CALL    DNEGA
      008699 CD 84 13         [ 4] 1698         CALL    RFROM
      00869C                       1699 MMOD1:
      00869C CD 84 3A         [ 4] 1700         CALL    TOR
      00869F 2A 06            [ 1] 1701         JRPL    MMOD2           ; DUPP ZLESS QBRAN
      0086A1 CD 84 31         [ 4] 1702         CALL    RAT
      0086A4 CD 84 B0         [ 4] 1703         CALL    PLUS
      0086A7 CD 84 13         [ 4] 1704 MMOD2:  CALL    RFROM
      0086AA AD 9C            [ 4] 1705         CALLR   UMMOD
      0086AC 84               [ 1] 1706         POP     A               ; RFROM
      0086AD 4D               [ 1] 1707         TNZ     A
      0086AE 2A 09            [ 1] 1708         JRPL    MMOD3           ; QBRAN
      0086B0 CD 84 7C         [ 4] 1709         CALL    SWAPP
      0086B3 CD 87 D6         [ 4] 1710         CALL    NEGAT
      0086B6 CD 84 7C         [ 4] 1711         CALL    SWAPP
      0086B9 81               [ 4] 1712 MMOD3:  RET
                                   1713 
                                   1714 ;       /MOD    ( n n -- r q )
                                   1715 ;       Signed divide. Return mod and quotient.
                                   1716 
      00062F                       1717         HEADER  SLMOD "/MOD"
                           000001     1         .ifeq   UNLINK_SLMOD
      0086BA 86 86                    2         .dw     LINK
                           000631     3         LINK    = .
      0086BC 04                       4         .db      (102$ - 101$)
      0086BD                          5 101$:
      0086BD 2F 4D 4F 44              6         .ascii  "/MOD"
      0086C1                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0086C1                       1718 SLMOD:
      0086C1 CD 84 91         [ 4] 1719         CALL    OVER
      0086C4 CD 84 F4         [ 4] 1720         CALL    ZLESS
      0086C7 CD 84 7C         [ 4] 1721         CALL    SWAPP
      0086CA 20 C0            [ 2] 1722         JRA     MSMOD
                                   1723 
                                   1724 ;       MOD     ( n n -- r )    ( TOS STM8: -- Y,Z,N )
                                   1725 ;       Signed divide. Return mod only.
                                   1726 
      000641                       1727         HEADER  MMOD "MOD"
                           000001     1         .ifeq   UNLINK_MMOD
      0086CC 86 BC                    2         .dw     LINK
                           000643     3         LINK    = .
      0086CE 03                       4         .db      (102$ - 101$)
      0086CF                          5 101$:
      0086CF 4D 4F 44                 6         .ascii  "MOD"
      0086D2                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0086D2                       1728 MODD:
      0086D2 AD ED            [ 4] 1729         CALLR   SLMOD
      0086D4 CC 84 56         [ 2] 1730         JP      DROP
                                   1731 
                                   1732 ;       /       ( n n -- q )    ( TOS STM8: -- Y,Z,N )
                                   1733 ;       Signed divide. Return quotient only.
                                   1734 
      00064C                       1735         HEADER  SLASH "/"
                           000001     1         .ifeq   UNLINK_SLASH
      0086D7 86 CE                    2         .dw     LINK
                           00064E     3         LINK    = .
      0086D9 01                       4         .db      (102$ - 101$)
      0086DA                          5 101$:
      0086DA 2F                       6         .ascii  "/"
      0086DB                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0086DB                       1736 SLASH:
      0086DB AD E4            [ 4] 1737         CALLR   SLMOD
      0086DD CC 84 4B         [ 2] 1738         JP      NIP
                                   1739         .endif
                                   1740 
                                   1741 ; Multiply
                                   1742 
                                   1743 ;       UM*     ( u u -- ud )
                                   1744 ;       Unsigned multiply. Return double product.
                                   1745 
      000655                       1746         HEADER  UMSTA "UM*"
                           000001     1         .ifeq   UNLINK_UMSTA
      0086E0 86 D9                    2         .dw     LINK
                           000657     3         LINK    = .
      0086E2 03                       4         .db      (102$ - 101$)
      0086E3                          5 101$:
      0086E3 55 4D 2A                 6         .ascii  "UM*"
      0086E6                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0086E6                       1747 UMSTA:                          ; stack have 4 bytes u1=a,b u2=c,d
      0086E6 E6 02            [ 1] 1748         LD      A,(2,X)         ; b
      0086E8 90 97            [ 1] 1749         LD      YL,A
      0086EA F6               [ 1] 1750         LD      A,(X)           ; d
      0086EB 90 42            [ 4] 1751         MUL     Y,A
      0086ED 90 89            [ 2] 1752         PUSHW   Y               ; PROD1 temp storage
      0086EF E6 03            [ 1] 1753         LD      A,(3,X)         ; a
      0086F1 90 97            [ 1] 1754         LD      YL,A
      0086F3 F6               [ 1] 1755         LD      A,(X)           ; d
      0086F4 90 42            [ 4] 1756         MUL     Y,A
      0086F6 90 89            [ 2] 1757         PUSHW   Y               ; PROD2 temp storage
      0086F8 E6 02            [ 1] 1758         LD      A,(2,X)         ; b
      0086FA 90 97            [ 1] 1759         LD      YL,A
      0086FC E6 01            [ 1] 1760         LD      A,(1,X)         ; c
      0086FE 90 42            [ 4] 1761         MUL     Y,A
      008700 90 89            [ 2] 1762         PUSHW   Y               ; PROD3,CARRY temp storage
      008702 E6 03            [ 1] 1763         LD      A,(3,X)         ; a
      008704 90 97            [ 1] 1764         LD      YL,A
      008706 E6 01            [ 1] 1765         LD      A,(1,X)         ; c
      008708 90 42            [ 4] 1766         MUL     Y,A             ; least signifiant product
      00870A 4F               [ 1] 1767         CLR     A
      00870B 90 01            [ 1] 1768         RRWA    Y
      00870D E7 03            [ 1] 1769         LD      (3,X),A         ; store least significant byte
      00870F 72 F9 01         [ 2] 1770         ADDW    Y,(1,SP)        ; PROD3
      008712 4F               [ 1] 1771         CLR     A
      008713 49               [ 1] 1772         RLC     A               ; save carry
      008714 6B 01            [ 1] 1773         LD      (1,SP),A        ; CARRY
      008716 72 F9 03         [ 2] 1774         ADDW    Y,(3,SP)        ; PROD2
      008719 7B 01            [ 1] 1775         LD      A,(1,SP)        ; CARRY
      00871B A9 00            [ 1] 1776         ADC     A,#0            ; add 2nd carry
      00871D 6B 01            [ 1] 1777         LD      (1,SP),A        ; CARRY
      00871F 4F               [ 1] 1778         CLR     A
      008720 90 01            [ 1] 1779         RRWA    Y
      008722 E7 02            [ 1] 1780         LD      (2,X),A         ; 2nd product byte
      008724 72 F9 05         [ 2] 1781         ADDW    Y,(5,SP)        ; PROD1
      008727 90 01            [ 1] 1782         RRWA    Y
      008729 E7 01            [ 1] 1783         LD      (1,X),A         ; 3rd product byte
      00872B 90 01            [ 1] 1784         RRWA    Y               ; 4th product byte now in A
      00872D 19 01            [ 1] 1785         ADC     A,(1,SP)        ; CARRY
      00872F F7               [ 1] 1786         LD      (X),A
      008730 5B 06            [ 2] 1787         ADDW    SP,#6           ; drop temp storage
      008732 81               [ 4] 1788         RET
                                   1789 
                                   1790 ;       *       ( n n -- n )    ( TOS STM8: -- Y,Z,N )
                                   1791 ;       Signed multiply. Return single product.
                                   1792 
      0006A8                       1793         HEADER  STAR "*"
                           000001     1         .ifeq   UNLINK_STAR
      008733 86 E2                    2         .dw     LINK
                           0006AA     3         LINK    = .
      008735 01                       4         .db      (102$ - 101$)
      008736                          5 101$:
      008736 2A                       6         .ascii  "*"
      008737                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008737                       1794 STAR:
      008737 AD AD            [ 4] 1795         CALLR   UMSTA
      008739 CC 84 56         [ 2] 1796         JP      DROP
                                   1797 
                           000001  1798         .ifeq   UNLINKCORE
                                   1799 ;       M*      ( n n -- d )
                                   1800 ;       Signed multiply. Return double product.
      0006B1                       1801         HEADER  MSTAR "M*"
                           000001     1         .ifeq   UNLINK_MSTAR
      00873C 87 35                    2         .dw     LINK
                           0006B3     3         LINK    = .
      00873E 02                       4         .db      (102$ - 101$)
      00873F                          5 101$:
      00873F 4D 2A                    6         .ascii  "M*"
      008741                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008741                       1802 MSTAR:
      008741 E6 02            [ 1] 1803         LD      A,(2,X)         ; DDUP
      008743 F8               [ 1] 1804         XOR     A,(X)           ; XORR
      008744 88               [ 1] 1805         PUSH    A               ; TOR
      008745 CD 87 E0         [ 4] 1806         CALL    ABSS
      008748 CD 84 7C         [ 4] 1807         CALL    SWAPP
      00874B CD 87 E0         [ 4] 1808         CALL    ABSS
      00874E AD 96            [ 4] 1809         CALLR   UMSTA
      008750 84               [ 1] 1810         POP     A               ; RFROM
      008751 4D               [ 1] 1811         TNZ     A
      008752 2A 03            [ 1] 1812         JRPL    MSTA1           ; QBRAN
      008754 CD 85 C0         [ 4] 1813         CALL    DNEGA
      008757 81               [ 4] 1814 MSTA1:  RET
                                   1815 
                                   1816 ;       */MOD   ( n1 n2 n3 -- r q )
                                   1817 ;       Multiply n1 and n2, then divide
                                   1818 ;       by n3. Return mod and quotient.
      0006CD                       1819         HEADER  SSMOD "*/MOD"
                           000001     1         .ifeq   UNLINK_SSMOD
      008758 87 3E                    2         .dw     LINK
                           0006CF     3         LINK    = .
      00875A 05                       4         .db      (102$ - 101$)
      00875B                          5 101$:
      00875B 2A 2F 4D 4F 44           6         .ascii  "*/MOD"
      008760                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008760                       1820 SSMOD:
      008760 CD 84 3A         [ 4] 1821         CALL    TOR
      008763 AD DC            [ 4] 1822         CALLR   MSTAR
      008765 CD 84 13         [ 4] 1823         CALL    RFROM
      008768 CC 86 8C         [ 2] 1824         JP      MSMOD
                                   1825 
                                   1826 ;       */      ( n1 n2 n3 -- q )    ( TOS STM8: -- Y,Z,N )
                                   1827 ;       Multiply n1 by n2, then divide
                                   1828 ;       by n3. Return quotient only.
      0006E0                       1829         HEADER  STASL "*/"
                           000001     1         .ifeq   UNLINK_STASL
      00876B 87 5A                    2         .dw     LINK
                           0006E2     3         LINK    = .
      00876D 02                       4         .db      (102$ - 101$)
      00876E                          5 101$:
      00876E 2A 2F                    6         .ascii  "*/"
      008770                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008770                       1830 STASL:
      008770 AD EE            [ 4] 1831         CALLR   SSMOD
      008772 CC 84 4B         [ 2] 1832         JP      NIP
                                   1833         .endif
                                   1834 
                                   1835 ; Miscellaneous
                                   1836 
                                   1837 
                           000001  1838         .ifeq   BAREBONES
                                   1839 ;       EXG      ( n -- n )      ( TOS STM8: -- Y,Z,N )
                                   1840 ;       Exchange high with low byte of n.
                                   1841 
      0006EA                       1842         HEADER  EXG "EXG"
                           000001     1         .ifeq   UNLINK_EXG
      008775 87 6D                    2         .dw     LINK
                           0006EC     3         LINK    = .
      008777 03                       4         .db      (102$ - 101$)
      008778                          5 101$:
      008778 45 58 47                 6         .ascii  "EXG"
      00877B                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00877B                       1843 EXG:
      00877B AD 3A            [ 4] 1844         CALLR   DOXCODE
      00877D 5E               [ 1] 1845         SWAPW   X
      00877E 81               [ 4] 1846         RET
                                   1847         .endif
                                   1848 
                           000001  1849         .ifeq   BOOTSTRAP
                                   1850 ;       2/      ( n -- n )      ( TOS STM8: -- Y,Z,N )
                                   1851 ;       Divide tos by 2.
                                   1852 
      0006F4                       1853         HEADER  TWOSL "2/"
                           000001     1         .ifeq   UNLINK_TWOSL
      00877F 87 77                    2         .dw     LINK
                           0006F6     3         LINK    = .
      008781 02                       4         .db      (102$ - 101$)
      008782                          5 101$:
      008782 32 2F                    6         .ascii  "2/"
      008784                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008784                       1854 TWOSL:
      008784 AD 31            [ 4] 1855         CALLR   DOXCODE
      008786 57               [ 2] 1856         SRAW    X
      008787 81               [ 4] 1857         RET
                                   1858 
                                   1859 ;       2*      ( n -- n )      ( TOS STM8: -- Y,Z,N )
                                   1860 ;       Multiply tos by 2.
                                   1861 
      0006FD                       1862         HEADER  CELLS "2*"
                           000001     1         .ifeq   UNLINK_CELLS
      008788 87 81                    2         .dw     LINK
                           0006FF     3         LINK    = .
      00878A 02                       4         .db      (102$ - 101$)
      00878B                          5 101$:
      00878B 32 2A                    6         .ascii  "2*"
      00878D                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00878D                       1863 CELLS:
      00878D AD 28            [ 4] 1864         CALLR   DOXCODE
      00878F 58               [ 2] 1865         SLAW    X
      008790 81               [ 4] 1866         RET
                                   1867         .endif
                                   1868 
                                   1869 ;       2-      ( a -- a )      ( TOS STM8: -- Y,Z,N )
                                   1870 ;       Subtract 2 from tos.
                                   1871 
      000706                       1872         HEADER  CELLM "2-"
                           000001     1         .ifeq   UNLINK_CELLM
      008791 87 8A                    2         .dw     LINK
                           000708     3         LINK    = .
      008793 02                       4         .db      (102$ - 101$)
      008794                          5 101$:
      008794 32 2D                    6         .ascii  "2-"
      008796                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008796                       1873 CELLM:
      008796 AD 1F            [ 4] 1874         CALLR   DOXCODE
      008798 5A               [ 2] 1875         DECW    X
      008799 5A               [ 2] 1876         DECW    X
      00879A 81               [ 4] 1877         RET
                                   1878 
                                   1879 ;       2+      ( a -- a )      ( TOS STM8: -- Y,Z,N )
                                   1880 ;       Add 2 to tos.
                                   1881 
      000710                       1882         HEADER  CELLP "2+"
                           000001     1         .ifeq   UNLINK_CELLP
      00879B 87 93                    2         .dw     LINK
                           000712     3         LINK    = .
      00879D 02                       4         .db      (102$ - 101$)
      00879E                          5 101$:
      00879E 32 2B                    6         .ascii  "2+"
      0087A0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0087A0                       1883 CELLP:
      0087A0 AD 15            [ 4] 1884         CALLR   DOXCODE
      0087A2 5C               [ 1] 1885         INCW    X
      0087A3 5C               [ 1] 1886         INCW    X
      0087A4 81               [ 4] 1887         RET
                                   1888 
                                   1889 ;       1-      ( n -- n )      ( TOS STM8: -- Y,Z,N )
                                   1890 ;       Subtract 1 from tos.
                                   1891 
      00071A                       1892         HEADER  ONEM "1-"
                           000001     1         .ifeq   UNLINK_ONEM
      0087A5 87 9D                    2         .dw     LINK
                           00071C     3         LINK    = .
      0087A7 02                       4         .db      (102$ - 101$)
      0087A8                          5 101$:
      0087A8 31 2D                    6         .ascii  "1-"
      0087AA                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0087AA                       1893 ONEM:
      0087AA AD 0B            [ 4] 1894         CALLR   DOXCODE
      0087AC 5A               [ 2] 1895         DECW    X
      0087AD 81               [ 4] 1896         RET
                                   1897 
                                   1898 ;       1+      ( n -- n )      ( TOS STM8: -- Y,Z,N )
                                   1899 ;       Add 1 to tos.
                                   1900 
      000723                       1901         HEADER  ONEP "1+"
                           000001     1         .ifeq   UNLINK_ONEP
      0087AE 87 A7                    2         .dw     LINK
                           000725     3         LINK    = .
      0087B0 02                       4         .db      (102$ - 101$)
      0087B1                          5 101$:
      0087B1 31 2B                    6         .ascii  "1+"
      0087B3                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0087B3                       1902 ONEP:
      0087B3 AD 02            [ 4] 1903         CALLR   DOXCODE
      0087B5 5C               [ 1] 1904         INCW    X
      0087B6 81               [ 4] 1905         RET
                                   1906 
                                   1907 ;       DOXCODE   ( n - n )   ( TOS STM8: - Y,Z,N )
                                   1908 ;       DOXCODE precedes assembly code for a primitive word
                                   1909 ;       In the assembly code: X=(TOS), YTEMP=TOS. (TOS)=X after RET
                                   1910 ;       Caution: no other Forth word may be called from assembly!
      0087B7                       1911 DOXCODE:
      0087B7 90 85            [ 2] 1912         POPW    Y
      0087B9 BF 7E            [ 2] 1913         LDW     YTEMP,X
      0087BB FE               [ 2] 1914         LDW     X,(X)
      0087BC 90 FD            [ 4] 1915         CALL    (Y)
      0087BE 51               [ 1] 1916         EXGW    X,Y
      0087BF BE 7E            [ 2] 1917         LDW     X,YTEMP
      0087C1 FF               [ 2] 1918         LDW     (X),Y
      0087C2 81               [ 4] 1919         RET
                                   1920 
                                   1921 ;       NOT     ( w -- w )     ( TOS STM8: -- Y,Z,N )
                                   1922 ;       One's complement of TOS.
                                   1923 
      000738                       1924         HEADER  INVER "NOT"
                           000001     1         .ifeq   UNLINK_INVER
      0087C3 87 B0                    2         .dw     LINK
                           00073A     3         LINK    = .
      0087C5 03                       4         .db      (102$ - 101$)
      0087C6                          5 101$:
      0087C6 4E 4F 54                 6         .ascii  "NOT"
      0087C9                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0087C9                       1925 INVER:
      0087C9 AD EC            [ 4] 1926         CALLR   DOXCODE
      0087CB 53               [ 2] 1927         CPLW    X
      0087CC 81               [ 4] 1928         RET
                                   1929 
                                   1930 ;       NEGATE  ( n -- -n )     ( TOS STM8: -- Y,Z,N )
                                   1931 ;       Two's complement of TOS.
                                   1932 
      000742                       1933         HEADER  NEGAT "NEGATE"
                           000001     1         .ifeq   UNLINK_NEGAT
      0087CD 87 C5                    2         .dw     LINK
                           000744     3         LINK    = .
      0087CF 06                       4         .db      (102$ - 101$)
      0087D0                          5 101$:
      0087D0 4E 45 47 41 54 45        6         .ascii  "NEGATE"
      0087D6                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0087D6                       1934 NEGAT:
      0087D6 AD DF            [ 4] 1935         CALLR   DOXCODE
      0087D8 50               [ 2] 1936         NEGW    X
      0087D9 81               [ 4] 1937         RET
                                   1938 
                                   1939 ;       ABS     ( n -- n )      ( TOS STM8: -- Y,Z,N )
                                   1940 ;       Return  absolute value of n.
                                   1941 
      00074F                       1942         HEADER  ABSS "ABS"
                           000001     1         .ifeq   UNLINK_ABSS
      0087DA 87 CF                    2         .dw     LINK
                           000751     3         LINK    = .
      0087DC 03                       4         .db      (102$ - 101$)
      0087DD                          5 101$:
      0087DD 41 42 53                 6         .ascii  "ABS"
      0087E0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0087E0                       1943 ABSS:
      0087E0 AD D5            [ 4] 1944         CALLR   DOXCODE
      0087E2 2A 01            [ 1] 1945         JRPL    1$              ; positive?
      0087E4 50               [ 2] 1946         NEGW    X               ; else negate
      0087E5 81               [ 4] 1947 1$:     RET
                                   1948 
                                   1949 ;       0=      ( n -- t )      ( TOS STM8: -- Y,Z,N ))
                                   1950 ;       Return true if n is equal to 0
                                   1951 
                           000001  1952         .ifne   WORDS_EXTRACORE
      00075B                       1953         HEADER  ZEQUAL "0="
                           000001     1         .ifeq   UNLINK_ZEQUAL
      0087E6 87 DC                    2         .dw     LINK
                           00075D     3         LINK    = .
      0087E8 02                       4         .db      (102$ - 101$)
      0087E9                          5 101$:
      0087E9 30 3D                    6         .ascii  "0="
      0087EB                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   1954         .endif
      0087EB                       1955 ZEQUAL:
      0087EB AD CA            [ 4] 1956         CALLR   DOXCODE
      0087ED 27 02            [ 1] 1957         JREQ    1$
      0087EF 5F               [ 1] 1958         CLRW    X
      0087F0 81               [ 4] 1959         RET
      0087F1 53               [ 2] 1960 1$:     CPLW    X               ; else -1
      0087F2 81               [ 4] 1961         RET
                                   1962 
                                   1963 ;       PICK    ( ... +n -- ... w )      ( TOS STM8: -- Y,Z,N )
                                   1964 ;       Copy    nth stack item to tos.
                                   1965 
      000768                       1966         HEADER  PICK "PICK"
                           000001     1         .ifeq   UNLINK_PICK
      0087F3 87 E8                    2         .dw     LINK
                           00076A     3         LINK    = .
      0087F5 04                       4         .db      (102$ - 101$)
      0087F6                          5 101$:
      0087F6 50 49 43 4B              6         .ascii  "PICK"
      0087FA                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0087FA                       1967 PICK:
      0087FA AD BB            [ 4] 1968         CALLR   DOXCODE
      0087FC 58               [ 2] 1969         SLAW    X
      0087FD 72 BB 00 7E      [ 2] 1970         ADDW    X,YTEMP
      008801 FE               [ 2] 1971         LDW     X,(X)
      008802 81               [ 4] 1972         RET
                                   1973 
                           000001  1974         .ifeq   BOOTSTRAP
                                   1975 ;       >CHAR   ( c -- c )      ( TOS STM8: -- A,Z,N )
                                   1976 ;       Filter non-printing characters.
                                   1977 
                           000000  1978         .ifne   WORDS_LINKMISC
                                   1979         HEADER  TCHAR ">CHAR"
                                   1980         .endif
      008803                       1981 TCHAR:
      008803 E6 01            [ 1] 1982         LD      A,(1,X)
      008805 A1 7F            [ 1] 1983         CP      A,#0x7F
      008807 24 04            [ 1] 1984         JRUGE   1$
      008809 A1 20            [ 1] 1985         CP      A,#(' ')
      00880B 24 02            [ 1] 1986         JRUGE   2$
      00880D A6 5F            [ 1] 1987 1$:     LD      A,#('_')
      00880F E7 01            [ 1] 1988 2$:     LD      (1,X),A
      008811 81               [ 4] 1989         RET
                                   1990         .endif
                                   1991 
                                   1992 ;       DEPTH   ( -- n )      ( TOS STM8: -- Y,Z,N )
                                   1993 ;       Return  depth of data stack.
                                   1994 
                           000001  1995         .ifeq   BAREBONES
      000787                       1996         HEADER  DEPTH "DEPTH"
                           000001     1         .ifeq   UNLINK_DEPTH
      008812 87 F5                    2         .dw     LINK
                           000789     3         LINK    = .
      008814 05                       4         .db      (102$ - 101$)
      008815                          5 101$:
      008815 44 45 50 54 48           6         .ascii  "DEPTH"
      00881A                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   1997         .endif
      00881A                       1998 DEPTH:
      00881A 90 93            [ 1] 1999         LDW     Y,X
      00881C 50               [ 2] 2000         NEGW    X
      00881D 1C 03 20         [ 2] 2001         ADDW    X,#SPP
      008820 57               [ 2] 2002         SRAW    X
                                   2003         ;DECW    X               ; fixed: off-by-one to compensate error in "rp!"
      008821 51               [ 1] 2004         EXGW    X,Y
      008822 CC 84 28         [ 2] 2005         JP      YSTOR
                                   2006 
                                   2007 ; Memory access
                                   2008 
                                   2009 ;       +!      ( n a -- )      ( TOS STM8: -- Y,Z,N )
                                   2010 ;       Add n to contents at address a.
                                   2011 
      00079A                       2012         HEADER  PSTOR "+!"
                           000001     1         .ifeq   UNLINK_PSTOR
      008825 88 14                    2         .dw     LINK
                           00079C     3         LINK    = .
      008827 02                       4         .db      (102$ - 101$)
      008828                          5 101$:
      008828 2B 21                    6         .ascii  "+!"
      00882A                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00882A                       2013 PSTOR:
      00882A 90 93            [ 1] 2014         LDW     Y,X
      00882C FE               [ 2] 2015         LDW     X,(X)
      00882D BF 7E            [ 2] 2016         LDW     YTEMP,X
      00882F 93               [ 1] 2017         LDW     X,Y
      008830 EE 02            [ 2] 2018         LDW     X,(2,X)
      008832 89               [ 2] 2019         PUSHW   X
      008833 92 CE 7E         [ 5] 2020         LDW     X,[YTEMP]
      008836 72 FB 01         [ 2] 2021         ADDW    X,(1,SP)
      008839 92 CF 7E         [ 5] 2022         LDW     [YTEMP],X
      00883C 85               [ 2] 2023         POPW    X
      00883D 51               [ 1] 2024         EXGW    X,Y
      00883E CC 84 65         [ 2] 2025         JP      DDROP
                                   2026 
                                   2027 ;       COUNT   ( b -- b +n )      ( TOS STM8: -- A,Z,N )
                                   2028 ;       Return count byte of a string
                                   2029 ;       and add 1 to byte address.
                                   2030 
                           000001  2031         .ifeq   BAREBONES
      0007B6                       2032         HEADER  COUNT "COUNT"
                           000001     1         .ifeq   UNLINK_COUNT
      008841 88 27                    2         .dw     LINK
                           0007B8     3         LINK    = .
      008843 05                       4         .db      (102$ - 101$)
      008844                          5 101$:
      008844 43 4F 55 4E 54           6         .ascii  "COUNT"
      008849                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2033         .endif
      008849                       2034 COUNT:
      008849 CD 84 6F         [ 4] 2035         CALL    DUPP
      00884C CD 87 B3         [ 4] 2036         CALL    ONEP
      00884F CD 84 7C         [ 4] 2037         CALL    SWAPP
      008852 CC 83 F5         [ 2] 2038         JP      CAT
                                   2039 
                           000001  2040         .ifne  HAS_CPNVM
      008855                       2041 RAMHERE:
      008855 CD 93 F8         [ 4] 2042         CALL    NVMQ
      008858 27 0D            [ 1] 2043         JREQ    HERE            ; NVM: CP points to NVM, NVMCP points to RAM
      0007CF                       2044         DoLitW  NVMCP           ; 'eval in Interpreter mode: HERE returns pointer to RAM
      00885A 83               [ 9]    1         TRAP
      00885B 00 6E                    2         .dw     NVMCP
      00885D CC 83 D9         [ 2] 2045         JP      AT
                           000000  2046         .else
                                   2047         RAMHERE = HERE
                                   2048         .endif
                                   2049 
                                   2050 ;       HERE    ( -- a )      ( TOS STM8: -- Y,Z,N )
                                   2051 ;       Return  top of  code dictionary.
                                   2052 
      0007D5                       2053         HEADER  HERE "HERE"
                           000001     1         .ifeq   UNLINK_HERE
      008860 88 43                    2         .dw     LINK
                           0007D7     3         LINK    = .
      008862 04                       4         .db      (102$ - 101$)
      008863                          5 101$:
      008863 48 45 52 45              6         .ascii  "HERE"
      008867                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008867                       2054 HERE:
      008867 CD 85 13         [ 4] 2055         CALL    CPP
      00886A CC 83 D9         [ 2] 2056         JP      AT
                                   2057 
                                   2058 ;       PAD     ( -- a )  ( TOS STM8: invalid )
                                   2059 ;       Return address of text buffer
                                   2060 ;       above code dictionary.
                                   2061 
                           000001  2062         .ifne   WORDS_LINKCHAR
      00886D                       2063         HEADER  PAD "PAD"
                           000000     1         .ifeq   UNLINK_PAD
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "PAD"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2064         .endif
      0007E2                       2065 PAD:
                           000001  2066         .ifne   HAS_BACKGROUND
                                   2067         ; get PAD area address (offset or dedicated) for PAD area
      00886D 8A               [ 1] 2068         PUSH    CC              ; Test interrupt level flags in CC
      00886E 84               [ 1] 2069         POP     A
      00886F A4 20            [ 1] 2070         AND     A,#0x20
      008871 26 04            [ 1] 2071         JRNE    1$
      0007E8                       2072         DoLitC  (PADBG+1)       ; dedicated memory for PAD in background task
      008873 83               [ 9]    1         TRAP
      008874 00 60                    2         .dw     (PADBG+1)
      008876 81               [ 4] 2073         RET
      008877                       2074 1$:
                                   2075         .endif
      008877 AD DC            [ 4] 2076         CALLR   RAMHERE         ; regular PAD with offset to HERE
      0007EE                       2077         DoLitC  PADOFFS
      008879 83               [ 9]    1         TRAP
      00887A 00 50                    2         .dw     PADOFFS
      00887C CC 84 B0         [ 2] 2078         JP      PLUS
                                   2079 
                           000000  2080         .ifeq   UNLINK_ATEXE
                                   2081 ;       @EXECUTE        ( a -- )  ( TOS STM8: undefined )
                                   2082 ;       Execute vector stored in address a.
                                   2083 
                                   2084         HEADER  ATEXE "@EXECUTE"
                                   2085 ATEXE:
                                   2086         CALL    YFLAGS
                                   2087         LDW     Y,(Y)
                                   2088         JREQ    1$
                                   2089         JP      (Y)
                                   2090 1$:     RET
                                   2091         .endif
                                   2092 
                                   2093 ;       CMOVE   ( b1 b2 u -- )
                                   2094 ;       Copy u bytes from b1 to b2.
                                   2095 
      0007F4                       2096         HEADER  CMOVE "CMOVE"
                           000001     1         .ifeq   UNLINK_CMOVE
      00887F 88 62                    2         .dw     LINK
                           0007F6     3         LINK    = .
      008881 05                       4         .db      (102$ - 101$)
      008882                          5 101$:
      008882 43 4D 4F 56 45           6         .ascii  "CMOVE"
      008887                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008887                       2097 CMOVE:
      008887 CD 84 3A         [ 4] 2098         CALL    TOR
      00888A 20 15            [ 2] 2099         JRA     CMOV2
      00888C CD 84 3A         [ 4] 2100 CMOV1:  CALL    TOR
      00888F CD 92 AC         [ 4] 2101         CALL    DUPPCAT
      008892 CD 84 31         [ 4] 2102         CALL    RAT
      008895 CD 84 04         [ 4] 2103         CALL    CSTOR
      008898 CD 87 B3         [ 4] 2104         CALL    ONEP
      00889B CD 84 13         [ 4] 2105         CALL    RFROM
      00889E CD 87 B3         [ 4] 2106         CALL    ONEP
      0088A1 CD 83 26         [ 4] 2107 CMOV2:  CALL    DONXT
      0088A4 88 8C                 2108         .dw     CMOV1
      0088A6 CC 84 65         [ 2] 2109         JP      DDROP
                                   2110 
                                   2111 ;       FILL    ( b u c -- )
                                   2112 ;       Fill u bytes of character c
                                   2113 ;       to area beginning at b.
                                   2114 
      00081E                       2115         HEADER  FILL "FILL"
                           000001     1         .ifeq   UNLINK_FILL
      0088A9 88 81                    2         .dw     LINK
                           000820     3         LINK    = .
      0088AB 04                       4         .db      (102$ - 101$)
      0088AC                          5 101$:
      0088AC 46 49 4C 4C              6         .ascii  "FILL"
      0088B0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0088B0                       2116 FILL:
      0088B0 CD 84 7C         [ 4] 2117         CALL    SWAPP
      0088B3 CD 84 3A         [ 4] 2118         CALL    TOR
      0088B6 CD 84 7C         [ 4] 2119         CALL    SWAPP
      0088B9 20 09            [ 2] 2120         JRA     FILL2
      0088BB CD 85 B1         [ 4] 2121 FILL1:  CALL    DDUP
      0088BE CD 84 04         [ 4] 2122         CALL    CSTOR
      0088C1 CD 87 B3         [ 4] 2123         CALL    ONEP
      0088C4 CD 83 26         [ 4] 2124 FILL2:  CALL    DONXT
      0088C7 88 BB                 2125         .dw     FILL1
      0088C9 CC 84 65         [ 2] 2126         JP      DDROP
                                   2127 
                           000001  2128         .ifeq   BAREBONES
                                   2129 ;       ERASE   ( b u -- )
                                   2130 ;       Erase u bytes beginning at b.
                                   2131 
      000841                       2132         HEADER  ERASE "ERASE"
                           000001     1         .ifeq   UNLINK_ERASE
      0088CC 88 AB                    2         .dw     LINK
                           000843     3         LINK    = .
      0088CE 05                       4         .db      (102$ - 101$)
      0088CF                          5 101$:
      0088CF 45 52 41 53 45           6         .ascii  "ERASE"
      0088D4                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0088D4                       2133 ERASE:
      0088D4 CD 85 66         [ 4] 2134         CALL    ZERO
      0088D7 20 D7            [ 2] 2135         JRA     FILL
                                   2136         .endif
                                   2137 
                                   2138 ;       PACK$   ( b u a -- a )
                                   2139 ;       Build a counted string with
                                   2140 ;       u characters from b. Null fill.
                                   2141 
      0088D9                       2142         HEADER  PACKS "PACK$"
                           000000     1         .ifeq   UNLINK_PACKS
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "PACK$"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      00084E                       2143 PACKS:
      0088D9 CD 84 6F         [ 4] 2144         CALL    DUPP
      0088DC CD 84 3A         [ 4] 2145         CALL    TOR             ; strings only on cell boundary
      0088DF CD 85 B1         [ 4] 2146         CALL    DDUP
      0088E2 CD 84 04         [ 4] 2147         CALL    CSTOR
      0088E5 CD 87 B3         [ 4] 2148         CALL    ONEP            ; save count
      0088E8 CD 84 7C         [ 4] 2149         CALL    SWAPP
      0088EB AD 9A            [ 4] 2150         CALLR   CMOVE
      0088ED CD 84 13         [ 4] 2151         CALL    RFROM
      0088F0 81               [ 4] 2152         RET
                                   2153 
                                   2154 ; Numeric output, single precision
                                   2155 
                                   2156 ;       DIGIT   ( u -- c )      ( TOS STM8: -- Y,Z,N )
                                   2157 ;       Convert digit u to a character.
                                   2158 
                           000001  2159         .ifne   WORDS_LINKCHAR
      0088F1                       2160         HEADER  DIGIT "DIGIT"
                           000000     1         .ifeq   UNLINK_DIGIT
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "DIGIT"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2161         .endif
      000866                       2162 DIGIT:
      0088F1 E6 01            [ 1] 2163         LD      A,(1,X)
      0088F3 A1 0A            [ 1] 2164         CP      A,#10
      0088F5 2B 02            [ 1] 2165         JRMI    1$
      0088F7 AB 07            [ 1] 2166         ADD     A,#7
      0088F9 AB 30            [ 1] 2167 1$:     ADD     A,#48
      0088FB E7 01            [ 1] 2168         LD      (1,X),A
      0088FD 81               [ 4] 2169         RET
                                   2170 
                                   2171 ;       EXTRACT ( n base -- n c )   ( TOS STM8: -- Y,Z,N )
                                   2172 ;       Extract least significant digit from n.
                                   2173 
      0088FE                       2174         HEADER  EXTRC "EXTRACT"
                           000000     1         .ifeq   UNLINK_EXTRC
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "EXTRACT"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000873                       2175 EXTRC:
      0088FE CD 85 66         [ 4] 2176         CALL    ZERO
      008901 CD 84 7C         [ 4] 2177         CALL    SWAPP
      008904 CD 86 48         [ 4] 2178         CALL    UMMOD
      008907 CD 84 7C         [ 4] 2179         CALL    SWAPP
      00890A 20 E5            [ 2] 2180         JRA     DIGIT
                                   2181 
                                   2182 ;       #>      ( w -- b u )
                                   2183 ;       Prepare output string.
                                   2184 
                           000001  2185         .ifne   WORDS_LINKCHAR
      000881                       2186         HEADER  EDIGS "#>"
                           000001     1         .ifeq   UNLINK_EDIGS
      00890C 88 CE                    2         .dw     LINK
                           000883     3         LINK    = .
      00890E 02                       4         .db      (102$ - 101$)
      00890F                          5 101$:
      00890F 23 3E                    6         .ascii  "#>"
      008911                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2187         .endif
      008911                       2188 EDIGS:
      008911 90 BE 78         [ 2] 2189         LDW     Y,USRHLD        ; DROP HLD
      008914 FF               [ 2] 2190         LDW     (X),Y
      008915 CD 88 6D         [ 4] 2191         CALL    PAD
      008918 CD 84 91         [ 4] 2192         CALL    OVER
      00891B CC 85 01         [ 2] 2193         JP      SUBB
                                   2194 
                                   2195 ;       #       ( u -- u )    ( TOS STM8: -- Y,Z,N )
                                   2196 ;       Extract one digit from u and
                                   2197 ;       append digit to output string.
                                   2198 
                           000001  2199         .ifne   WORDS_LINKCHAR
      000893                       2200         HEADER  DIG "#"
                           000001     1         .ifeq   UNLINK_DIG
      00891E 89 0E                    2         .dw     LINK
                           000895     3         LINK    = .
      008920 01                       4         .db      (102$ - 101$)
      008921                          5 101$:
      008921 23                       6         .ascii  "#"
      008922                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2201         .endif
      008922                       2202 DIG:
      008922 AD 6F            [ 4] 2203         CALLR   BASEAT
      008924 AD D8            [ 4] 2204         CALLR   EXTRC
      008926 20 11            [ 2] 2205         JRA     HOLD
                                   2206 
                                   2207 ;       #S      ( u -- 0 )
                                   2208 ;       Convert u until all digits
                                   2209 ;       are added to output string.
                                   2210 
                           000001  2211         .ifne   WORDS_LINKCHAR
      00089D                       2212         HEADER  DIGS "#S"
                           000001     1         .ifeq   UNLINK_DIGS
      008928 89 20                    2         .dw     LINK
                           00089F     3         LINK    = .
      00892A 02                       4         .db      (102$ - 101$)
      00892B                          5 101$:
      00892B 23 53                    6         .ascii  "#S"
      00892D                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2213         .endif
      00892D                       2214 DIGS:
      00892D AD F3            [ 4] 2215 DIGS1:  CALLR   DIG
      00892F 26 FC            [ 1] 2216         JRNE    DIGS1
      008931 81               [ 4] 2217         RET
                                   2218 
                                   2219 ;       HOLD    ( c -- )    ( TOS STM8: -- Y,Z,N )
                                   2220 ;       Insert a character into output string.
                                   2221 
                           000001  2222         .ifne   WORDS_LINKCHAR
      0008A7                       2223         HEADER  HOLD "HOLD"
                           000001     1         .ifeq   UNLINK_HOLD
      008932 89 2A                    2         .dw     LINK
                           0008A9     3         LINK    = .
      008934 04                       4         .db      (102$ - 101$)
      008935                          5 101$:
      008935 48 4F 4C 44              6         .ascii  "HOLD"
      008939                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2224         .endif
      008939                       2225 HOLD:
      008939 E6 01            [ 1] 2226         LD      A,(1,X)         ; A < c
      00893B 51               [ 1] 2227         EXGW    X,Y
      00893C BE 78            [ 2] 2228         LDW     X,USRHLD        ; HLD @
      00893E 5A               [ 2] 2229         DECW    X               ; 1 -
      00893F BF 78            [ 2] 2230         LDW     USRHLD,X        ; DUP HLD !
      008941 F7               [ 1] 2231         LD      (X),A           ; C!
      008942 51               [ 1] 2232         EXGW    X,Y
      008943                       2233 H_DROP:
      008943 CC 84 56         [ 2] 2234         JP      DROP
                                   2235 
                                   2236 ;       SIGN    ( n -- )
                                   2237 ;       Add a minus sign to
                                   2238 ;       numeric output string.
                                   2239 
                           000001  2240         .ifne   WORDS_LINKCHAR
      0008BB                       2241         HEADER  SIGN "SIGN"
                           000001     1         .ifeq   UNLINK_SIGN
      008946 89 34                    2         .dw     LINK
                           0008BD     3         LINK    = .
      008948 04                       4         .db      (102$ - 101$)
      008949                          5 101$:
      008949 53 49 47 4E              6         .ascii  "SIGN"
      00894D                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2242         .endif
      00894D                       2243 SIGN:
      00894D 7D               [ 1] 2244         TNZ     (X)
      00894E 2A F3            [ 1] 2245         JRPL    H_DROP
      008950 A6 2D            [ 1] 2246         LD      A,#('-')
      008952 E7 01            [ 1] 2247         LD      (1,X),A
      008954 20 E3            [ 2] 2248         JRA     HOLD
                                   2249 
                                   2250 ;       <#      ( -- )   ( TOS STM8: -- Y,Z,N )
                                   2251 ;       Initiate numeric output process.
                                   2252 
                           000001  2253         .ifne   WORDS_LINKCHAR
      0008CB                       2254         HEADER  BDIGS "<#"
                           000001     1         .ifeq   UNLINK_BDIGS
      008956 89 48                    2         .dw     LINK
                           0008CD     3         LINK    = .
      008958 02                       4         .db      (102$ - 101$)
      008959                          5 101$:
      008959 3C 23                    6         .ascii  "<#"
      00895B                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2255         .endif
      00895B                       2256 BDIGS:
      00895B CD 88 6D         [ 4] 2257         CALL    PAD
      0008D3                       2258         DoLitC  USRHLD
      00895E 83               [ 9]    1         TRAP
      00895F 00 78                    2         .dw     USRHLD
      008961 CC 83 E4         [ 2] 2259         JP      STORE
                                   2260 
                                   2261 ;       str     ( w -- b u )
                                   2262 ;       Convert a signed integer
                                   2263 ;       to a numeric string.
                                   2264 
      008964                       2265         HEADER  STR "str"
                           000000     1         .ifeq   UNLINK_STR
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "str"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      0008D9                       2266 STR:
      008964 CD 84 6F         [ 4] 2267         CALL    DUPP
      008967 CD 84 3A         [ 4] 2268         CALL    TOR
      00896A CD 87 E0         [ 4] 2269         CALL    ABSS
      00896D AD EC            [ 4] 2270         CALLR   BDIGS
      00896F AD BC            [ 4] 2271         CALLR   DIGS
      008971 CD 84 13         [ 4] 2272         CALL    RFROM
      008974 AD D7            [ 4] 2273         CALLR   SIGN
      008976 20 99            [ 2] 2274         JRA     EDIGS
                                   2275 
                                   2276 ;       HEX     ( -- )
                                   2277 ;       Use radix 16 as base for
                                   2278 ;       numeric conversions.
                                   2279 
      0008ED                       2280         HEADER  HEX "HEX"
                           000001     1         .ifeq   UNLINK_HEX
      008978 89 58                    2         .dw     LINK
                           0008EF     3         LINK    = .
      00897A 03                       4         .db      (102$ - 101$)
      00897B                          5 101$:
      00897B 48 45 58                 6         .ascii  "HEX"
      00897E                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00897E                       2281 HEX:
      00897E A6 10            [ 1] 2282         LD      A,#16
      008980 20 0C            [ 2] 2283         JRA     BASESET
                                   2284 
                                   2285 ;       DECIMAL ( -- )
                                   2286 ;       Use radix 10 as base
                                   2287 ;       for numeric conversions.
                                   2288 
      0008F7                       2289         HEADER  DECIM "DECIMAL"
                           000001     1         .ifeq   UNLINK_DECIM
      008982 89 7A                    2         .dw     LINK
                           0008F9     3         LINK    = .
      008984 07                       4         .db      (102$ - 101$)
      008985                          5 101$:
      008985 44 45 43 49 4D 41 4C     6         .ascii  "DECIMAL"
      00898C                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00898C                       2290 DECIM:
      00898C A6 0A            [ 1] 2291         LD      A,#10
      00898E                       2292 BASESET:
      00898E B7 65            [ 1] 2293         LD      USRBASE+1,A
      008990 3F 64            [ 1] 2294         CLR     USRBASE
      008992 81               [ 4] 2295         RET
                                   2296 
                                   2297 ;       BASE@     ( -- u )
                                   2298 ;       Get BASE value
      008993                       2299 BASEAT:
      008993 B6 65            [ 1] 2300         LD      A,USRBASE+1
      008995 CC 85 43         [ 2] 2301         JP      ASTOR
                                   2302 
                                   2303 ; Numeric input, single precision
                                   2304 
                                   2305 ;       NUMBER? ( a -- n T | a F )
                                   2306 ;       Convert a number string to
                                   2307 ;       integer. Push a flag on tos.
                                   2308 
      008998                       2309         HEADER  NUMBQ "NUMBER?"
                           000000     1         .ifeq   UNLINK_NUMBQ
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "NUMBER?"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      00090D                       2310 NUMBQ:
      008998 90 BE 64         [ 2] 2311         LDW      Y,USRBASE
      00899B 90 89            [ 2] 2312         PUSHW    Y              ; note: (1,SP) used as sign flag
                                   2313 
      00899D CD 85 66         [ 4] 2314         CALL    ZERO
      0089A0 CD 84 91         [ 4] 2315         CALL    OVER
      0089A3 CD 88 49         [ 4] 2316         CALL    COUNT
      0089A6                       2317 NUMQ0:
      0089A6 CD 84 91         [ 4] 2318         CALL    OVER
      0089A9 CD 83 F5         [ 4] 2319         CALL    CAT
      0089AC CD 8B 26         [ 4] 2320         CALL    AFLAGS
                                   2321 
      0089AF A1 24            [ 1] 2322         CP      A,#('$')
      0089B1 26 04            [ 1] 2323         JRNE    1$
      0089B3 AD C9            [ 4] 2324         CALLR   HEX
      0089B5 20 19            [ 2] 2325         JRA     NUMQSKIP
      0089B7                       2326 1$:
                           000001  2327         .ifeq   BAREBONES
      0089B7 A1 25            [ 1] 2328         CP      A,#('%')
      0089B9 26 06            [ 1] 2329         JRNE    2$
      0089BB 35 02 00 65      [ 1] 2330         MOV     USRBASE+1,#2
      0089BF 20 0F            [ 2] 2331         JRA     NUMQSKIP
      0089C1                       2332 2$:
      0089C1 A1 26            [ 1] 2333         CP      A,#('&')
      0089C3 26 04            [ 1] 2334         JRNE    3$
      0089C5 AD C5            [ 4] 2335         CALLR   DECIM
      0089C7 20 07            [ 2] 2336         JRA     NUMQSKIP
      0089C9                       2337 3$:
                                   2338         .endif
      0089C9 A1 2D            [ 1] 2339         CP      A,#('-')
      0089CB 26 11            [ 1] 2340         JRNE    NUMQ1
      0089CD 84               [ 1] 2341         POP     A
      0089CE 4B 80            [ 1] 2342         PUSH    #0x80           ; flag ?sign
                                   2343 
      0089D0                       2344 NUMQSKIP:
      0089D0 CD 84 7C         [ 4] 2345         CALL    SWAPP
      0089D3 CD 87 B3         [ 4] 2346         CALL    ONEP
      0089D6 CD 84 7C         [ 4] 2347         CALL    SWAPP
      0089D9 CD 87 AA         [ 4] 2348         CALL    ONEM
      0089DC 26 C8            [ 1] 2349         JRNE    NUMQ0           ; check for more modifiers
                                   2350 
      0089DE                       2351 NUMQ1:
      0089DE CD 83 37         [ 4] 2352         CALL    QDQBRAN
      0089E1 8A 29                 2353         .dw     NUMQ6
      0089E3 CD 87 AA         [ 4] 2354         CALL    ONEM
      0089E6 CD 84 3A         [ 4] 2355         CALL    TOR             ; FOR
      0089E9 CD 84 6F         [ 4] 2356 NUMQ2:  CALL    DUPP
      0089EC CD 84 3A         [ 4] 2357         CALL    TOR
      0089EF CD 83 F5         [ 4] 2358         CALL    CAT
      0089F2 AD 9F            [ 4] 2359         CALLR   BASEAT
      0089F4 AD 3A            [ 4] 2360         CALLR   DIGTQ
      0089F6 CD 83 3C         [ 4] 2361         CALL    QBRAN           ; WHILE ( no digit -> LEAVE )
      0089F9 8A 1F                 2362         .dw     NUMLEAVE
                                   2363 
      0089FB CD 84 7C         [ 4] 2364         CALL    SWAPP
      0089FE AD 93            [ 4] 2365         CALLR   BASEAT
      008A00 CD 87 37         [ 4] 2366         CALL    STAR
      008A03 CD 84 B0         [ 4] 2367         CALL    PLUS
                                   2368 
      008A06 CD 84 13         [ 4] 2369         CALL    RFROM
      008A09 CD 87 B3         [ 4] 2370         CALL    ONEP
                                   2371 
      008A0C CD 83 26         [ 4] 2372         CALL    DONXT           ; NEXT
      008A0F 89 E9                 2373         .dw     NUMQ2
                                   2374 
      008A11 AD 1A            [ 4] 2375         CALLR   NUMDROP         ; drop b
                                   2376 
      008A13 7B 01            [ 1] 2377         LD      A,(1,SP)        ; test sign flag
      008A15 2A 03            [ 1] 2378         JRPL    NUMPLUS
      008A17 CD 87 D6         [ 4] 2379         CALL    NEGAT
      008A1A                       2380 NUMPLUS:
      008A1A CD 84 7C         [ 4] 2381         CALL    SWAPP
      008A1D 20 07            [ 2] 2382         JRA     NUMQ5
      008A1F                       2383 NUMLEAVE:                       ; LEAVE ( clean-up FOR .. NEXT )
      008A1F 5B 04            [ 2] 2384         ADDW    SP,#4           ; RFROM,RFROM,DDROP
      008A21 AD 0A            [ 4] 2385         CALLR   NUMDROP         ; DDROP 0
      008A23 7F               [ 1] 2386         CLR     (X)
      008A24 6F 01            [ 1] 2387         CLR     (1,X)
                                   2388         ; fall through
      008A26                       2389 NUMQ5:                          ; THEN
      008A26 CD 84 6F         [ 4] 2390         CALL    DUPP
                                   2391         ; fall through
      008A29                       2392 NUMQ6:
      008A29 84               [ 1] 2393         POP     A               ; discard sign flag
      008A2A 84               [ 1] 2394         POP     A               ; restore BASE
      008A2B B7 65            [ 1] 2395         LD      USRBASE+1,A
      008A2D                       2396 NUMDROP:
      008A2D CC 84 56         [ 2] 2397         JP      DROP
                                   2398 
                                   2399 ;       DIGIT?  ( c base -- u t )
                                   2400 ;       Convert a character to its numeric
                                   2401 ;       value. A flag indicates success.
                                   2402 
      008A30                       2403         HEADER  DIGTQ "DIGIT?"
                           000000     1         .ifeq   UNLINK_DIGTQ
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "DIGIT?"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      0009A5                       2404 DIGTQ:
      008A30 CD 84 3A         [ 4] 2405         CALL    TOR
      008A33 90 9F            [ 1] 2406         LD      A,YL
      008A35 A0 30            [ 1] 2407         SUB     A,#'0'
      008A37 A1 0A            [ 1] 2408         CP      A,#10
      008A39 2B 09            [ 1] 2409         JRMI    DGTQ1
      008A3B A0 07            [ 1] 2410         SUB     A,#7
                           000001  2411         .ifne   CASEINSENSITIVE
      008A3D A4 DF            [ 1] 2412         AND     A,#0xDF
                                   2413         .endif
      008A3F A1 0A            [ 1] 2414         CP      A,#10
      008A41 2A 01            [ 1] 2415         JRPL    DGTQ1
      008A43 43               [ 1] 2416         CPL     A               ; make sure A > base
      008A44 E7 01            [ 1] 2417 DGTQ1:  LD      (1,X),A
      008A46 CD 84 6F         [ 4] 2418         CALL    DUPP
      008A49 CD 84 13         [ 4] 2419         CALL    RFROM
      008A4C CC 85 E6         [ 2] 2420         JP      ULESS
                                   2421 
                                   2422 
                                   2423 ; Basic I/O
                                   2424 
                                   2425 ;       KEY     ( -- c )
                                   2426 ;       Wait for and return an
                                   2427 ;       input character.
                                   2428 
      0009C4                       2429         HEADER  KEY "KEY"
                           000001     1         .ifeq   UNLINK_KEY
      008A4F 89 84                    2         .dw     LINK
                           0009C6     3         LINK    = .
      008A51 03                       4         .db      (102$ - 101$)
      008A52                          5 101$:
      008A52 4B 45 59                 6         .ascii  "KEY"
      008A55                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008A55                       2430 KEY:
      008A55 92 CD 62         [ 6] 2431 KEY1:   CALL    [USRQKEY]
      008A58 CD 83 3C         [ 4] 2432         CALL    QBRAN
      008A5B 8A 55                 2433         .dw     KEY1
      008A5D 81               [ 4] 2434         RET
                                   2435 
                           000000  2436         .ifeq   REMOVE_NUFQ
                                   2437 ;       NUF?    ( -- t )
                                   2438 ;       Return false if no input,
                                   2439 ;       else pause and if CR return true.
                                   2440 
                                   2441         HEADER  NUFQ "NUF?"
                                   2442 NUFQ:
                                   2443         .ifne   HALF_DUPLEX
                                   2444         ; slow EMIT down to free the line for RX
                                   2445         .ifne   HAS_BACKGROUND * HALF_DUPLEX
                                   2446         LD      A,TICKCNT+1
                                   2447         ADD     A,#3
                                   2448 1$:     CP      A,TICKCNT+1
                                   2449         JRNE    1$
                                   2450         .else
                                   2451         CLRW    Y
                                   2452 1$:     DECW    Y
                                   2453         JRNE    1$
                                   2454         .endif
                                   2455         .endif
                                   2456         CALL    [USRQKEY]
                                   2457         LD      A,(1,X)
                                   2458         JREQ    NUFQ1
                                   2459         CALL    DDROP
                                   2460         CALLR   KEY
                                   2461         DoLitC  CRR
                                   2462         JP      EQUAL
                                   2463 NUFQ1:  RET
                                   2464         .endif
                                   2465 
                                   2466 ;       SPACE   ( -- )
                                   2467 ;       Send    blank character to
                                   2468 ;       output device.
                                   2469 
      0009D3                       2470         HEADER  SPACE "SPACE"
                           000001     1         .ifeq   UNLINK_SPACE
      008A5E 8A 51                    2         .dw     LINK
                           0009D5     3         LINK    = .
      008A60 05                       4         .db      (102$ - 101$)
      008A61                          5 101$:
      008A61 53 50 41 43 45           6         .ascii  "SPACE"
      008A66                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008A66                       2471 SPACE:
                                   2472 
      008A66 CD 85 62         [ 4] 2473         CALL    BLANK
      008A69 92 CC 60         [ 5] 2474         JP      [USREMIT]
                                   2475 
                           000001  2476         .ifeq   UNLINKCORE
                                   2477 ;       SPACES  ( +n -- )
                                   2478 ;       Send n spaces to output device.
                                   2479 
                           000001  2480         .ifeq   BAREBONES
      0009E1                       2481         HEADER  SPACS "SPACES"
                           000001     1         .ifeq   UNLINK_SPACS
      008A6C 8A 60                    2         .dw     LINK
                           0009E3     3         LINK    = .
      008A6E 06                       4         .db      (102$ - 101$)
      008A6F                          5 101$:
      008A6F 53 50 41 43 45 53        6         .ascii  "SPACES"
      008A75                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008A75                       2482 SPACS:
      008A75 CD 85 66         [ 4] 2483         CALL    ZERO
      008A78 CD 86 10         [ 4] 2484         CALL    MAX
                           000000  2485         .else
                                   2486 SPACS:
                                   2487         .endif
      008A7B CD 84 3A         [ 4] 2488         CALL    TOR
      008A7E 20 02            [ 2] 2489         JRA     CHAR2
      008A80 AD E4            [ 4] 2490 CHAR1:  CALLR   SPACE
      008A82 CD 83 26         [ 4] 2491 CHAR2:  CALL    DONXT
      008A85 8A 80                 2492         .dw     CHAR1
      008A87 81               [ 4] 2493         RET
                                   2494         .endif
                                   2495 
                                   2496 ;       do$     ( -- a )
                                   2497 ;       Return  address of a compiled
                                   2498 ;       string.
                                   2499 
                           000000  2500         .ifne   WORDS_LINKRUNTI
                                   2501         HEADFLG DOSTR "do$" COMPO
                                   2502         .endif
      008A88                       2503 DOSTR:
      008A88 CD 84 13         [ 4] 2504         CALL    RFROM
      008A8B CD 84 31         [ 4] 2505         CALL    RAT
      008A8E CD 84 13         [ 4] 2506         CALL    RFROM
      008A91 CD 88 49         [ 4] 2507         CALL    COUNT
      008A94 CD 84 B0         [ 4] 2508         CALL    PLUS
      008A97 CD 84 3A         [ 4] 2509         CALL    TOR
      008A9A CD 84 7C         [ 4] 2510         CALL    SWAPP
      008A9D CD 84 3A         [ 4] 2511         CALL    TOR
      008AA0 81               [ 4] 2512         RET
                                   2513 
                                   2514 ;       $"|     ( -- a )
                                   2515 ;       Run time routine compiled by $".
                                   2516 ;       Return address of a compiled string.
                                   2517 
      008AA1                       2518         HEADFLG STRQP '$"|' COMPO
                           000000     1         .ifeq   UNLINK_STRQP
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      ((102$ - 101$) + COMPO)
                                      5 101$:
                                      6         .ascii  '$"|'
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2519 
      000A16                       2520 STRQP:
      008AA1 AD E5            [ 4] 2521         CALLR   DOSTR
      008AA3 81               [ 4] 2522         RET
                                   2523 
                                   2524 ;       ."|     ( -- )
                                   2525 ;       Run time routine of ." .
                                   2526 ;       Output a compiled string.
                                   2527 
                           000000  2528         .ifne   WORDS_LINKRUNTI
                                   2529         HEADFLG DOTQP '."|' COMPO
                                   2530         .endif
      008AA4                       2531 DOTQP:
      008AA4 AD E2            [ 4] 2532         CALLR   DOSTR
      008AA6                       2533 COUNTTYPES:
      008AA6 CD 88 49         [ 4] 2534         CALL    COUNT
      008AA9 20 2C            [ 2] 2535         JRA     TYPES
                                   2536 
                           000001  2537         .ifeq   BAREBONES
                                   2538 ;       .R      ( n +n -- )
                                   2539 ;       Display an integer in a field
                                   2540 ;       of n columns, right justified.
                                   2541 
      000A20                       2542         HEADER  DOTR ".R"
                           000001     1         .ifeq   UNLINK_DOTR
      008AAB 8A 6E                    2         .dw     LINK
                           000A22     3         LINK    = .
      008AAD 02                       4         .db      (102$ - 101$)
      008AAE                          5 101$:
      008AAE 2E 52                    6         .ascii  ".R"
      008AB0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008AB0                       2543 DOTR:
      008AB0 CD 84 3A         [ 4] 2544         CALL    TOR
      008AB3 CD 89 64         [ 4] 2545         CALL    STR
      008AB6 20 0B            [ 2] 2546         JRA     RFROMTYPES
                                   2547         .endif
                                   2548 
                                   2549 ;       U.R     ( u +n -- )
                                   2550 ;       Display an unsigned integer
                                   2551 ;       in n column, right justified.
                                   2552 
                           000001  2553         .ifeq   BAREBONES
      000A2D                       2554         HEADER  UDOTR "U.R"
                           000001     1         .ifeq   UNLINK_UDOTR
      008AB8 8A AD                    2         .dw     LINK
                           000A2F     3         LINK    = .
      008ABA 03                       4         .db      (102$ - 101$)
      008ABB                          5 101$:
      008ABB 55 2E 52                 6         .ascii  "U.R"
      008ABE                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2555         .endif
      008ABE                       2556 UDOTR:
      008ABE CD 84 3A         [ 4] 2557         CALL    TOR
      008AC1 AD 36            [ 4] 2558         CALLR   BDEDIGS
      008AC3                       2559 RFROMTYPES:
      008AC3 CD 84 13         [ 4] 2560         CALL    RFROM
      008AC6 CD 84 91         [ 4] 2561         CALL    OVER
      008AC9 CD 85 01         [ 4] 2562         CALL    SUBB
      008ACC AD A7            [ 4] 2563         CALLR   SPACS
      008ACE 20 07            [ 2] 2564         JRA     TYPES
                                   2565 
                                   2566 ;       TYPE    ( b u -- )
                                   2567 ;       Output u characters from b.
                                   2568 
      000A45                       2569         HEADER  TYPES "TYPE"
                           000001     1         .ifeq   UNLINK_TYPES
      008AD0 8A BA                    2         .dw     LINK
                           000A47     3         LINK    = .
      008AD2 04                       4         .db      (102$ - 101$)
      008AD3                          5 101$:
      008AD3 54 59 50 45              6         .ascii  "TYPE"
      008AD7                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008AD7                       2570 TYPES:
      008AD7 CD 84 3A         [ 4] 2571         CALL    TOR
      008ADA 20 09            [ 2] 2572         JRA     TYPE2
      008ADC CD 92 AC         [ 4] 2573 TYPE1:  CALL    DUPPCAT
      008ADF 92 CD 60         [ 6] 2574         CALL    [USREMIT]
      008AE2 CD 87 B3         [ 4] 2575         CALL    ONEP
      008AE5                       2576 TYPE2:
      008AE5 CD 83 26         [ 4] 2577         CALL    DONXT
      008AE8 8A DC                 2578         .dw     TYPE1
      008AEA CC 84 56         [ 2] 2579         JP      DROP
                                   2580 
                           000001  2581         .ifeq   BOOTSTRAP
                                   2582 ;       U.      ( u -- )
                                   2583 ;       Display an unsigned integer
                                   2584 ;       in free format.
                                   2585 
      000A62                       2586         HEADER  UDOT "U."
                           000001     1         .ifeq   UNLINK_UDOT
      008AED 8A D2                    2         .dw     LINK
                           000A64     3         LINK    = .
      008AEF 02                       4         .db      (102$ - 101$)
      008AF0                          5 101$:
      008AF0 55 2E                    6         .ascii  "U."
      008AF2                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008AF2                       2587 UDOT:
      008AF2 AD 05            [ 4] 2588         CALLR   BDEDIGS
      008AF4 CD 8A 66         [ 4] 2589         CALL    SPACE
      008AF7 20 DE            [ 2] 2590         JRA     TYPES
                                   2591 
                                   2592 ;       UDOT helper routine
      008AF9                       2593 BDEDIGS:
      008AF9 CD 89 5B         [ 4] 2594         CALL    BDIGS
      008AFC CD 89 2D         [ 4] 2595         CALL    DIGS
      008AFF CC 89 11         [ 2] 2596         JP      EDIGS
                                   2597         .endif
                                   2598 
                           000001  2599         .ifeq   BOOTSTRAP
                                   2600 ;       .       ( w -- )
                                   2601 ;       Display an integer in free
                                   2602 ;       format, preceeded by a space.
                                   2603 
      000A77                       2604         HEADER  DOT "."
                           000001     1         .ifeq   UNLINK_DOT
      008B02 8A EF                    2         .dw     LINK
                           000A79     3         LINK    = .
      008B04 01                       4         .db      (102$ - 101$)
      008B05                          5 101$:
      008B05 2E                       6         .ascii  "."
      008B06                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008B06                       2605 DOT:
      008B06 B6 65            [ 1] 2606         LD      A,USRBASE+1
      008B08 A8 0A            [ 1] 2607         XOR     A,#10
      008B0A 27 02            [ 1] 2608         JREQ    1$
      008B0C 20 E4            [ 2] 2609         JRA     UDOT
      008B0E CD 89 64         [ 4] 2610 1$:     CALL    STR
      008B11 CD 8A 66         [ 4] 2611         CALL    SPACE
      008B14 20 C1            [ 2] 2612         JRA     TYPES
                                   2613         .endif
                                   2614 
                           000001  2615         .ifeq   BOOTSTRAP
                                   2616 ;       ?       ( a -- )
                                   2617 ;       Display contents in memory cell.
                                   2618 
      000A8B                       2619         HEADER  QUEST "?"
                           000001     1         .ifeq   UNLINK_QUEST
      008B16 8B 04                    2         .dw     LINK
                           000A8D     3         LINK    = .
      008B18 01                       4         .db      (102$ - 101$)
      008B19                          5 101$:
      008B19 3F                       6         .ascii  "?"
      008B1A                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008B1A                       2620 QUEST:
      008B1A CD 83 D9         [ 4] 2621         CALL    AT
      008B1D 20 E7            [ 2] 2622         JRA     DOT
                                   2623         .endif
                                   2624 
                                   2625 
                                   2626 ; Parsing
                                   2627 
                                   2628 ;       YFLAGS  ( n - )       ( TOS STM8: - Y,Z,N )
                                   2629 ;       Consume TOS to CPU Y and Flags
                                   2630 
      008B1F                       2631 YFLAGS:
      008B1F 90 93            [ 1] 2632         LDW     Y,X
      008B21 5C               [ 1] 2633         INCW    X
      008B22 5C               [ 1] 2634         INCW    X
      008B23 90 FE            [ 2] 2635         LDW     Y,(Y)
      008B25 81               [ 4] 2636         RET
                                   2637 
                                   2638 
                                   2639 ;       AFLAGS  ( c - )       ( TOS STM8: - A,Z,N )
                                   2640 ;       Consume TOS to CPU A and Flags
                                   2641 
      008B26                       2642 AFLAGS:
      008B26 5C               [ 1] 2643         INCW    X
      008B27 F6               [ 1] 2644         LD      A,(X)
      008B28 5C               [ 1] 2645         INCW    X
      008B29 4D               [ 1] 2646         TNZ     A
      008B2A 81               [ 4] 2647         RET
                                   2648 
                                   2649 ;       parse   ( b u c -- b u delta ; <string> )
                                   2650 ;       Scan string delimited by c.
                                   2651 ;       Return found string and its offset.
                                   2652 
      008B2B                       2653         HEADER  PARS "pars"
                           000000     1         .ifeq   UNLINK_PARS
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "pars"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000AA0                       2654 PARS:
      008B2B AD F9            [ 4] 2655         CALLR   AFLAGS          ; TEMP CSTOR
      008B2D 88               [ 1] 2656         PUSH    A
      008B2E CD 84 91         [ 4] 2657         CALL    OVER
      008B31 CD 84 3A         [ 4] 2658         CALL    TOR
      008B34 26 08            [ 1] 2659         JRNE    1$
      008B36 CD 84 91         [ 4] 2660         CALL    OVER
      008B39 CD 84 13         [ 4] 2661         CALL    RFROM
      008B3C 20 72            [ 2] 2662         JRA     PARSEND
      008B3E CD 87 AA         [ 4] 2663 1$:     CALL    ONEM
      008B41 7B 03            [ 1] 2664         ld      A,(3,SP)        ; TEMP CAT
      008B43 A1 20            [ 1] 2665         CP      A,#' '          ; BLANK EQUAL
      008B45 26 21            [ 1] 2666         JRNE    PARS3
      008B47 CD 84 3A         [ 4] 2667         CALL    TOR
      008B4A                       2668 PARS1:
      008B4A A6 20            [ 1] 2669         LD      A,#' '
      008B4C 90 93            [ 1] 2670         LDW     Y,X
      008B4E 90 FE            [ 2] 2671         LDW     Y,(Y)
      008B50 90 F1            [ 1] 2672         CP      A,(Y)
      008B52 2B 11            [ 1] 2673         JRMI    PARS2
      008B54 CD 87 B3         [ 4] 2674         CALL    ONEP
      008B57 CD 83 26         [ 4] 2675         CALL    DONXT
      008B5A 8B 4A                 2676         .dw     PARS1
      008B5C 5B 02            [ 2] 2677         ADDW    SP,#2           ; RFROM DROP
      008B5E CD 85 66         [ 4] 2678         CALL    ZERO
      008B61 84               [ 1] 2679         POP     A               ; discard TEMP
      008B62                       2680 DUPPARS:
      008B62 CC 84 6F         [ 2] 2681         JP      DUPP
      008B65 CD 84 13         [ 4] 2682 PARS2:  CALL    RFROM
      008B68 CD 84 91         [ 4] 2683 PARS3:  CALL    OVER
      008B6B CD 84 7C         [ 4] 2684         CALL    SWAPP
      008B6E CD 84 3A         [ 4] 2685         CALL    TOR
      008B71                       2686 PARS4:
      008B71 7B 05            [ 1] 2687         LD      A,(5,SP)        ; TEMP CAT
      008B73 CD 85 43         [ 4] 2688         CALL    ASTOR
      008B76 CD 84 91         [ 4] 2689         CALL    OVER
      008B79 CD 83 F5         [ 4] 2690         CALL    CAT
      008B7C AD 33            [ 4] 2691         CALLR   SUBPARS         ; scan for delimiter
      008B7E 7B 05            [ 1] 2692         LD      A,(5,SP)        ; TEMP CAT
      008B80 A1 20            [ 1] 2693         CP      A,#' '          ; BLANK EQUAL
      008B82 26 03            [ 1] 2694         JRNE    PARS5
      008B84 CD 84 F4         [ 4] 2695         CALL    ZLESS
      008B87 CD 83 3C         [ 4] 2696 PARS5:  CALL    QBRAN
      008B8A 8B 9B                 2697         .dw     PARS6
      008B8C CD 87 B3         [ 4] 2698         CALL    ONEP
      008B8F CD 83 26         [ 4] 2699         CALL    DONXT
      008B92 8B 71                 2700         .dw     PARS4
      008B94 AD CC            [ 4] 2701         CALLR   DUPPARS
      008B96 CD 84 3A         [ 4] 2702         CALL    TOR
      008B99 20 0A            [ 2] 2703         JRA     PARS7
      008B9B 5B 02            [ 2] 2704 PARS6:  ADDW    SP,#2           ; RFROM DROP
      008B9D AD C3            [ 4] 2705         CALLR   DUPPARS
      008B9F CD 87 B3         [ 4] 2706         CALL    ONEP
      008BA2 CD 84 3A         [ 4] 2707         CALL    TOR
      008BA5 CD 84 91         [ 4] 2708 PARS7:  CALL    OVER
      008BA8 AD 07            [ 4] 2709         CALLR   SUBPARS
      008BAA CD 84 13         [ 4] 2710         CALL    RFROM
      008BAD CD 84 13         [ 4] 2711         CALL    RFROM
      008BB0                       2712 PARSEND:
      008BB0 84               [ 1] 2713         POP     A               ; discard TEMP
      008BB1                       2714 SUBPARS:
      008BB1 CC 85 01         [ 2] 2715         JP      SUBB
                                   2716 
                                   2717 ;       PARSE   ( c -- b u ; <string> )
                                   2718 ;       Scan input stream and return
                                   2719 ;       counted string delimited by c.
                                   2720 
      008BB4                       2721         HEADER  PARSE "PARSE"
                           000000     1         .ifeq   UNLINK_PARSE
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "PARSE"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000B29                       2722 PARSE:
      000B29                       2723         DoLitW  TIBB
      008BB4 83               [ 9]    1         TRAP
      008BB5 03 50                    2         .dw     TIBB
      008BB7 72 B9 00 7C      [ 2] 2724         ADDW    Y,USR_IN        ; current input buffer pointer
      008BBB FF               [ 2] 2725         LDW     (X),Y
      008BBC B6 7B            [ 1] 2726         LD      A,USRNTIB+1
      008BBE B0 7D            [ 1] 2727         SUB     A,USR_IN+1      ; remaining count
      008BC0 CD 85 43         [ 4] 2728         CALL    ASTOR
      008BC3 CD 85 9F         [ 4] 2729         CALL    ROT
      008BC6 CD 8B 2B         [ 4] 2730         CALL    PARS
      000B3E                       2731         DoLitC  USR_IN
      008BC9 83               [ 9]    1         TRAP
      008BCA 00 7C                    2         .dw     USR_IN
      008BCC CC 88 2A         [ 2] 2732         JP      PSTOR
                                   2733 
                           000001  2734         .ifeq   BOOTSTRAP
                                   2735 ;       .(      ( -- )
                                   2736 ;       Output following string up to next ) .
                                   2737 
      000B44                       2738         HEADFLG DOTPR ".(" IMEDD
                           000001     1         .ifeq   UNLINK_DOTPR
      008BCF 8B 18                    2         .dw     LINK
                           000B46     3         LINK    = .
      008BD1 82                       4         .db      ((102$ - 101$) + IMEDD)
      008BD2                          5 101$:
      008BD2 2E 28                    6         .ascii  ".("
      008BD4                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008BD4                       2739 DOTPR:
      000B49                       2740         DoLitC  41      ; ")"
      008BD4 83               [ 9]    1         TRAP
      008BD5 00 29                    2         .dw     41
      008BD7 AD DB            [ 4] 2741         CALLR   PARSE
      008BD9 CC 8A D7         [ 2] 2742         JP      TYPES
                                   2743         .endif
                                   2744 
                           000001  2745         .ifeq   BOOTSTRAP
                                   2746 ;       (       ( -- )
                                   2747 ;       Ignore following string up to next ).
                                   2748 ;       A comment.
                                   2749 
      000B51                       2750         HEADFLG PAREN "(" IMEDD
                           000001     1         .ifeq   UNLINK_PAREN
      008BDC 8B D1                    2         .dw     LINK
                           000B53     3         LINK    = .
      008BDE 81                       4         .db      ((102$ - 101$) + IMEDD)
      008BDF                          5 101$:
      008BDF 28                       6         .ascii  "("
      008BE0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008BE0                       2751 PAREN:
      000B55                       2752         DoLitC  41      ; ")"
      008BE0 83               [ 9]    1         TRAP
      008BE1 00 29                    2         .dw     41
      008BE3 AD CF            [ 4] 2753         CALLR   PARSE
      008BE5 CC 84 65         [ 2] 2754         JP      DDROP
                                   2755         .endif
                                   2756 
                                   2757 ;       \       ( -- )
                                   2758 ;       Ignore following text till
                                   2759 ;       end of line.
                                   2760 
      000B5D                       2761         HEADFLG BKSLA "\" IMEDD
                           000001     1         .ifeq   UNLINK_BKSLA
      008BE8 8B DE                    2         .dw     LINK
                           000B5F     3         LINK    = .
      008BEA 81                       4         .db      ((102$ - 101$) + IMEDD)
      008BEB                          5 101$:
      008BEB 5C                       6         .ascii  "\"
      008BEC                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008BEC                       2762 BKSLA:
      008BEC 45 7B 7D         [ 1] 2763         MOV       USR_IN+1,USRNTIB+1
      008BEF 81               [ 4] 2764         RET
                                   2765 
                                   2766 ;       TOKEN   ( -- a ; <string> )
                                   2767 ;       Parse a word from input stream
                                   2768 ;       and copy it to code dictionary or to RAM.
                                   2769 
      008BF0                       2770         HEADER  TOKEN "TOKEN"
                           000000     1         .ifeq   UNLINK_TOKEN
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "TOKEN"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2771 
      000B65                       2772 TOKEN:
      008BF0 CD 85 62         [ 4] 2773         CALL    BLANK
      008BF3 20 00            [ 2] 2774         JRA     WORDD
                                   2775 
                                   2776 ;       WORD    ( c -- a ; <string> )
                                   2777 ;       Parse a word from input stream
                                   2778 ;       and copy it to code dictionary or to RAM.
                                   2779 
      008BF5                       2780         HEADER  WORDD "WORD"
                           000000     1         .ifeq   UNLINK_WORDD
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "WORD"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000B6A                       2781 WORDD:
      008BF5 AD BD            [ 4] 2782         CALLR   PARSE
      008BF7 CD 88 55         [ 4] 2783         CALL    RAMHERE
      008BFA                       2784 CPPACKS:
      008BFA CD 87 A0         [ 4] 2785         CALL    CELLP
      008BFD CC 88 D9         [ 2] 2786         JP      PACKS
                                   2787 
                                   2788 ;       TOKEN_$,n  ( <word> - <dict header> )
                                   2789 ;       copy token to the code dictionary
                                   2790 ;       and build a new dictionary name
                                   2791 ;       note: for defining words (e.g. :, CREATE)
      008C00                       2792 TOKSNAME:
      008C00 CD 85 62         [ 4] 2793         CALL    BLANK
      008C03 AD AF            [ 4] 2794         CALLR   PARSE
      008C05 CD 88 67         [ 4] 2795         CALL    HERE
      008C08 AD F0            [ 4] 2796         CALLR   CPPACKS
      008C0A CC 90 51         [ 2] 2797         JP      SNAME
                                   2798 
                                   2799 ; Dictionary search
                                   2800 ;       NAME>   ( na -- ca )
                                   2801 ;       Return a code address given
                                   2802 ;       a name address.
                                   2803 
                           000001  2804         .ifeq   BAREBONES
      000B82                       2805         HEADER  NAMET "NAME>"
                           000001     1         .ifeq   UNLINK_NAMET
      008C0D 8B EA                    2         .dw     LINK
                           000B84     3         LINK    = .
      008C0F 05                       4         .db      (102$ - 101$)
      008C10                          5 101$:
      008C10 4E 41 4D 45 3E           6         .ascii  "NAME>"
      008C15                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   2806         .endif
      008C15                       2807 NAMET:
      008C15 CD 88 49         [ 4] 2808         CALL    COUNT
      000B8D                       2809         DoLitC  31
      008C18 83               [ 9]    1         TRAP
      008C19 00 1F                    2         .dw     31
      008C1B CD 84 D4         [ 4] 2810         CALL    ANDD
                           000000  2811         .ifeq   HAS_ALIAS
                                   2812         JP      PLUS
                           000001  2813         .else
      008C1E CD 84 B0         [ 4] 2814         CALL    PLUS            ; ALIAS: return the target address of a JP
      008C21 90 F6            [ 1] 2815         LD      A,(Y)           ; DUP C@
      008C23 A1 CC            [ 1] 2816         CP      A,#BRAN_OPC     ; BRAN_OPC =
      008C25 26 05            [ 1] 2817         JRNE    1$              ; IF
      008C27 90 5C            [ 1] 2818         INCW    Y               ; 1+
      008C29 90 FE            [ 2] 2819         LDW     Y,(Y)           ; @
      008C2B FF               [ 2] 2820         LDW     (X),Y
      008C2C 81               [ 4] 2821 1$:     RET                     ; THEN
                                   2822         .endif
                                   2823 
                                   2824 
                                   2825 ;       R@ indexed char lookup for SAME?
      008C2D                       2826 SAMEQCAT:
      008C2D CD 84 91         [ 4] 2827         CALL    OVER
      008C30 72 F9 03         [ 2] 2828         ADDW    Y,(3,SP)        ; R-OVER> PLUS
                           000001  2829         .ifne   CASEINSENSITIVE
      008C33 CD 83 F9         [ 4] 2830         CALL    YCAT
      008C36 20 1F            [ 2] 2831         JRA   CUPPER
                           000000  2832         .else
                                   2833         JP      YCAT
                                   2834         .endif
                                   2835 
                                   2836 ;       SAME?   ( a a u -- a a f \ -0+ )
                                   2837 ;       Compare u cells in two
                                   2838 ;       strings. Return 0 if identical.
                                   2839 
      008C38                       2840         HEADER  SAMEQ "SAME?"
                           000000     1         .ifeq   UNLINK_SAMEQ
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "SAME?"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000BAD                       2841 SAMEQ:
      008C38 CD 87 AA         [ 4] 2842         CALL    ONEM
      008C3B CD 84 3A         [ 4] 2843         CALL    TOR
      008C3E 20 0F            [ 2] 2844         JRA     SAME2
      008C40                       2845 SAME1:
      008C40 AD EB            [ 4] 2846         CALLR   SAMEQCAT
      008C42 AD E9            [ 4] 2847         CALLR   SAMEQCAT
      008C44 CD 84 C3         [ 4] 2848         CALL    XORR
      008C47 CD 83 37         [ 4] 2849         CALL    QDQBRAN
      008C4A 8C 4F                 2850         .dw     SAME2
      008C4C 90 85            [ 2] 2851         POPW    Y               ; RFROM DROP
      008C4E 81               [ 4] 2852         RET
      008C4F CD 83 26         [ 4] 2853 SAME2:  CALL    DONXT
      008C52 8C 40                 2854         .dw     SAME1
      008C54 CC 85 66         [ 2] 2855         JP      ZERO
                                   2856 
                           000001  2857         .ifne   CASEINSENSITIVE
                                   2858 ;       CUPPER  ( c -- c )
                                   2859 ;       convert char to upper case
                                   2860 
      008C57                       2861         HEADER  CUPPER "CUPPER"
                           000000     1         .ifeq   UNLINK_CUPPER
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "CUPPER"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000BCC                       2862 CUPPER:
      008C57 E6 01            [ 1] 2863         LD      A,(1,X)
      008C59 A1 61            [ 1] 2864         CP      A,#('a')
      008C5B 25 08            [ 1] 2865         JRULT   1$
      008C5D A1 7A            [ 1] 2866         CP      A,#('z')
      008C5F 22 04            [ 1] 2867         JRUGT   1$
      008C61 A4 DF            [ 1] 2868         AND     A,#0xDF
      008C63 E7 01            [ 1] 2869         LD      (1,X),A
      008C65 81               [ 4] 2870 1$:     RET
                                   2871         .endif
                                   2872 
                                   2873 ;       NAME?   ( a -- ca na | a F )
                                   2874 ;       Search vocabularies for a string.
                                   2875 
      008C66                       2876         HEADER  NAMEQ "NAME?"
                           000000     1         .ifeq   UNLINK_NAMEQ
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "NAME?"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000BDB                       2877 NAMEQ:
                           000001  2878         .ifne   HAS_ALIAS
      008C66 CD 85 0F         [ 4] 2879         CALL    CNTXT_ALIAS
                           000000  2880         .else
                                   2881         CALL    CNTXT
                                   2882         .endif
                                   2883 
      008C69 20 00            [ 2] 2884         JRA     FIND
                                   2885 
                                   2886 ;       find    ( a va -- ca na | a F )
                                   2887 ;       Search vocabulary for string.
                                   2888 ;       Return ca and na if succeeded.
                                   2889 
      008C6B                       2890         HEADER  FIND "find"
                           000000     1         .ifeq   UNLINK_FIND
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "find"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000BE0                       2891 FIND:
      008C6B AD 65            [ 4] 2892         CALLR   SWAPPF          ; SWAPP
      008C6D 90 FE            [ 2] 2893         LDW     Y,(Y)           ; DUPP CAT TEMP CSTOR DUPP AT
      008C6F 90 9E            [ 1] 2894         LD      A,YH
      008C71 88               [ 1] 2895         PUSH    A               ; (push TEMP)
      008C72 90 89            [ 2] 2896         PUSHW   Y               ; TOR
      008C74 CD 87 A0         [ 4] 2897         CALL    CELLP
      008C77 AD 59            [ 4] 2898         CALLR   SWAPPF
      008C79 CD 83 D9         [ 4] 2899 FIND1:  CALL    AT
      008C7C 27 30            [ 1] 2900         JREQ    FIND6
      008C7E CD 84 6F         [ 4] 2901         CALL    DUPP
      008C81 CD 83 D9         [ 4] 2902         CALL    AT
      000BF9                       2903         DoLitW  MASKK
      008C84 83               [ 9]    1         TRAP
      008C85 1F 7F                    2         .dw     MASKK
      008C87 CD 84 D4         [ 4] 2904         CALL    ANDD
                           000001  2905         .ifne   CASEINSENSITIVE
      008C8A AD CB            [ 4] 2906         CALLR   CUPPER
                                   2907         .endif
      008C8C CD 84 31         [ 4] 2908         CALL    RAT
                           000001  2909         .ifne   CASEINSENSITIVE
      008C8F AD C6            [ 4] 2910         CALLR   CUPPER
                                   2911         .endif
      008C91 CD 84 C3         [ 4] 2912         CALL    XORR
      008C94 CD 83 3C         [ 4] 2913         CALL    QBRAN
      008C97 8C A1                 2914         .dw     FIND2
      008C99 CD 87 A0         [ 4] 2915         CALL    CELLP
      008C9C CD 85 6D         [ 4] 2916         CALL    MONE            ; 0xFFFF
      008C9F 20 0B            [ 2] 2917         JRA     FIND3
      008CA1 CD 87 A0         [ 4] 2918 FIND2:  CALL    CELLP
      008CA4 7B 03            [ 1] 2919         LD      A,(3,SP)        ; TEMP CAT
      008CA6 CD 85 43         [ 4] 2920         CALL    ASTOR
      008CA9 CD 8C 38         [ 4] 2921         CALL    SAMEQ
      008CAC 20 09            [ 2] 2922 FIND3:  JRA     FIND4
      008CAE 5B 03            [ 2] 2923 FIND6:  ADDW    SP,#3           ; (pop TEMP) RFROM DROP
      008CB0 AD 20            [ 4] 2924         CALLR   SWAPPF
      008CB2 CD 87 96         [ 4] 2925         CALL    CELLM
      008CB5 20 1B            [ 2] 2926         JRA     SWAPPF
      008CB7 CD 83 3C         [ 4] 2927 FIND4:  CALL    QBRAN
      008CBA 8C C4                 2928         .dw     FIND5
      008CBC CD 87 96         [ 4] 2929         CALL    CELLM
      008CBF CD 87 96         [ 4] 2930         CALL    CELLM
      008CC2 20 B5            [ 2] 2931         JRA     FIND1
      008CC4 5B 03            [ 2] 2932 FIND5:  ADDW    SP,#3           ; (pop TEMP) RFROM DROP
      008CC6 CD 84 4B         [ 4] 2933         CALL    NIP
      008CC9 CD 87 96         [ 4] 2934         CALL    CELLM
      008CCC CD 84 6F         [ 4] 2935         CALL    DUPP
      008CCF CD 8C 15         [ 4] 2936         CALL    NAMET
      008CD2                       2937 SWAPPF:
      008CD2 CC 84 7C         [ 2] 2938         JP      SWAPP
                                   2939 
                                   2940 ; Terminal response
                                   2941 
                                   2942 ;       ^H      ( bot eot cur -- bot eot cur )
                                   2943 ;       Backup cursor by one character.
                                   2944 
      008CD5                       2945         HEADER  BKSP "^h"
                           000000     1         .ifeq   UNLINK_BKSP
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "^h"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000C4A                       2946 BKSP:
      008CD5 E6 04            [ 1] 2947         LD      A,(4,X)         ; backspace if CUR != BOT
      008CD7 F1               [ 1] 2948         CP      A,(X)
      008CD8 26 06            [ 1] 2949         JRNE    BACK0
      008CDA E6 05            [ 1] 2950         LD      A,(5,X)
      008CDC E1 01            [ 1] 2951         CP      A,(1,X)
      008CDE 27 0E            [ 1] 2952         JREQ    BACK1
      008CE0                       2953 BACK0:
                           000001  2954         .ifeq   HALF_DUPLEX
      008CE0 AD 06            [ 4] 2955         CALLR   BACKSP
                                   2956         .endif
      008CE2 CD 87 AA         [ 4] 2957         CALL    ONEM
      008CE5 CD 8A 66         [ 4] 2958         CALL    SPACE
      008CE8                       2959 BACKSP:
      000C5D                       2960         DoLitC  BKSPP
      008CE8 83               [ 9]    1         TRAP
      008CE9 00 08                    2         .dw     BKSPP
      008CEB 92 CC 60         [ 5] 2961         JP      [USREMIT]
      008CEE 81               [ 4] 2962 BACK1:  RET
                                   2963 
                                   2964 ;       TAP     ( bot eot cur c -- bot eot cur )
                                   2965 ;       Accept and echo key stroke
                                   2966 ;       and bump cursor.
                                   2967 
      008CEF                       2968         HEADER  TAP "TAP"
                           000000     1         .ifeq   UNLINK_TAP
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "TAP"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000C64                       2969 TAP:
                           000001  2970         .ifeq   HALF_DUPLEX
      008CEF CD 84 6F         [ 4] 2971         CALL    DUPP
      008CF2 92 CD 60         [ 6] 2972         CALL    [USREMIT]
                                   2973         .endif
      008CF5 CD 84 91         [ 4] 2974         CALL    OVER
      008CF8 CD 84 04         [ 4] 2975         CALL    CSTOR
      008CFB CC 87 B3         [ 2] 2976         JP      ONEP
                                   2977 
                                   2978 ;       kTAP    ( bot eot cur c -- bot eot cur )
                                   2979 ;       Process a key stroke,
                                   2980 ;       CR or backspace.
                                   2981 
      008CFE                       2982         HEADER  KTAP "kTAP"
                           000000     1         .ifeq   UNLINK_KTAP
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "kTAP"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000C73                       2983 KTAP:
      008CFE E6 01            [ 1] 2984         LD      A,(1,X)
      008D00 A1 0D            [ 1] 2985         CP      A,#CRR
      008D02 27 12            [ 1] 2986         JREQ    KTAP2
                                   2987 
      000C79                       2988         DoLitC  BKSPP
      008D04 83               [ 9]    1         TRAP
      008D05 00 08                    2         .dw     BKSPP
      008D07 CD 84 C3         [ 4] 2989         CALL    XORR
      008D0A CD 83 3C         [ 4] 2990         CALL    QBRAN
      008D0D 8D 14                 2991         .dw     KTAP1
                                   2992 
      008D0F CD 85 62         [ 4] 2993         CALL    BLANK
      008D12 20 DB            [ 2] 2994         JRA     TAP
      008D14 20 BF            [ 2] 2995 KTAP1:  JRA     BKSP
      008D16 CD 84 56         [ 4] 2996 KTAP2:  CALL    DROP
      008D19 CD 84 4B         [ 4] 2997         CALL    NIP
      008D1C CC 84 6F         [ 2] 2998         JP      DUPP
                                   2999 
                                   3000 ;       ACCEPT  ( b u -- b u )
                                   3001 ;       Accept one line of characters to input
                                   3002 ;       buffer. Return with actual count.
                                   3003 
      008D1F                       3004         HEADER  ACCEP "ACCEPT"
                           000000     1         .ifeq   UNLINK_ACCEP
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "ACCEPT"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000C94                       3005 ACCEP:
      008D1F CD 84 91         [ 4] 3006         CALL    OVER
      008D22 CD 84 B0         [ 4] 3007         CALL    PLUS
      008D25 CD 84 91         [ 4] 3008         CALL    OVER
      008D28 CD 85 B1         [ 4] 3009 ACCP1:  CALL    DDUP
      008D2B CD 84 C3         [ 4] 3010         CALL    XORR
      008D2E CD 83 3C         [ 4] 3011         CALL    QBRAN
      008D31 8D 46                 3012         .dw     ACCP4
      008D33 CD 8A 55         [ 4] 3013         CALL    KEY
      008D36 E6 01            [ 1] 3014         LD      A,(1,X)         ; DUPP
      008D38 2B 08            [ 1] 3015         JRMI    ACCP2           ; BL 127 WITHIN
      008D3A A1 20            [ 1] 3016         CP      A,#32
      008D3C 2B 04            [ 1] 3017         JRMI    ACCP2           ; ?branch ACC2
      008D3E AD AF            [ 4] 3018         CALLR   TAP
      008D40 20 02            [ 2] 3019         JRA     ACCP3
      008D42 AD BA            [ 4] 3020 ACCP2:  CALLR   KTAP
      008D44 20 E2            [ 2] 3021 ACCP3:  JRA     ACCP1
      008D46 CD 84 56         [ 4] 3022 ACCP4:  CALL    DROP
      008D49 CD 84 91         [ 4] 3023         CALL    OVER
      008D4C CC 85 01         [ 2] 3024         JP      SUBB
                                   3025 
                                   3026 ;       QUERY   ( -- )
                                   3027 ;       Accept one line from input stream to
                                   3028 ;       terminal input buffer.
                                   3029 
      008D4F                       3030         HEADER  QUERY "QUERY"
                           000000     1         .ifeq   UNLINK_QUERY
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "QUERY"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000CC4                       3031 QUERY:
      000CC4                       3032         DoLitW  TIBB
      008D4F 83               [ 9]    1         TRAP
      008D50 03 50                    2         .dw     TIBB
      000CC7                       3033         DoLitC  TIBLENGTH
      008D52 83               [ 9]    1         TRAP
      008D53 00 50                    2         .dw     TIBLENGTH
      008D55 AD C8            [ 4] 3034         CALLR   ACCEP
      008D57 CD 8B 26         [ 4] 3035         CALL    AFLAGS          ; NTIB !
      008D5A B7 7B            [ 1] 3036         LD      USRNTIB+1,A
      008D5C 3F 7C            [ 1] 3037         CLR     USR_IN
      008D5E 3F 7D            [ 1] 3038         CLR     USR_IN+1
      008D60 CC 84 56         [ 2] 3039         JP      DROP
                                   3040 
                                   3041 ;       ABORT   ( -- )
                                   3042 ;       Reset data stack and
                                   3043 ;       jump to QUIT.
                                   3044 
      008D63                       3045         HEADER  ABORT "ABORT"
                           000000     1         .ifeq   UNLINK_ABORT
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "ABORT"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000CD8                       3046 ABORT:
      008D63 AD 20            [ 4] 3047         CALLR   PRESE
      008D65 CC 8E 1E         [ 2] 3048         JP      QUIT
                                   3049 
                                   3050 ;       abort"  ( f -- )
                                   3051 ;       Run time routine of ABORT".
                                   3052 ;       Abort with a message.
                                   3053 
      008D68                       3054         HEADFLG ABORQ "aborq" COMPO
                           000000     1         .ifeq   UNLINK_ABORQ
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      ((102$ - 101$) + COMPO)
                                      5 101$:
                                      6         .ascii  "aborq"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000CDD                       3055 ABORQ:
      008D68 CD 83 3C         [ 4] 3056         CALL    QBRAN
      008D6B 8D 7F                 3057         .dw     ABOR2           ; text flag
      008D6D CD 8A 88         [ 4] 3058         CALL    DOSTR
      008D70 CD 8A 66         [ 4] 3059 ABOR1:  CALL    SPACE
      008D73 CD 8A A6         [ 4] 3060         CALL    COUNTTYPES
      008D76 CD 8A A4         [ 4] 3061         CALL    DOTQP
                           000000  3062         .ifne   HAS_OLDOK
                                   3063         .db     2, 63,  10       ; ?<CR>
                           000001  3064         .else
      008D79 03 3F 07 0A           3065         .db     3, 63,  7, 10   ; ?<BEL><CR>
                                   3066         .endif
      008D7D 20 E4            [ 2] 3067         JRA     ABORT           ; pass error string
      008D7F CD 8A 88         [ 4] 3068 ABOR2:  CALL    DOSTR
      008D82 CC 84 56         [ 2] 3069         JP      DROP
                                   3070 
                                   3071 ;       PRESET  ( -- )
                                   3072 ;       Reset data stack pointer and
                                   3073 ;       terminal input buffer.
                                   3074 
      008D85                       3075         HEADER  PRESE "PRESET"
                           000000     1         .ifeq   UNLINK_PRESE
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "PRESET"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000CFA                       3076 PRESE:
      008D85 5F               [ 1] 3077         CLRW    X
      008D86 BF 7A            [ 2] 3078         LDW     USRNTIB,X
      008D88 AE 03 20         [ 2] 3079         LDW     X,#SPP          ; initialize data stack
      008D8B 81               [ 4] 3080         RET
                                   3081 
                                   3082 ; The text interpreter
                                   3083 
                                   3084 ;       $INTERPRET      ( a -- )
                                   3085 ;       Interpret a word. If failed,
                                   3086 ;       try to convert it to an integer.
                                   3087 
      008D8C                       3088         HEADER  INTER "$INTERPRET"
                           000000     1         .ifeq   UNLINK_INTER
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "$INTERPRET"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000D01                       3089 INTER:
      008D8C CD 8C 66         [ 4] 3090         CALL    NAMEQ
      008D8F CD 83 37         [ 4] 3091         CALL    QDQBRAN         ; ?defined
      008D92 8D B0                 3092         .dw     INTE1
      008D94 CD 83 D9         [ 4] 3093         CALL    AT
      000D0C                       3094         DoLitW  0x04000         ; COMPO*256
      008D97 83               [ 9]    1         TRAP
      008D98 40 00                    2         .dw     0x04000
      008D9A CD 84 D4         [ 4] 3095         CALL    ANDD            ; ?compile only lexicon bits
      008D9D AD C9            [ 4] 3096         CALLR   ABORQ
      008D9F 0D                    3097         .db     13
      008DA0 20 63 6F 6D 70 69 6C  3098         .ascii  " compile only"
             65 20 6F 6E 6C 79
      008DAD CC 83 59         [ 2] 3099         JP      EXECU
      008DB0 CD 89 98         [ 4] 3100 INTE1:  CALL    NUMBQ           ; convert a number
      008DB3 CD 83 3C         [ 4] 3101         CALL    QBRAN
      008DB6 8D 70                 3102         .dw     ABOR1
      008DB8 81               [ 4] 3103         RET
                                   3104 
                                   3105 ;       [       ( -- )
                                   3106 ;       Start   text interpreter.
      000D2E                       3107         HEADFLG LBRAC "[" IMEDD
                           000001     1         .ifeq   UNLINK_LBRAC
      008DB9 8C 0F                    2         .dw     LINK
                           000D30     3         LINK    = .
      008DBB 81                       4         .db      ((102$ - 101$) + IMEDD)
      008DBC                          5 101$:
      008DBC 5B                       6         .ascii  "["
      008DBD                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008DBD                       3108 LBRAC:
      008DBD 90 AE 8D 8C      [ 2] 3109         LDW     Y,#INTER
      008DC1 90 BF 66         [ 2] 3110         LDW     USREVAL,Y
      008DC4 81               [ 4] 3111         RET
                                   3112 
                                   3113 ;       CR      ( -- )
                                   3114 ;       Output a carriage return
                                   3115 ;       and a line feed.
                                   3116 
      000D3A                       3117         HEADER  CR "CR"
                           000001     1         .ifeq   UNLINK_CR
      008DC5 8D BB                    2         .dw     LINK
                           000D3C     3         LINK    = .
      008DC7 02                       4         .db      (102$ - 101$)
      008DC8                          5 101$:
      008DC8 43 52                    6         .ascii  "CR"
      008DCA                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008DCA                       3118 CR:
                           000000  3119         .ifeq TERM_LINUX
                                   3120         DoLitC  CRR
                                   3121         CALL    [USREMIT]
                                   3122         .endif
      000D3F                       3123         DoLitC  LF
      008DCA 83               [ 9]    1         TRAP
      008DCB 00 0A                    2         .dw     LF
      008DCD 92 CC 60         [ 5] 3124         JP      [USREMIT]
                                   3125 
                                   3126 ;       COMPILE?   ( -- n )
                                   3127 ;       0 if 'EVAL points to $INTERPRETER
                                   3128 ;       HEADER  COMPIQ "COMPILE?"
      008DD0                       3129 COMPIQ:
      008DD0 90 BE 66         [ 2] 3130         LDW     Y,USREVAL
      008DD3 72 A2 8D 8C      [ 2] 3131         SUBW    Y,#INTER
      008DD7 81               [ 4] 3132         RET
                                   3133 
                                   3134 ;       .OK     ( -- )
                                   3135 ;       Display 'ok' while interpreting.
                                   3136 
      008DD8                       3137         HEADER  DOTOK ".OK"
                           000000     1         .ifeq   UNLINK_DOTOK
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  ".OK"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000D4D                       3138 DOTOK:
      008DD8 AD F6            [ 4] 3139         CALLR   COMPIQ
      008DDA 27 24            [ 1] 3140         JREQ    DOTO1
                           000000  3141         .ifne   HAS_OLDOK
                                   3142         JRA     CR
                           000001  3143         .else
      008DDC CD 8A A4         [ 4] 3144         CALL    DOTQP            ; e4thcom handshake (which also works with " ok")
      008DDF 04                    3145         .db     4
      008DE0 20 4F 4B              3146         .ascii  " OK"
      008DE3 0A                    3147         .db     10
      008DE4 81               [ 4] 3148         RET
                                   3149         .endif
                                   3150 
                           000001  3151         .ifeq   BAREBONES
                                   3152 ;       hi      ( -- )
                                   3153 ;       Display sign-on message.
                                   3154 
      000D5A                       3155         HEADER  HI "hi"
                           000001     1         .ifeq   UNLINK_HI
      008DE5 8D C7                    2         .dw     LINK
                           000D5C     3         LINK    = .
      008DE7 02                       4         .db      (102$ - 101$)
      008DE8                          5 101$:
      008DE8 68 69                    6         .ascii  "hi"
      008DEA                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008DEA                       3156 HI:
      008DEA CD 8A A4         [ 4] 3157         CALL    DOTQP           ; initialize I/O
      008DED 12 0A                 3158         .db     18, 10
      008DEF 53 54 4D 38 65 46 6F  3159         .ascii  "STM8eForth 2.2."
             72 74 68 20 32 2E 32
             2E
      008DFE 32                    3160         .db     (RELVER1+'0')
      008DFF 30                    3161         .db     (RELVER0+'0')   ; version
                                   3162 
                                   3163          ; fall through
                           000000  3164         .else
                                   3165 HI:
                                   3166         .endif
      008E00                       3167 DOTO1:
      008E00 CD 8A A4         [ 4] 3168         CALL    DOTQP
      008E03 04                    3169         .db     4
      008E04 20 6F 6B              3170         .ascii  " ok"
      008E07 0A                    3171         .db     10
      008E08 81               [ 4] 3172         RET
                                   3173 
                                   3174 
                                   3175 ;       ?STACK  ( -- )
                                   3176 ;       Abort if stack underflows.
                                   3177 
      008E09                       3178         HEADER  QSTAC "?STACK"
                           000000     1         .ifeq   UNLINK_QSTAC
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "?STACK"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000D7E                       3179 QSTAC:
      008E09 CD 88 1A         [ 4] 3180         CALL    DEPTH
      008E0C CD 84 F4         [ 4] 3181         CALL    ZLESS           ; check only for underflow
      008E0F CD 8D 68         [ 4] 3182         CALL    ABORQ
      008E12 0A                    3183         .db     10
      008E13 20 75 6E 64 65 72 66  3184         .ascii  " underflow"
             6C 6F 77
      008E1D 81               [ 4] 3185         RET
                                   3186 
                                   3187 ;       QUIT    ( -- )
                                   3188 ;       Reset return stack pointer
                                   3189 ;       and start text interpreter.
                                   3190 
      008E1E                       3191         HEADER  QUIT "QUIT"
                           000000     1         .ifeq   UNLINK_QUIT
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "QUIT"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000D93                       3192 QUIT:
      008E1E 90 AE 03 FF      [ 2] 3193         LDW     Y,#RPP          ; initialize return stack
      008E22 90 94            [ 1] 3194         LDW     SP,Y
      008E24 AD 97            [ 4] 3195 QUIT1:  CALLR   LBRAC           ; start interpretation
      008E26 CD 8D 4F         [ 4] 3196 QUIT2:  CALL    QUERY           ; get input
      008E29 AD 02            [ 4] 3197         CALLR   EVAL
      008E2B 20 F9            [ 2] 3198         JRA     QUIT2           ; continue till error
                                   3199 
                                   3200 ;       EVAL    ( -- )
                                   3201 ;       Interpret input stream.
                                   3202 
      008E2D                       3203         HEADER  EVAL "EVAL"
                           000000     1         .ifeq   UNLINK_EVAL
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "EVAL"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000DA2                       3204 EVAL:
      008E2D CD 8B F0         [ 4] 3205 EVAL1:  CALL    TOKEN
      008E30 90 F6            [ 1] 3206         LD      A,(Y)
      008E32 27 07            [ 1] 3207         JREQ    EVAL2
      008E34 92 CD 66         [ 6] 3208         CALL    [USREVAL]
      008E37 AD D0            [ 4] 3209         CALLR   QSTAC           ; evaluate input, check stack
      008E39 20 F2            [ 2] 3210         JRA     EVAL1
      008E3B                       3211 EVAL2:
      008E3B 5C               [ 1] 3212         INCW    X
      008E3C 5C               [ 1] 3213         INCW    X
      008E3D 92 CC 68         [ 5] 3214         JP      [USRPROMPT]     ; DOTOK or PACE
                                   3215 
                                   3216 ; The compiler
                                   3217 
                                   3218 ;       '       ( -- ca )
                                   3219 ;       Search vocabularies for
                                   3220 ;       next word in input stream.
                                   3221 
      000DB5                       3222         HEADER  TICK "'"
                           000001     1         .ifeq   UNLINK_TICK
      008E40 8D E7                    2         .dw     LINK
                           000DB7     3         LINK    = .
      008E42 01                       4         .db      (102$ - 101$)
      008E43                          5 101$:
      008E43 27                       6         .ascii  "'"
      008E44                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008E44                       3223 TICK:
      008E44 CD 8B F0         [ 4] 3224         CALL    TOKEN
      008E47 CD 8C 66         [ 4] 3225         CALL    NAMEQ           ; ?defined
      008E4A CD 83 3C         [ 4] 3226         CALL    QBRAN
      008E4D 8D 70                 3227         .dw     ABOR1
      008E4F 81               [ 4] 3228         RET                     ; yes, push code address
                                   3229 
                                   3230 ;       ,       ( w -- )
                                   3231 ;       Compile an integer into
                                   3232 ;       code dictionary.
                                   3233 
                                   3234 ;       HEADER  COMMA ","
      008E50 8E 42                 3235         .dw     LINK
                                   3236 
                           000DC7  3237         LINK =  .
      008E52 01                    3238         .db     1
      008E53 2C                    3239         .ascii  ","
      008E54                       3240 COMMA:
      000DC9                       3241         DoLitC  2
      008E54 83               [ 9]    1         TRAP
      008E55 00 02                    2         .dw     2
      008E57 AD 10            [ 4] 3242         CALLR   OMMA
      008E59 CC 83 E4         [ 2] 3243         JP      STORE
                                   3244 
                                   3245 ;       C,      ( c -- )
                                   3246 ;       Compile a byte into code dictionary.
                                   3247 ;       HEADER  CCOMMA "C,"
      008E5C 8E 52                 3248         .dw     LINK
                                   3249 
                           000DD3  3250         LINK =  .
      008E5E 02                    3251         .db     2
      008E5F 43 2C                 3252         .ascii  "C,"
      008E61                       3253 CCOMMA:
      008E61 CD 85 69         [ 4] 3254         CALL    ONE
      008E64 AD 03            [ 4] 3255         CALLR   OMMA
      008E66 CC 84 04         [ 2] 3256         JP      CSTOR
                                   3257 
                                   3258 ;       common part of COMMA and CCOMMA
      008E69                       3259 OMMA:
      008E69 CD 88 67         [ 4] 3260         CALL    HERE
      008E6C CD 84 7C         [ 4] 3261         CALL    SWAPP
      008E6F CD 85 13         [ 4] 3262         CALL    CPP
      008E72 CC 88 2A         [ 2] 3263         JP      PSTOR
                                   3264 
                                   3265 ;       CALL,   ( ca -- )
                                   3266 ;       Compile a subroutine call.
                           000001  3267         .ifeq   BAREBONES
                           000001  3268         .ifeq   UNLINK_JSRC
                                   3269 ;       HEADER  JSRC "CALL,"
      008E75 8E 5E                 3270         .dw     LINK
                                   3271 
                           000DEC  3272         LINK =  .
      008E77 05                    3273         .db     5
      008E78 43 41 4C 4C 2C        3274         .ascii  "CALL,"
                                   3275         .endif
                                   3276         .endif
      008E7D                       3277 JSRC:
      008E7D CD 84 6F         [ 4] 3278         CALL    DUPP
      008E80 CD 88 67         [ 4] 3279         CALL    HERE
      008E83 CD 87 A0         [ 4] 3280         CALL    CELLP
      008E86 CD 85 01         [ 4] 3281         CALL    SUBB            ; Y now contains the relative call address
      008E89 90 9E            [ 1] 3282         LD      A,YH
      008E8B 4C               [ 1] 3283         INC     A
      008E8C 26 0D            [ 1] 3284         JRNE    1$              ; YH must be 0XFF
      008E8E 90 9F            [ 1] 3285         LD      A,YL
      008E90 4D               [ 1] 3286         TNZ     A
      008E91 2A 08            [ 1] 3287         JRPL    1$              ; YL must be negative
      008E93 A6 AD            [ 1] 3288         LD      A,#CALLR_OPC
      008E95 90 95            [ 1] 3289         LD      YH,A            ; replace YH with opcode CALLR
      008E97 EF 02            [ 2] 3290         LDW     (2,X),Y
      008E99 20 04            [ 2] 3291         JRA     2$
      008E9B                       3292 1$:
      008E9B CD 82 EA         [ 4] 3293         CALL    CCOMMALIT
      008E9E CD                    3294         .db     CALL_OPC         ; opcode CALL
      008E9F                       3295 2$:
      008E9F CD 84 56         [ 4] 3296         CALL    DROP             ; drop relative address
      008EA2 20 B0            [ 2] 3297         JRA     COMMA            ; store absolute address or "CALLR reladdr"
                                   3298 
                                   3299 ;       LITERAL ( w -- )
                                   3300 ;       Compile tos to dictionary
                                   3301 ;       as an integer literal.
                                   3302 
      000E19                       3303         HEADFLG LITER "LITERAL" IMEDD
                           000001     1         .ifeq   UNLINK_LITER
      008EA4 8E 77                    2         .dw     LINK
                           000E1B     3         LINK    = .
      008EA6 87                       4         .db      ((102$ - 101$) + IMEDD)
      008EA7                          5 101$:
      008EA7 4C 49 54 45 52 41 4C     6         .ascii  "LITERAL"
      008EAE                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008EAE                       3304 LITER:
                           000000  3305         .ifne  USE_CALLDOLIT
                                   3306         CALLR   COMPI
                                   3307         CALL    DOLIT
                           000001  3308         .else
      008EAE CD 82 EA         [ 4] 3309         CALL    CCOMMALIT
      008EB1 83                    3310         .db     DOLIT_OPC
                                   3311         .endif
      008EB2 20 A0            [ 2] 3312         JRA      COMMA
                                   3313 
                           000001  3314         .ifeq BOOTSTRAP
                                   3315 ;       [COMPILE]       ( -- ; <string> )
                                   3316 ;       Compile next immediate
                                   3317 ;       word into code dictionary.
                                   3318 
                           000001  3319         .ifeq   BAREBONES
      000E29                       3320         HEADFLG BCOMP "[COMPILE]" IMEDD
                           000001     1         .ifeq   UNLINK_BCOMP
      008EB4 8E A6                    2         .dw     LINK
                           000E2B     3         LINK    = .
      008EB6 89                       4         .db      ((102$ - 101$) + IMEDD)
      008EB7                          5 101$:
      008EB7 5B 43 4F 4D 50 49 4C     6         .ascii  "[COMPILE]"
             45 5D
      008EC0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   3321         .endif
      008EC0                       3322 BCOMP:
      008EC0 AD 82            [ 4] 3323         CALLR   TICK
      008EC2 20 B9            [ 2] 3324         JRA     JSRC
                                   3325         .endif
                                   3326 
                                   3327 ;       COMPILE ( -- )
                                   3328 ;       Compile next jsr in
                                   3329 ;       colon list to code dictionary.
                                   3330 
                           000001  3331         .ifeq   BAREBONES
      000E39                       3332         HEADFLG COMPI "COMPILE" COMPO
                           000001     1         .ifeq   UNLINK_COMPI
      008EC4 8E B6                    2         .dw     LINK
                           000E3B     3         LINK    = .
      008EC6 47                       4         .db      ((102$ - 101$) + COMPO)
      008EC7                          5 101$:
      008EC7 43 4F 4D 50 49 4C 45     6         .ascii  "COMPILE"
      008ECE                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   3333         .endif
      008ECE                       3334 COMPI:
      008ECE 51               [ 1] 3335         EXGW    X,Y
      008ECF 85               [ 2] 3336         POPW    X
      008ED0 F6               [ 1] 3337         LD      A,(X)
      008ED1 5C               [ 1] 3338         INCW    X
      008ED2 A1 CD            [ 1] 3339         CP      A,#CALL_OPC
      008ED4 26 0A            [ 1] 3340         JRNE    COMPIO1
      008ED6 BF 7E            [ 2] 3341         LDW     YTEMP,X         ; COMPILE CALL address
      008ED8 5C               [ 1] 3342         INCW    X
      008ED9 5C               [ 1] 3343         INCW    X
      008EDA 89               [ 2] 3344         PUSHW   X
      008EDB 92 CE 7E         [ 5] 3345         LDW     X,[YTEMP]
      008EDE 20 0C            [ 2] 3346         JRA     COMPIO2
      008EE0                       3347 COMPIO1:
      008EE0 F6               [ 1] 3348         LD      A,(X)           ; COMPILE CALLR offset
      008EE1 5C               [ 1] 3349         INCW    X
      008EE2 89               [ 2] 3350         PUSHW   X               ; return address
      008EE3 5F               [ 1] 3351         CLRW    X               ; offset i8_t to i16_t
      008EE4 4D               [ 1] 3352         TNZ     A
      008EE5 2A 01            [ 1] 3353         JRPL    1$
      008EE7 5A               [ 2] 3354         DECW    X
      008EE8 97               [ 1] 3355 1$:     LD      XL,A
      008EE9 72 FB 01         [ 2] 3356         ADDW    X,(1,SP)        ; add offset in X to address of next instruction
      008EEC                       3357 COMPIO2:
      008EEC 51               [ 1] 3358         EXGW    X,Y
      008EED CD 84 28         [ 4] 3359         CALL    YSTOR
      008EF0 20 8B            [ 2] 3360         JRA     JSRC            ; compile subroutine
                                   3361 
                                   3362 
                                   3363 ;       $,"     ( -- )
                                   3364 ;       Compile a literal string
                                   3365 ;       up to next " .
                           000000  3366         .ifeq   UNLINK_STRCQ
                                   3367 ;       HEADER  STRCQ '$,"'
                                   3368          .dw     LINK
                                   3369 
                                   3370         LINK =  .
                                   3371         .db     3
                                   3372         .ascii  '$,"'
                                   3373         .endif
      008EF2                       3374 STRCQ:
      000E67                       3375         DoLitC  34              ; "
      008EF2 83               [ 9]    1         TRAP
      008EF3 00 22                    2         .dw     34
      008EF5 CD 8B B4         [ 4] 3376         CALL    PARSE
      008EF8 CD 88 67         [ 4] 3377         CALL    HERE
      008EFB CD 88 D9         [ 4] 3378         CALL    PACKS           ; string to code dictionary
      008EFE                       3379 CNTPCPPSTORE:
      008EFE CD 88 49         [ 4] 3380         CALL    COUNT
      008F01 CD 84 B0         [ 4] 3381         CALL    PLUS            ; calculate aligned end of string
      008F04 CD 85 13         [ 4] 3382         CALL    CPP
      008F07 CC 83 E4         [ 2] 3383         JP      STORE
                                   3384 
                                   3385 ; Structures
                                   3386 
                           000001  3387         .ifeq   BOOTSTRAP
                                   3388 ;       FOR     ( -- a )
                                   3389 ;       Start a FOR-NEXT loop
                                   3390 ;       structure in a colon definition.
                                   3391 
      000E7F                       3392         HEADFLG FOR "FOR" IMEDD
                           000001     1         .ifeq   UNLINK_FOR
      008F0A 8E C6                    2         .dw     LINK
                           000E81     3         LINK    = .
      008F0C 83                       4         .db      ((102$ - 101$) + IMEDD)
      008F0D                          5 101$:
      008F0D 46 4F 52                 6         .ascii  "FOR"
      008F10                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008F10                       3393 FOR:
      008F10 AD BC            [ 4] 3394         CALLR   COMPI
      008F12 CD 84 3A         [ 4] 3395         CALL    TOR
      008F15 CC 88 67         [ 2] 3396         JP      HERE
                                   3397 
                                   3398 ;       NEXT    ( a -- )
                                   3399 ;       Terminate a FOR-NEXT loop.
                                   3400 
      000E8D                       3401         HEADFLG NEXT "NEXT" IMEDD
                           000001     1         .ifeq   UNLINK_NEXT
      008F18 8F 0C                    2         .dw     LINK
                           000E8F     3         LINK    = .
      008F1A 84                       4         .db      ((102$ - 101$) + IMEDD)
      008F1B                          5 101$:
      008F1B 4E 45 58 54              6         .ascii  "NEXT"
      008F1F                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008F1F                       3402 NEXT:
      008F1F AD AD            [ 4] 3403         CALLR   COMPI
      008F21 CD 83 26         [ 4] 3404         CALL    DONXT
      008F24 CC 8E 54         [ 2] 3405         JP      COMMA
                                   3406         .endif
                                   3407 
                           000001  3408         .ifne   HAS_DOLOOP
                           000001  3409         .ifeq   BOOTSTRAP
                                   3410 ;       DO      ( n1 n2 -- )
                                   3411 ;       Start a DO LOOP loop from n1 to n2
                                   3412 ;       structure in a colon definition.
                                   3413 
      000E9C                       3414         HEADFLG DOO "DO" IMEDD
                           000001     1         .ifeq   UNLINK_DOO
      008F27 8F 1A                    2         .dw     LINK
                           000E9E     3         LINK    = .
      008F29 82                       4         .db      ((102$ - 101$) + IMEDD)
      008F2A                          5 101$:
      008F2A 44 4F                    6         .ascii  "DO"
      008F2C                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008F2C                       3415 DOO:
      008F2C CD 82 EA         [ 4] 3416         CALL    CCOMMALIT
      008F2F 83                    3417         .db     DOLIT_OPC       ; LOOP address cell for usage by LEAVE at runtime
      008F30 CD 8F D6         [ 4] 3418         CALL    ZEROCOMMA       ; changes here require an offset adjustment in PLOOP
      008F33 AD 99            [ 4] 3419         CALLR   COMPI
      008F35 CD 84 3A         [ 4] 3420         CALL    TOR
      008F38 AD 94            [ 4] 3421         CALLR   COMPI
      008F3A CD 84 7C         [ 4] 3422         CALL    SWAPP
      008F3D AD 8F            [ 4] 3423         CALLR   COMPI
      008F3F CD 84 3A         [ 4] 3424         CALL    TOR
      008F42 20 CC            [ 2] 3425         JRA     FOR
                                   3426 
                                   3427 ;       LOOP    ( a -- )
                                   3428 ;       Terminate a DO-LOOP loop.
                                   3429 
      000EB9                       3430         HEADFLG LOOP "LOOP" IMEDD
                           000001     1         .ifeq   UNLINK_LOOP
      008F44 8F 29                    2         .dw     LINK
                           000EBB     3         LINK    = .
      008F46 84                       4         .db      ((102$ - 101$) + IMEDD)
      008F47                          5 101$:
      008F47 4C 4F 4F 50              6         .ascii  "LOOP"
      008F4B                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008F4B                       3431 LOOP:
      008F4B CD 8E CE         [ 4] 3432         CALL    COMPI
      008F4E CD 85 69         [ 4] 3433         CALL    ONE
      008F51 20 08            [ 2] 3434         JRA     PLOOP
                                   3435 
                                   3436 ;       +LOOP   ( a +n -- )
                                   3437 ;       Terminate a DO - +LOOP loop.
                                   3438 
      000EC8                       3439         HEADFLG PLOOP "+LOOP" IMEDD
                           000001     1         .ifeq   UNLINK_PLOOP
      008F53 8F 46                    2         .dw     LINK
                           000ECA     3         LINK    = .
      008F55 85                       4         .db      ((102$ - 101$) + IMEDD)
      008F56                          5 101$:
      008F56 2B 4C 4F 4F 50           6         .ascii  "+LOOP"
      008F5B                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008F5B                       3440 PLOOP:
      008F5B CD 8E CE         [ 4] 3441         CALL    COMPI
      008F5E CD 82 F4         [ 4] 3442         CALL    DOPLOOP
      008F61 CD 88 67         [ 4] 3443         CALL    HERE
      008F64 CD 84 91         [ 4] 3444         CALL    OVER            ; use mark from DO/FOR, apply negative offset
      000EDC                       3445         DoLitC  14
      008F67 83               [ 9]    1         TRAP
      008F68 00 0E                    2         .dw     14
      008F6A CD 85 01         [ 4] 3446         CALL    SUBB
      008F6D CD 83 E4         [ 4] 3447         CALL    STORE           ; patch DO runtime code for LEAVE
      008F70 CC 8E 54         [ 2] 3448         JP      COMMA
                                   3449         .endif
                                   3450         .endif
                                   3451 
                           000001  3452         .ifeq   BOOTSTRAP
                                   3453 ;       BEGIN   ( -- a )
                                   3454 ;       Start an infinite or
                                   3455 ;       indefinite loop structure.
                                   3456 
      000EE8                       3457         HEADFLG BEGIN "BEGIN" IMEDD
                           000001     1         .ifeq   UNLINK_BEGIN
      008F73 8F 55                    2         .dw     LINK
                           000EEA     3         LINK    = .
      008F75 85                       4         .db      ((102$ - 101$) + IMEDD)
      008F76                          5 101$:
      008F76 42 45 47 49 4E           6         .ascii  "BEGIN"
      008F7B                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008F7B                       3458 BEGIN:
      008F7B CC 88 67         [ 2] 3459         JP      HERE
                                   3460 
                                   3461 ;       UNTIL   ( a -- )
                                   3462 ;       Terminate a BEGIN-UNTIL
                                   3463 ;       indefinite loop structure.
                                   3464 
      000EF3                       3465         HEADFLG UNTIL "UNTIL" IMEDD
                           000001     1         .ifeq   UNLINK_UNTIL
      008F7E 8F 75                    2         .dw     LINK
                           000EF5     3         LINK    = .
      008F80 85                       4         .db      ((102$ - 101$) + IMEDD)
      008F81                          5 101$:
      008F81 55 4E 54 49 4C           6         .ascii  "UNTIL"
      008F86                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008F86                       3466 UNTIL:
      008F86 CD 8E CE         [ 4] 3467         CALL    COMPI
      008F89 CD 83 3C         [ 4] 3468         CALL    QBRAN
      008F8C CC 8E 54         [ 2] 3469         JP      COMMA
                                   3470 
                                   3471 ;       AGAIN   ( a -- )
                                   3472 ;       Terminate a BEGIN-AGAIN
                                   3473 ;       infinite loop structure.
                                   3474 
      000F04                       3475         HEADFLG AGAIN "AGAIN" IMEDD
                           000001     1         .ifeq   UNLINK_AGAIN
      008F8F 8F 80                    2         .dw     LINK
                           000F06     3         LINK    = .
      008F91 85                       4         .db      ((102$ - 101$) + IMEDD)
      008F92                          5 101$:
      008F92 41 47 41 49 4E           6         .ascii  "AGAIN"
      008F97                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008F97                       3476 AGAIN:
      008F97 CD 82 EA         [ 4] 3477         CALL    CCOMMALIT
      008F9A CC                    3478         .db     BRAN_OPC
      008F9B CC 8E 54         [ 2] 3479         JP      COMMA
                                   3480 
                                   3481 ;       IF      ( -- A )
                                   3482 ;       Begin a conditional branch.
                                   3483 
      000F13                       3484         HEADFLG IFF "IF" IMEDD
                           000001     1         .ifeq   UNLINK_IFF
      008F9E 8F 91                    2         .dw     LINK
                           000F15     3         LINK    = .
      008FA0 82                       4         .db      ((102$ - 101$) + IMEDD)
      008FA1                          5 101$:
      008FA1 49 46                    6         .ascii  "IF"
      008FA3                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008FA3                       3485 IFF:
      008FA3 CD 8E CE         [ 4] 3486         CALL    COMPI
      008FA6 CD 83 3C         [ 4] 3487         CALL    QBRAN
      008FA9 20 28            [ 2] 3488         JRA     HERE0COMMA
                                   3489 
                                   3490 ;       THEN    ( A -- )
                                   3491 ;       Terminate a conditional branch structure.
                                   3492 
      000F20                       3493         HEADFLG THENN "THEN" IMEDD
                           000001     1         .ifeq   UNLINK_THENN
      008FAB 8F A0                    2         .dw     LINK
                           000F22     3         LINK    = .
      008FAD 84                       4         .db      ((102$ - 101$) + IMEDD)
      008FAE                          5 101$:
      008FAE 54 48 45 4E              6         .ascii  "THEN"
      008FB2                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008FB2                       3494 THENN:
      008FB2 CD 88 67         [ 4] 3495         CALL    HERE
      008FB5 AD 2F            [ 4] 3496         CALLR   SWAPLOC
      008FB7 CC 83 E4         [ 2] 3497         JP      STORE
                                   3498 
                                   3499 ;       ELSE    ( A -- A )
                                   3500 ;       Start the false clause in an IF-ELSE-THEN structure.
                                   3501 
      000F2F                       3502         HEADFLG ELSE "ELSE" IMEDD
                           000001     1         .ifeq   UNLINK_ELSE
      008FBA 8F AD                    2         .dw     LINK
                           000F31     3         LINK    = .
      008FBC 84                       4         .db      ((102$ - 101$) + IMEDD)
      008FBD                          5 101$:
      008FBD 45 4C 53 45              6         .ascii  "ELSE"
      008FC1                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008FC1                       3503 ELSEE:
      008FC1 AD 0C            [ 4] 3504         CALLR   AHEAD
      008FC3 AD 21            [ 4] 3505         CALLR   SWAPLOC
      008FC5 20 EB            [ 2] 3506         JRA     THENN
                                   3507 
                                   3508 ;       AHEAD   ( -- A )
                                   3509 ;       Compile a forward branch instruction.
                                   3510 
                           000001  3511         .ifeq   BAREBONES
      000F3C                       3512         HEADFLG AHEAD "AHEAD" IMEDD
                           000001     1         .ifeq   UNLINK_AHEAD
      008FC7 8F BC                    2         .dw     LINK
                           000F3E     3         LINK    = .
      008FC9 85                       4         .db      ((102$ - 101$) + IMEDD)
      008FCA                          5 101$:
      008FCA 41 48 45 41 44           6         .ascii  "AHEAD"
      008FCF                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   3513         .endif
      008FCF                       3514 AHEAD:
      008FCF CD 82 EA         [ 4] 3515         CALL    CCOMMALIT
      008FD2 CC                    3516         .db     BRAN_OPC
      008FD3                       3517 HERE0COMMA:
      008FD3 CD 88 67         [ 4] 3518         CALL    HERE
                                   3519         .endif
      008FD6                       3520 ZEROCOMMA:
      008FD6 CD 85 66         [ 4] 3521         CALL    ZERO
      008FD9 CC 8E 54         [ 2] 3522         JP      COMMA
                                   3523 
                           000001  3524         .ifeq   BOOTSTRAP
                                   3525 ;       WHILE   ( a -- A a )
                                   3526 ;       Conditional branch out of a BEGIN-WHILE-REPEAT loop.
                                   3527 
      000F51                       3528         HEADFLG WHILE "WHILE" IMEDD
                           000001     1         .ifeq   UNLINK_WHILE
      008FDC 8F C9                    2         .dw     LINK
                           000F53     3         LINK    = .
      008FDE 85                       4         .db      ((102$ - 101$) + IMEDD)
      008FDF                          5 101$:
      008FDF 57 48 49 4C 45           6         .ascii  "WHILE"
      008FE4                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008FE4                       3529 WHILE:
      008FE4 AD BD            [ 4] 3530         CALLR   IFF
      008FE6                       3531 SWAPLOC:
      008FE6 CC 84 7C         [ 2] 3532         JP      SWAPP
                                   3533 
                                   3534 ;       REPEAT  ( A a -- )
                                   3535 ;       Terminate a BEGIN-WHILE-REPEAT indefinite loop.
                                   3536 
      000F5E                       3537         HEADFLG REPEA "REPEAT" IMEDD
                           000001     1         .ifeq   UNLINK_REPEA
      008FE9 8F DE                    2         .dw     LINK
                           000F60     3         LINK    = .
      008FEB 86                       4         .db      ((102$ - 101$) + IMEDD)
      008FEC                          5 101$:
      008FEC 52 45 50 45 41 54        6         .ascii  "REPEAT"
      008FF2                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008FF2                       3538 REPEA:
      008FF2 AD A3            [ 4] 3539         CALLR   AGAIN
      008FF4 20 BC            [ 2] 3540         JRA     THENN
                                   3541 
                                   3542 ;       AFT     ( a -- a A )
                                   3543 ;       Jump to THEN in a FOR-AFT-THEN-NEXT loop the first time through.
                                   3544 
      000F6B                       3545         HEADFLG AFT "AFT" IMEDD
                           000001     1         .ifeq   UNLINK_AFT
      008FF6 8F EB                    2         .dw     LINK
                           000F6D     3         LINK    = .
      008FF8 83                       4         .db      ((102$ - 101$) + IMEDD)
      008FF9                          5 101$:
      008FF9 41 46 54                 6         .ascii  "AFT"
      008FFC                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      008FFC                       3546 AFT:
      008FFC CD 84 56         [ 4] 3547         CALL    DROP
      008FFF AD CE            [ 4] 3548         CALLR   AHEAD
      009001 CD 88 67         [ 4] 3549         CALL    HERE
      009004 20 E0            [ 2] 3550         JRA     SWAPLOC
                                   3551         .endif
                                   3552 
                           000001  3553         .ifeq   BOOTSTRAP
                                   3554 ;       ABORT"  ( -- ; <string> )
                                   3555 ;       Conditional abort with an error message.
                                   3556 
                           000001  3557         .ifeq   BAREBONES
      000F7B                       3558         HEADFLG ABRTQ 'ABORT"' IMEDD
                           000001     1         .ifeq   UNLINK_ABRTQ
      009006 8F F8                    2         .dw     LINK
                           000F7D     3         LINK    = .
      009008 86                       4         .db      ((102$ - 101$) + IMEDD)
      009009                          5 101$:
      009009 41 42 4F 52 54 22        6         .ascii  'ABORT"'
      00900F                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   3559         .endif
      00900F                       3560 ABRTQ:
      00900F CD 8E CE         [ 4] 3561         CALL    COMPI
      009012 CD 8D 68         [ 4] 3562         CALL    ABORQ
      009015 20 0B            [ 2] 3563         JRA     STRCQLOC
                                   3564         .endif
                                   3565 
                                   3566 ;       $"      ( -- ; <string> )
                                   3567 ;       Compile an inline string literal.
                                   3568 
                           000001  3569         .ifne   WORDS_LINKCHAR
      000F8C                       3570         HEADFLG STRQ '$"' IMEDD
                           000001     1         .ifeq   UNLINK_STRQ
      009017 90 08                    2         .dw     LINK
                           000F8E     3         LINK    = .
      009019 82                       4         .db      ((102$ - 101$) + IMEDD)
      00901A                          5 101$:
      00901A 24 22                    6         .ascii  '$"'
      00901C                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   3571         .endif
      00901C                       3572 STRQ:
      00901C CD 8E CE         [ 4] 3573         CALL    COMPI
      00901F CD 8A A1         [ 4] 3574         CALL    STRQP
      009022                       3575 STRCQLOC:
      009022 CC 8E F2         [ 2] 3576         JP      STRCQ
                                   3577 
                           000001  3578         .ifeq   BOOTSTRAP
                                   3579 ;       ."      ( -- ; <string> )
                                   3580 ;       Compile an inline string literal to be typed out at run time.
                                   3581 
      000F9A                       3582         HEADFLG DOTQ '."' IMEDD
                           000001     1         .ifeq   UNLINK_DOTQ
      009025 90 19                    2         .dw     LINK
                           000F9C     3         LINK    = .
      009027 82                       4         .db      ((102$ - 101$) + IMEDD)
      009028                          5 101$:
      009028 2E 22                    6         .ascii  '."'
      00902A                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00902A                       3583 DOTQ:
      00902A CD 8E CE         [ 4] 3584         CALL    COMPI
      00902D CD 8A A4         [ 4] 3585         CALL    DOTQP
      009030 20 F0            [ 2] 3586         JRA     STRCQLOC
                                   3587         .endif
                                   3588 
                                   3589 ; Name compiler
                                   3590 
                                   3591 ;       ?UNIQUE ( a -- a )
                                   3592 ;       Display a warning message
                                   3593 ;       if word already exists.
                                   3594 
                           000000  3595         .ifne   WORDS_LINKCOMP
                                   3596         HEADER  UNIQU "?UNIQUE"
                                   3597         .endif
      009032                       3598 UNIQU:
      009032 CD 84 6F         [ 4] 3599         CALL    DUPP
      009035 CD 8C 66         [ 4] 3600         CALL    NAMEQ           ; ?name exists
      009038 CD 83 3C         [ 4] 3601         CALL    QBRAN
      00903B 90 4E                 3602         .dw     UNIQ1
      00903D CD 8A A4         [ 4] 3603         CALL    DOTQP           ; redef are OK
      009040 07                    3604         .db     7
      009041 20 72 65 44 65 66 20  3605         .ascii  " reDef "
      009048 CD 84 91         [ 4] 3606         CALL    OVER
      00904B CD 8A A6         [ 4] 3607         CALL    COUNTTYPES      ; just in case
      00904E CC 84 56         [ 2] 3608 UNIQ1:  JP      DROP
                                   3609 
                                   3610 
                                   3611 ;       $,n     ( na -- )
                                   3612 ;       Build a new dictionary name
                                   3613 ;       using string at na.
                                   3614 
                           000000  3615         .ifne   WORDS_LINKCOMP
                                   3616         .ifeq   UNLINK_SNAME
                                   3617 ;       HEADER  SNAME "$,n"
                                   3618         .dw     LINK
                                   3619 
                                   3620         LINK =  .
                                   3621         .db     3
                                   3622         .ascii  "$,n"
                                   3623         .endif
                                   3624         .endif
      009051                       3625 SNAME:
      009051 CD 92 AC         [ 4] 3626         CALL    DUPPCAT         ; ?null input
      009054 CD 83 3C         [ 4] 3627         CALL    QBRAN
      009057 90 79                 3628         .dw     PNAM1
      009059 AD D7            [ 4] 3629         CALLR   UNIQU           ; ?redefinition
      00905B CD 84 6F         [ 4] 3630         CALL    DUPP
      00905E CD 8E FE         [ 4] 3631         CALL    CNTPCPPSTORE
      009061 CD 84 6F         [ 4] 3632         CALL    DUPP
      009064 CD 85 41         [ 4] 3633         CALL    LAST
      009067 CD 83 E4         [ 4] 3634         CALL    STORE           ; save na for vocabulary link
      00906A CD 87 96         [ 4] 3635         CALL    CELLM           ; link address
      00906D CD 85 06         [ 4] 3636         CALL    CNTXT
      009070 CD 83 D9         [ 4] 3637         CALL    AT
      009073 CD 84 7C         [ 4] 3638         CALL    SWAPP
      009076 CC 83 E4         [ 2] 3639         JP      STORE           ; save code pointer
      009079 CD 8A A1         [ 4] 3640 PNAM1:  CALL    STRQP
      00907C 05                    3641         .db     5
      00907D 20 6E 61 6D 65        3642         .ascii  " name"         ; null input
      009082 CC 8D 70         [ 2] 3643         JP      ABOR1
                                   3644 
                                   3645 ; FORTH compiler
                                   3646 
                                   3647 ;       $COMPILE        ( a -- )
                                   3648 ;       Compile next word to
                                   3649 ;       dictionary as a token or literal.
                                   3650 
      009085                       3651         HEADER  SCOMP "$COMPILE"
                           000000     1         .ifeq   UNLINK_SCOMP
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "$COMPILE"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      000FFA                       3652 SCOMP:
      009085 CD 8C 66         [ 4] 3653         CALL    NAMEQ
      009088 CD 83 37         [ 4] 3654         CALL    QDQBRAN         ; ?defined
      00908B 90 9E                 3655         .dw     SCOM2
      00908D CD 8B 1F         [ 4] 3656         CALL    YFLAGS
      009090 90 FE            [ 2] 3657         LDW     Y,(Y)
                                   3658 
      009092 90 9E            [ 1] 3659         LD      A,YH
      009094 A4 80            [ 1] 3660         AND     A,#IMEDD
      009096 27 03            [ 1] 3661         JREQ    SCOM1
                                   3662 
      009098 CC 83 59         [ 2] 3663         JP      EXECU
      00909B CC 8E 7D         [ 2] 3664 SCOM1:  JP      JSRC
      00909E CD 89 98         [ 4] 3665 SCOM2:  CALL    NUMBQ           ; try to convert to number
      0090A1 CD 83 3C         [ 4] 3666         CALL    QBRAN
      0090A4 8D 70                 3667         .dw     ABOR1
      0090A6 CC 8E AE         [ 2] 3668         JP      LITER
                                   3669 
                                   3670 ;       OVERT   ( -- )
                                   3671 ;       Link a new word into vocabulary.
                                   3672 
                           000001  3673         .ifne   WORDS_LINKCOMP + HAS_ALIAS
      00101E                       3674         HEADER  OVERT "OVERT"
                           000001     1         .ifeq   UNLINK_OVERT
      0090A9 90 27                    2         .dw     LINK
                           001020     3         LINK    = .
      0090AB 05                       4         .db      (102$ - 101$)
      0090AC                          5 101$:
      0090AC 4F 56 45 52 54           6         .ascii  "OVERT"
      0090B1                          7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   3675         .endif
      0090B1                       3676 OVERT:
                           000001  3677         .ifne   HAS_CPNVM
      0090B1 CD 85 41         [ 4] 3678         CALL    LAST
      0090B4 CD 83 D9         [ 4] 3679         CALL    AT
                                   3680 
      0090B7 90 9E            [ 1] 3681         LD      A,YH
      0090B9 A4 F8            [ 1] 3682         AND     A,#0xF8         ; does USRLAST point to NVM?
      0090BB 27 19            [ 1] 3683         JREQ    1$
                                   3684 
      0090BD 90 BF 74         [ 2] 3685         LDW     NVMCONTEXT,Y    ; update NVMCONTEXT
      0090C0 90 BE 70         [ 2] 3686         LDW     Y,USRCTOP
      0090C3 CD 84 28         [ 4] 3687         CALL    YSTOR
                                   3688 
      0090C6 CD 84 6F         [ 4] 3689         CALL    DUPP
      0090C9 CD 83 D9         [ 4] 3690         CALL    AT
      0090CC CD 83 3C         [ 4] 3691         CALL    QBRAN
      0090CF 90 D3                 3692         .dw     2$
      0090D1 20 06            [ 2] 3693         JRA     OVSTORE         ; link dictionary in RAM
      0090D3                       3694 2$:
      0090D3 CD 84 56         [ 4] 3695         CALL    DROP
      0090D6                       3696 1$:
      00104B                       3697         DoLitC  USRCONTEXT
      0090D6 83               [ 9]    1         TRAP
      0090D7 00 76                    2         .dw     USRCONTEXT
      0090D9                       3698 OVSTORE:
      0090D9 CC 83 E4         [ 2] 3699         JP      STORE           ; or update USRCONTEXT
                                   3700 
                           000000  3701         .else
                                   3702 
                                   3703         LDW     Y,USRLAST
                                   3704         LDW     USRCONTEXT,Y
                                   3705         RET
                                   3706         .endif
                                   3707 
                                   3708 ;       ;       ( -- )
                                   3709 ;       Terminate a colon definition.
                                   3710 
                                   3711 ;       HEADFLG SEMIS ";" (IMEDD+COMPO)
      0090DC 90 AB                 3712         .dw     LINK
                                   3713 
                           001053  3714         LINK =  .
      0090DE C1                    3715         .db     (IMEDD+COMPO+1)
      0090DF 3B                    3716         .ascii  ";"
                                   3717 
      0090E0                       3718 SEMIS:
      0090E0 CD 82 EA         [ 4] 3719         CALL    CCOMMALIT
      0090E3 81                    3720         .db     EXIT_OPC
      0090E4 CD 8D BD         [ 4] 3721         CALL    LBRAC
      0090E7 20 C8            [ 2] 3722         JRA     OVERT
                                   3723 
                                   3724 ;       :       ( -- ; <string> )
                                   3725 ;       Start a new colon definition
                                   3726 ;       using next word as its name.
      00105E                       3727         HEADER  COLON ":"
                           000001     1         .ifeq   UNLINK_COLON
      0090E9 90 DE                    2         .dw     LINK
                           001060     3         LINK    = .
      0090EB 01                       4         .db      (102$ - 101$)
      0090EC                          5 101$:
      0090EC 3A                       6         .ascii  ":"
      0090ED                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0090ED                       3728 COLON:
      0090ED AD 07            [ 4] 3729         CALLR   RBRAC           ; do "]" first to set HERE to compile state
      0090EF CC 8C 00         [ 2] 3730         JP      TOKSNAME        ; copy token to dictionary
                                   3731 
                                   3732 
                                   3733 ;       ]       ( -- )
                                   3734 ;       Start compiling words in
                                   3735 ;       input stream.
      001067                       3736         HEADER  RBRAC "]"
                           000001     1         .ifeq   UNLINK_RBRAC
      0090F2 90 EB                    2         .dw     LINK
                           001069     3         LINK    = .
      0090F4 01                       4         .db      (102$ - 101$)
      0090F5                          5 101$:
      0090F5 5D                       6         .ascii  "]"
      0090F6                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0090F6                       3737 RBRAC:
      0090F6 90 AE 90 85      [ 2] 3738         LDW     Y,#SCOMP
      0090FA 90 BF 66         [ 2] 3739         LDW     USREVAL,Y
      0090FD 81               [ 4] 3740         RET
                                   3741 
                                   3742 
                                   3743 ; Defining words
                                   3744 
                           000001  3745         .ifne   HAS_DOES
                                   3746 
                                   3747 ;       DOES>   ( -- )
                                   3748 ;       Define action of defining words
                                   3749 
      001073                       3750         HEADFLG DOESS "DOES>" IMEDD
                           000001     1         .ifeq   UNLINK_DOESS
      0090FE 90 F4                    2         .dw     LINK
                           001075     3         LINK    = .
      009100 85                       4         .db      ((102$ - 101$) + IMEDD)
      009101                          5 101$:
      009101 44 4F 45 53 3E           6         .ascii  "DOES>"
      009106                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009106                       3751 DOESS:
      009106 CD 8E CE         [ 4] 3752         CALL    COMPI
      009109 AD 17            [ 4] 3753         CALLR   DODOES          ; 3 CALL dodoes>
      00910B CD 88 67         [ 4] 3754         CALL    HERE
                           000000  3755         .ifne  USE_CALLDOLIT
                                   3756         DoLitC  9
                           000001  3757         .else
      001083                       3758         DoLitC  7
      00910E 83               [ 9]    1         TRAP
      00910F 00 07                    2         .dw     7
                                   3759         .endif
      009111 CD 84 B0         [ 4] 3760         CALL    PLUS
      009114 CD 8E AE         [ 4] 3761         CALL    LITER           ; 3 CALL doLit + 2 (HERE+9)
      009117 CD 8E CE         [ 4] 3762         CALL    COMPI
      00911A CD 8E 54         [ 4] 3763         CALL    COMMA           ; 3 CALL COMMA
      00911D CD 82 EA         [ 4] 3764         CALL    CCOMMALIT
      009120 81                    3765         .db     EXIT_OPC        ; 1 RET (EXIT)
      009121 81               [ 4] 3766         RET
                                   3767 
                                   3768 ;       dodoes  ( -- )
                                   3769 ;       link action to words created by defining words
                                   3770 
                           000000  3771         .ifne   WORDS_LINKRUNTI
                                   3772         HEADER  DODOES "dodoes"
                                   3773         .endif
      009122                       3774 DODOES:
      009122 CD 85 41         [ 4] 3775         CALL    LAST            ; ( link field of current word )
      009125 CD 83 D9         [ 4] 3776         CALL    AT
      009128 CD 8C 15         [ 4] 3777         CALL    NAMET           ; ' ( 'last  )
      0010A0                       3778         DoLitC  BRAN_OPC        ; ' JP
      00912B 83               [ 9]    1         TRAP
      00912C 00 CC                    2         .dw     BRAN_OPC
      00912E CD 84 91         [ 4] 3779         CALL    OVER            ; ' JP '
      009131 CD 84 04         [ 4] 3780         CALL    CSTOR           ; ' \ CALL <- JP
      009134 CD 88 67         [ 4] 3781         CALL    HERE            ; ' HERE
      009137 CD 84 91         [ 4] 3782         CALL    OVER            ; ' HERE '
      00913A CD 87 B3         [ 4] 3783         CALL    ONEP            ; ' HERE ('+1)
      00913D CD 83 E4         [ 4] 3784         CALL    STORE           ; ' \ CALL DOVAR <- JP HERE
                           000000  3785         .ifne  USE_CALLDOLIT
                                   3786         CALL    COMPI
                                   3787         CALL    DOLIT           ; ' \ HERE <- DOLIT
                           000001  3788         .else
      009140 CD 82 EA         [ 4] 3789         CALL    CCOMMALIT
      009143 83                    3790         .db     DOLIT_OPC       ; \ HERE <- DOLIT <- ('+3) <- branch
                                   3791         .endif
      0010B9                       3792         DoLitC  3               ; ' 3
      009144 83               [ 9]    1         TRAP
      009145 00 03                    2         .dw     3
      009147 CD 84 B0         [ 4] 3793         CALL    PLUS            ; ('+3)
      00914A CD 8E 54         [ 4] 3794         CALL    COMMA           ; \ HERE <- DOLIT <-('+3)
      00914D CD 82 EA         [ 4] 3795         CALL    CCOMMALIT
      009150 CC                    3796         .db     BRAN_OPC        ; \ HERE <- DOLIT <- ('+3) <- branch
      009151 81               [ 4] 3797         RET
                                   3798         .endif
                                   3799 
                                   3800 ;       CREATE  ( -- ; <string> )
                                   3801 ;       Compile a new array
                                   3802 ;       without allocating space.
                                   3803 
      0010C7                       3804         HEADER  CREAT "CREATE"
                           000001     1         .ifeq   UNLINK_CREAT
      009152 91 00                    2         .dw     LINK
                           0010C9     3         LINK    = .
      009154 06                       4         .db      (102$ - 101$)
      009155                          5 101$:
      009155 43 52 45 41 54 45        6         .ascii  "CREATE"
      00915B                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00915B                       3805 CREAT:
      00915B CD 8C 00         [ 4] 3806         CALL    TOKSNAME        ; copy token to dictionary
      00915E CD 90 B1         [ 4] 3807         CALL    OVERT
      009161 CD 8E CE         [ 4] 3808         CALL    COMPI
      009164 CD 84 26         [ 4] 3809         CALL    DOVAR
      009167 81               [ 4] 3810         RET
                                   3811 
                           000001  3812         .ifeq   UNLINK_CONST
                                   3813 ;       CONSTANT ( "name" n -- )
                                   3814 ;       Create a named constant with state dependant action
                                   3815 
      0010DD                       3816         HEADER CONST "CONSTANT"
                           000001     1         .ifeq   UNLINK_CONST
      009168 91 54                    2         .dw     LINK
                           0010DF     3         LINK    = .
      00916A 08                       4         .db      (102$ - 101$)
      00916B                          5 101$:
      00916B 43 4F 4E 53 54 41 4E     6         .ascii  "CONSTANT"
             54
      009173                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009173                       3817 CONST:
      009173 CD 90 ED         [ 4] 3818         CALL    COLON
      009176 CD 8E CE         [ 4] 3819         CALL    COMPI
      009179 AD 0B            [ 4] 3820         CALLR   DOCON           ; compile action code
      00917B CD 8E 54         [ 4] 3821         CALL    COMMA           ; compile constant
      00917E CD 8D BD         [ 4] 3822         CALL    LBRAC
      009181 CD 90 B1         [ 4] 3823         CALL    OVERT
      009184 20 5F            [ 2] 3824         JRA     IMMED           ; make immediate
                                   3825 
                                   3826 ;       docon ( -- )
                                   3827 ;       state dependent action code of constant
                                   3828 
      009186                       3829         HEADER  DOCON "docon"
                           000000     1         .ifeq   UNLINK_DOCON
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "docon"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
      0010FB                       3830 DOCON:
      009186 CD 84 13         [ 4] 3831         CALL    RFROM
      009189 CD 83 D9         [ 4] 3832         CALL    AT              ; push constant in interpreter mode
      00918C CD 8D D0         [ 4] 3833         CALL    COMPIQ
      00918F 27 03            [ 1] 3834         JREQ    1$
      009191 CD 8E AE         [ 4] 3835         CALL    LITER           ; compile constant in compiler mode
      009194 81               [ 4] 3836 1$:     RET
                                   3837         .endif
                                   3838 
                           000001  3839         .ifeq   NO_VARIABLE
                                   3840 ;       VARIABLE        ( -- ; <string> )
                                   3841 ;       Compile a new variable
                                   3842 ;       initialized to 0.
                                   3843 
      00110A                       3844         HEADER  VARIA "VARIABLE"
                           000001     1         .ifeq   UNLINK_VARIA
      009195 91 6A                    2         .dw     LINK
                           00110C     3         LINK    = .
      009197 08                       4         .db      (102$ - 101$)
      009198                          5 101$:
      009198 56 41 52 49 41 42 4C     6         .ascii  "VARIABLE"
             45
      0091A0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0091A0                       3845 VARIA:
      0091A0 AD B9            [ 4] 3846         CALLR   CREAT
      0091A2 CD 85 66         [ 4] 3847         CALL    ZERO
                           000001  3848         .ifne   HAS_CPNVM
      0091A5 CD 93 F8         [ 4] 3849         CALL    NVMQ
      0091A8 27 15            [ 1] 3850         JREQ    1$              ; NVM: allocate space in RAM
      00111F                       3851         DoLitW  DOVARPTR        ; overwrite call address "DOVAR" with "DOVARPTR"
      0091AA 83               [ 9]    1         TRAP
      0091AB 84 20                    2         .dw     DOVARPTR
      0091AD CD 88 67         [ 4] 3852         CALL    HERE
      0091B0 CD 87 96         [ 4] 3853         CALL    CELLM
      0091B3 CD 83 E4         [ 4] 3854         CALL    STORE
      0091B6 90 BE 72         [ 2] 3855         LDW     Y,USRVAR
      0091B9 FF               [ 2] 3856         LDW     (X),Y           ; overwrite ZERO with RAM address for COMMA
      00112F                       3857         DoLitC  2               ; Allocate space for variable in RAM
      0091BA 83               [ 9]    1         TRAP
      0091BB 00 02                    2         .dw     2
      0091BD AD 0B            [ 4] 3858         CALLR   ALLOT
                                   3859         .endif
      0091BF CC 8E 54         [ 2] 3860 1$:     JP      COMMA
                                   3861         .endif
                                   3862 
                                   3863 
                           000001  3864         .ifeq   NO_VARIABLE
                                   3865 ;       ALLOT   ( n -- )
                                   3866 ;       Allocate n bytes to code DICTIONARY.
                                   3867 
      001137                       3868         HEADER  ALLOT "ALLOT"
                           000001     1         .ifeq   UNLINK_ALLOT
      0091C2 91 97                    2         .dw     LINK
                           001139     3         LINK    = .
      0091C4 05                       4         .db      (102$ - 101$)
      0091C5                          5 101$:
      0091C5 41 4C 4C 4F 54           6         .ascii  "ALLOT"
      0091CA                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0091CA                       3869 ALLOT:
      0091CA CD 85 13         [ 4] 3870         CALL    CPP
                           000001  3871         .ifne   HAS_CPNVM
      0091CD CD 93 F8         [ 4] 3872         CALL    NVMQ
      0091D0 27 04            [ 1] 3873         JREQ    1$              ; NVM: allocate space in RAM
      0091D2 A6 72            [ 1] 3874         LD      A,#(USRVAR)
      0091D4 E7 01            [ 1] 3875         LD      (1,X),A
      0091D6                       3876 1$:
                                   3877         .endif
      0091D6 CC 88 2A         [ 2] 3878         JP      PSTOR
                                   3879         .endif
                                   3880 
                                   3881 ; Tools
                                   3882 
                                   3883 ;       IMMEDIATE       ( -- )
                                   3884 ;       Make last compiled word
                                   3885 ;       an immediate word.
                                   3886 
      00114E                       3887         HEADER  IMMED "IMMEDIATE"
                           000001     1         .ifeq   UNLINK_IMMED
      0091D9 91 C4                    2         .dw     LINK
                           001150     3         LINK    = .
      0091DB 09                       4         .db      (102$ - 101$)
      0091DC                          5 101$:
      0091DC 49 4D 4D 45 44 49 41     6         .ascii  "IMMEDIATE"
             54 45
      0091E5                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0091E5                       3888 IMMED:
      0091E5 92 C6 6C         [ 4] 3889         LD      A,[USRLAST]
      0091E8 AA 80            [ 1] 3890         OR      A,#IMEDD
      0091EA 92 C7 6C         [ 4] 3891         LD      [USRLAST],A
      0091ED 81               [ 4] 3892         RET
                                   3893 
                           000001  3894         .ifeq   BOOTSTRAP
                                   3895 ;       _TYPE   ( b u -- )
                                   3896 ;       Display a string. Filter
                                   3897 ;       non-printing characters.
                                   3898 
                           000000  3899         .ifne   WORDS_LINKMISC
                                   3900         HEADER  UTYPE "_TYPE"
                                   3901         .endif
      0091EE                       3902 UTYPE:
      0091EE CD 84 3A         [ 4] 3903         CALL    TOR             ; start count down loop
      0091F1 20 0C            [ 2] 3904         JRA     UTYP2           ; skip first pass
      0091F3 CD 92 AC         [ 4] 3905 UTYP1:  CALL    DUPPCAT
      0091F6 CD 88 03         [ 4] 3906         CALL    TCHAR
      0091F9 92 CD 60         [ 6] 3907         CALL    [USREMIT]       ; display only printable
      0091FC CD 87 B3         [ 4] 3908         CALL    ONEP            ; increment address
      0091FF CD 83 26         [ 4] 3909 UTYP2:  CALL    DONXT
      009202 91 F3                 3910         .dw     UTYP1           ; loop till done
      009204 CC 84 56         [ 2] 3911         JP      DROP
                                   3912         .endif
                                   3913 
                           000001  3914         .ifeq   REMOVE_DUMP
                                   3915 ;       dm+     ( a u -- a )
                                   3916 ;       Dump u bytes from ,
                                   3917 ;       leaving a+u on  stack.
                                   3918 
                           000000  3919         .ifne   WORDS_LINKMISC
                                   3920         HEADER  DUMPP "dm+"
                                   3921         .endif
      009207                       3922 DUMPP:
      009207 CD 84 91         [ 4] 3923         CALL    OVER
      00117F                       3924         DoLitC  4
      00920A 83               [ 9]    1         TRAP
      00920B 00 04                    2         .dw     4
      00920D CD 8A BE         [ 4] 3925         CALL    UDOTR           ; display address
      009210 CD 8A 66         [ 4] 3926         CALL    SPACE
      009213 CD 84 3A         [ 4] 3927         CALL    TOR             ; start count down loop
      009216 20 0C            [ 2] 3928         JRA     PDUM2           ; skip first pass
      009218 CD 92 AC         [ 4] 3929 PDUM1:  CALL    DUPPCAT
      001190                       3930         DoLitC  3
      00921B 83               [ 9]    1         TRAP
      00921C 00 03                    2         .dw     3
      00921E CD 8A BE         [ 4] 3931         CALL    UDOTR           ; display numeric data
      009221 CD 87 B3         [ 4] 3932         CALL    ONEP            ; increment address
      009224 CD 83 26         [ 4] 3933 PDUM2:  CALL    DONXT
      009227 92 18                 3934         .dw     PDUM1           ; loop till done
      009229 81               [ 4] 3935         RET
                                   3936         .endif
                                   3937 
                           000001  3938         .ifeq   REMOVE_DUMP
                                   3939 ;       DUMP    ( a u -- )
                                   3940 ;       Dump u bytes from a,
                                   3941 ;       in a formatted manner.
                                   3942 
      00119F                       3943         HEADER  DUMP "DUMP"
                           000001     1         .ifeq   UNLINK_DUMP
      00922A 91 DB                    2         .dw     LINK
                           0011A1     3         LINK    = .
      00922C 04                       4         .db      (102$ - 101$)
      00922D                          5 101$:
      00922D 44 55 4D 50              6         .ascii  "DUMP"
      009231                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009231                       3944 DUMP:
      009231 3B 00 65         [ 1] 3945         PUSH    USRBASE+1       ; BASE AT TOR save radix
      009234 CD 89 7E         [ 4] 3946         CALL    HEX
      009237 CD 8B 1F         [ 4] 3947         CALL    YFLAGS
      00923A 90 62            [ 2] 3948         DIV     Y,A             ; / change count to lines
      00923C 90 89            [ 2] 3949         PUSHW   Y               ; start count down loop
      00923E CD 8D CA         [ 4] 3950 DUMP1:  CALL    CR
      0011B6                       3951         DoLitC  16
      009241 83               [ 9]    1         TRAP
      009242 00 10                    2         .dw     16
      009244 CD 85 B1         [ 4] 3952         CALL    DDUP
      009247 AD BE            [ 4] 3953         CALLR   DUMPP           ; display numeric
      009249 CD 85 9F         [ 4] 3954         CALL    ROT
      00924C CD 85 9F         [ 4] 3955         CALL    ROT
      00924F CD 8A 66         [ 4] 3956         CALL    SPACE
      009252 CD 8A 66         [ 4] 3957         CALL    SPACE
      009255 AD 97            [ 4] 3958         CALLR   UTYPE           ; display printable characters
      009257 CD 83 26         [ 4] 3959         CALL    DONXT
      00925A 92 3E                 3960         .dw     DUMP1           ; loop till done
      00925C                       3961 DUMP3:
      00925C 84               [ 1] 3962         POP     A
      00925D B7 65            [ 1] 3963         LD      USRBASE+1,A     ; restore radix
      00925F CC 84 56         [ 2] 3964         JP      DROP
                                   3965         .endif
                                   3966 
                           000001  3967         .ifeq   REMOVE_DOTS
                                   3968 ;       .S      ( ... -- ... )
                                   3969 ;       Display contents of stack.
                                   3970 
      0011D7                       3971         HEADER  DOTS ".S"
                           000001     1         .ifeq   UNLINK_DOTS
      009262 92 2C                    2         .dw     LINK
                           0011D9     3         LINK    = .
      009264 02                       4         .db      (102$ - 101$)
      009265                          5 101$:
      009265 2E 53                    6         .ascii  ".S"
      009267                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009267                       3972 DOTS:
      009267 CD 8D CA         [ 4] 3973         CALL    CR
      00926A CD 88 1A         [ 4] 3974         CALL    DEPTH           ; stack depth
      00926D CD 84 3A         [ 4] 3975         CALL    TOR             ; start count down loop
      009270 20 0C            [ 2] 3976         JRA     DOTS2           ; skip first pass
      009272 CD 84 31         [ 4] 3977 DOTS1:  CALL    RAT
      009275 CD 87 B3         [ 4] 3978         CALL    ONEP
      009278 CD 87 FA         [ 4] 3979         CALL    PICK
      00927B CD 8B 06         [ 4] 3980         CALL    DOT             ; index stack, display contents
      00927E CD 83 26         [ 4] 3981 DOTS2:  CALL    DONXT
      009281 92 72                 3982         .dw     DOTS1           ; loop till done
      009283 CD 8A A4         [ 4] 3983         CALL    DOTQP
      009286 05                    3984         .db     5
      009287 20 3C 73 70 20        3985         .ascii  " <sp "
      00928C 81               [ 4] 3986         RET
                                   3987         .endif
                                   3988 
                           000001  3989         .ifeq   BOOTSTRAP
                                   3990 ;       .ID     ( na -- )
                                   3991 ;       Display name at address.
                                   3992 
                           000000  3993         .ifne   WORDS_LINKMISC
                                   3994         HEADER  DOTID ".ID"
                                   3995         .endif
      00928D                       3996 DOTID:
      00928D CD 83 37         [ 4] 3997         CALL    QDQBRAN         ; if zero no name
      009290 92 9E                 3998         .dw     DOTI1
      009292 CD 88 49         [ 4] 3999         CALL    COUNT
      00120A                       4000         DoLitC  0x01F
      009295 83               [ 9]    1         TRAP
      009296 00 1F                    2         .dw     0x01F
      009298 CD 84 D4         [ 4] 4001         CALL    ANDD            ; mask lexicon bits
      00929B CC 91 EE         [ 2] 4002         JP      UTYPE
      00929E CD 8A A4         [ 4] 4003 DOTI1:  CALL    DOTQP
      0092A1 09                    4004         .db     9
      0092A2 20 28 6E 6F 4E 61 6D  4005         .ascii  " (noName)"
             65 29
      0092AB 81               [ 4] 4006         RET
                                   4007         .endif
                                   4008 
      0092AC                       4009 DUPPCAT:
      0092AC CD 84 6F         [ 4] 4010         CALL    DUPP
      0092AF CC 83 F5         [ 2] 4011         JP      CAT
                                   4012 
                                   4013 
                                   4014 
                           000000  4015         .ifeq   REMOVE_TNAME
                                   4016 ;       >NAME   ( ca -- na | F )
                                   4017 ;       Convert code address
                                   4018 ;       to a name address.
                                   4019 
                                   4020         HEADER  TNAME ">NAME"
                                   4021 TNAME:
                                   4022         CALL    CNTXT           ; vocabulary link
                                   4023 TNAM2:  CALL    AT
                                   4024         CALL    DUPP            ; ?last word in a vocabulary
                                   4025         CALL    QBRAN
                                   4026         .dw     TNAM4
                                   4027         CALL    DDUP
                                   4028         CALL    NAMET
                                   4029         CALL    XORR            ; compare
                                   4030         CALL    QBRAN
                                   4031         .dw     TNAM3
                                   4032         CALL    CELLM           ; continue with next word
                                   4033         JRA     TNAM2
                                   4034 TNAM3:  JP      NIP
                                   4035 TNAM4:  CALL    DDROP
                                   4036         JP      ZERO
                                   4037         .endif
                                   4038 
                           000001  4039         .ifeq   UNLINKCORE
                                   4040 ;       WORDS   ( -- )
                                   4041 ;       Display names in vocabulary.
                                   4042 
      001227                       4043         HEADER  WORDS "WORDS"
                           000001     1         .ifeq   UNLINK_WORDS
      0092B2 92 64                    2         .dw     LINK
                           001229     3         LINK    = .
      0092B4 05                       4         .db      (102$ - 101$)
      0092B5                          5 101$:
      0092B5 57 4F 52 44 53           6         .ascii  "WORDS"
      0092BA                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0092BA                       4044 WORDS:
      0092BA CD 8D CA         [ 4] 4045         CALL    CR
      0092BD CD 85 06         [ 4] 4046         CALL    CNTXT           ; only in context
      0092C0 CD 83 D9         [ 4] 4047 WORS1:  CALL    AT              ; @ sets Z and N
      0092C3 27 0E            [ 1] 4048         JREQ    1$              ; ?at end of list
      0092C5 CD 84 6F         [ 4] 4049         CALL    DUPP
      0092C8 CD 8A 66         [ 4] 4050         CALL    SPACE
      0092CB CD 92 8D         [ 4] 4051         CALL    DOTID           ; display a name
      0092CE CD 87 96         [ 4] 4052         CALL    CELLM
      0092D1 20 ED            [ 2] 4053         JRA     WORS1
      0092D3 CC 84 56         [ 2] 4054 1$:     JP      DROP
                                   4055         .endif
                                   4056 
                                   4057 
                                   4058 
                                   4059 
                                   4060 ;===============================================================
                                   4061 
                           000001  4062         .ifne   HAS_LED7SEG
                                   4063 
                                   4064 ;       7-seg LED patterns, "70s chique"
      0092D6                       4065 PAT7SM9:
      0092D6 00 40 80 52           4066         .db     0x00, 0x40, 0x80, 0x52 ; , - . / (',' as blank)
      0092DA 3F 06 5B 4F           4067         .db     0x3F, 0x06, 0x5B, 0x4F ; 0,1,2,3
      0092DE 66 6D 7D 07           4068         .db     0x66, 0x6D, 0x7D, 0x07 ; 4,5,6,7
      0092E2 7F 6F                 4069         .db     0x7F, 0x6F             ; 8,9
      0092E4                       4070 PAT7SAZ:
      0092E4 77 7C 39              4071         .db           0x77, 0x7C, 0x39 ;   A,B,C
      0092E7 5E 79 71 3D           4072         .db     0x5E, 0x79, 0x71, 0x3D ; D,E,F,G
      0092EB 74 30 1E 7A           4073         .db     0x74, 0x30, 0x1E, 0x7A ; H,I,J,K
      0092EF 38 55 54 5C           4074         .db     0x38, 0x55, 0x54, 0x5C ; L,M,N,O
      0092F3 73 67 50 6D           4075         .db     0x73, 0x67, 0x50, 0x6D ; P,Q,R,S
      0092F7 78 3E 1C 1D           4076         .db     0x78, 0x3E, 0x1C, 0x1D ; T,U,V,W
      0092FB 76 6E 5B              4077         .db     0x76, 0x6E, 0x5B       ; X,Y,Z
                                   4078 
                                   4079 ;       E7S  ( c -- )
                                   4080 ;       Convert char to 7-seg LED pattern, and insert it in display buffer
                                   4081 
      001273                       4082         HEADER  EMIT7S "E7S"
                           000001     1         .ifeq   UNLINK_EMIT7S
      0092FE 92 B4                    2         .dw     LINK
                           001275     3         LINK    = .
      009300 03                       4         .db      (102$ - 101$)
      009301                          5 101$:
      009301 45 37 53                 6         .ascii  "E7S"
      009304                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009304                       4083 EMIT7S:
      009304 E6 01            [ 1] 4084         LD      A,(1,X)         ; c to A
                                   4085 
      009306 A1 20            [ 1] 4086         CP      A,#' '
      009308 26 0B            [ 1] 4087         JRNE    E7SNOBLK
                                   4088 
                           000000  4089         .if     gt,(HAS_LED7SEG-1)
                                   4090         LD      A,LED7GROUP
                                   4091         JRMI    2$              ; test LED7GROUP.7 "no-tab flag"
                                   4092         INC     A
                                   4093         CP      A,#HAS_LED7SEG
                                   4094         JRULT   1$
                                   4095         CLR     A
                                   4096 1$:     OR      A,#0x80         ; only one tab action, set "no-tab flag"
                                   4097         LD      LED7GROUP,A
                                   4098 
                                   4099 2$:     CALLR   XLEDGROUP
                                   4100         EXGW    X,Y             ; restore X/Y after XLEDGROUP
                           000001  4101         .else
      00930A 90 AE 00 42      [ 2] 4102         LDW     Y,#LED7FIRST    ; DROP DOLIT LED7FIRST
                                   4103         .endif
      00930E FF               [ 2] 4104         LDW     (X),Y
      001284                       4105         DoLitC  LEN_7SGROUP
      00930F 83               [ 9]    1         TRAP
      009310 00 03                    2         .dw     LEN_7SGROUP
      009312 CC 88 D4         [ 2] 4106         JP      ERASE
                                   4107 
      009315                       4108 E7SNOBLK:
                                   4109 
                           000000  4110         .if     gt,(HAS_LED7SEG-1)
                                   4111         CP      A,#LF           ; test for c ~ /[<CR><LF>]/
                                   4112         JRNE    E7SNOLF
                                   4113         MOV     LED7GROUP,#0x80 ; go to first LED group, set "no-tab flag"
                                   4114         JRA     E7END
                                   4115         .endif
                                   4116 
      009315                       4117 E7SNOLF:
                           000000  4118         .if     gt,(HAS_LED7SEG-1)
                                   4119         BRES    LED7GROUP,#7    ; on char output: clear "no-tab flag"
                                   4120         .endif
                                   4121 
      009315 A1 2E            [ 1] 4122         CP      A,#'.'
      009317 27 27            [ 1] 4123         JREQ    E7DOT
      009319 A1 2C            [ 1] 4124         CP      A,#','
      00931B 2B 29            [ 1] 4125         JRMI    E7END
      00931D A1 7A            [ 1] 4126         CP      A,#'z'
      00931F 2A 25            [ 1] 4127         JRPL    E7END
      009321 A1 41            [ 1] 4128         CP      A,#'A'
      009323 24 09            [ 1] 4129         JRUGE   E7ALPH
                                   4130 
                                   4131         ; '-'--'9' (and '@')
      009325 A0 2C            [ 1] 4132         SUB     A,#','
      009327 E7 01            [ 1] 4133         LD      (1,X),A
      00129E                       4134         DoLitW  PAT7SM9
      009329 83               [ 9]    1         TRAP
      00932A 92 D6                    2         .dw     PAT7SM9
      00932C 20 09            [ 2] 4135         JRA     E7LOOKA
      00932E                       4136 E7ALPH:
                                   4137         ; 'A'--'z'
      00932E A4 5F            [ 1] 4138         AND     A,#0x5F         ; convert to uppercase
      009330 A0 41            [ 1] 4139         SUB     A,#'A'
      009332 E7 01            [ 1] 4140         LD      (1,X),A
      0012A9                       4141         DoLitW  PAT7SAZ
      009334 83               [ 9]    1         TRAP
      009335 92 E4                    2         .dw     PAT7SAZ
      009337                       4142 E7LOOKA:
      009337 CD 84 B0         [ 4] 4143         CALL    PLUS
      00933A CD 83 F5         [ 4] 4144         CALL    CAT
      00933D CC 93 4F         [ 2] 4145         JP      PUT7S
                                   4146 
      009340                       4147 E7DOT:
                           000000  4148         .if     gt,(HAS_LED7SEG-1)
                                   4149         CALL    XLEDGROUP
                                   4150         LD      A,((LEN_7SGROUP-1),X)
                                   4151         OR      A,#0x80
                                   4152         LD      ((LEN_7SGROUP-1),X),A
                                   4153         EXGW    X,Y             ; restore X/Y after XLEDGROUP
                                   4154         ; fall trough
                                   4155 
                           000001  4156         .else
      009340 A6 80            [ 1] 4157         LD      A,#0x80         ; 7-seg P (dot)
      009342 BA 44            [ 1] 4158         OR      A,LED7LAST
      009344 B7 44            [ 1] 4159         LD      LED7LAST,A
                                   4160         .endif
                                   4161         ; fall trough
                                   4162 
      009346                       4163 E7END:
      009346 CC 84 56         [ 2] 4164         JP      DROP
                                   4165 
                           000000  4166         .if     gt,(HAS_LED7SEG-1)
                                   4167 ;       Helper routine for calculating LED group start adress
                                   4168 ;       return: X: LED group addr, Y: DSP, A: LEN_7SGROUP
                                   4169 ;       caution: caller must restore X/Y!
                                   4170 XLEDGROUP:
                                   4171         EXGW    X,Y             ; use X to save memory
                                   4172         LD      A,LED7GROUP
                                   4173         AND     A,#0x7F         ; ignore "no-tab flag"
                                   4174         LD      XL,A
                                   4175         LD      A,#LEN_7SGROUP
                                   4176         MUL     X,A
                                   4177         ADDW    X,#LED7FIRST
                                   4178         RET
                                   4179         .endif
                                   4180 
                                   4181 ;       P7S  ( c -- )
                                   4182 ;       Right aligned 7S-LED pattern output, rotates LED group buffer
                                   4183 
      0012BE                       4184         HEADER  PUT7S "P7S"
                           000001     1         .ifeq   UNLINK_PUT7S
      009349 93 00                    2         .dw     LINK
                           0012C0     3         LINK    = .
      00934B 03                       4         .db      (102$ - 101$)
      00934C                          5 101$:
      00934C 50 37 53                 6         .ascii  "P7S"
      00934F                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00934F                       4185 PUT7S:
                           000000  4186         .if     gt,(HAS_LED7SEG-1)
                                   4187         CALLR   XLEDGROUP
                                   4188         DEC     A
                                   4189         PUSH    A
                                   4190 1$:     LD      A,(1,X)
                                   4191         LD      (X),A
                                   4192         INCW    X
                                   4193         DEC     (1,SP)
                                   4194         JRNE    1$
                                   4195         POP     A
                                   4196 
                                   4197         EXGW    X,Y             ; restore X/Y after XLEDGROUP
                                   4198         CALL    AFLAGS
                                   4199         LD      (Y),A
                           000001  4200         .else
      0012C4                       4201         DoLitC  LED7FIRST+1
      00934F 83               [ 9]    1         TRAP
      009350 00 43                    2         .dw     LED7FIRST+1
      0012C7                       4202         DoLitC  LED7FIRST
      009352 83               [ 9]    1         TRAP
      009353 00 42                    2         .dw     LED7FIRST
      0012CA                       4203         DoLitC  (LEN_7SGROUP-1)
      009355 83               [ 9]    1         TRAP
      009356 00 02                    2         .dw     (LEN_7SGROUP-1)
      009358 CD 88 87         [ 4] 4204         CALL    CMOVE
      00935B CD 8B 26         [ 4] 4205         CALL    AFLAGS
      00935E B7 44            [ 1] 4206         LD      LED7LAST,A
                                   4207         .endif
                                   4208 
      009360 81               [ 4] 4209         RET
                                   4210 
                                   4211         .endif
                                   4212 
                                   4213 ;===============================================================
                                   4214 
                           000001  4215         .ifne   HAS_KEYS
                                   4216 
                                   4217 ;       ?KEYB   ( -- c T | F )  ( TOS STM8: -- Y,Z,N )
                                   4218 ;       Return keyboard char and true, or false if no key pressed.
                                   4219 
      0012D6                       4220         HEADER  QKEYB "?KEYB"
                           000001     1         .ifeq   UNLINK_QKEYB
      009361 93 4B                    2         .dw     LINK
                           0012D8     3         LINK    = .
      009363 05                       4         .db      (102$ - 101$)
      009364                          5 101$:
      009364 3F 4B 45 59 42           6         .ascii  "?KEYB"
      009369                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009369                       4221 QKEYB:
      009369 CD 82 17         [ 4] 4222         CALL    BKEYCHAR        ; Read char from keyboard (option: vectored code)
      00936C CD 8B 26         [ 4] 4223         CALL    AFLAGS
                                   4224 
      00936F 26 06            [ 1] 4225         JRNE    KEYBPRESS
                                   4226         ; Bit7: flag press + 100*5ms hold before repetition
      009371 35 E4 00 49      [ 1] 4227         MOV     KEYREPET,#(0x80 + 100)
      009375 20 16            [ 2] 4228         JRA     NOKEYB
      009377                       4229 KEYBPRESS:
      009377 72 0F 00 49 06   [ 2] 4230         BTJF    KEYREPET,#7,KEYBHOLD
      00937C 72 1F 00 49      [ 1] 4231         BRES    KEYREPET,#7
      009380 20 08            [ 2] 4232         JRA     ATOKEYB
      009382                       4233 KEYBHOLD:
      009382 3A 49            [ 1] 4234         DEC     KEYREPET
      009384 26 07            [ 1] 4235         JRNE    NOKEYB
      009386 35 1E 00 49      [ 1] 4236         MOV     KEYREPET,#30    ; repetition time: n*5ms
      00938A                       4237 ATOKEYB:
      00938A CC 85 4A         [ 2] 4238         JP      ATOKEY          ; push char and flag true
      00938D                       4239 NOKEYB:
      00938D CC 85 66         [ 2] 4240         JP      ZERO            ; push flag false
                                   4241 
                                   4242         .endif
                                   4243 
                                   4244 ;===============================================================
                                   4245 
                           000001  4246         .ifne   HAS_ADC
                                   4247 ;       ADC!  ( c -- )
                                   4248 ;       Init ADC, select channel for conversion
                                   4249 
      001305                       4250         HEADER  ADCSTOR "ADC!"
                           000001     1         .ifeq   UNLINK_ADCSTOR
      009390 93 63                    2         .dw     LINK
                           001307     3         LINK    = .
      009392 04                       4         .db      (102$ - 101$)
      009393                          5 101$:
      009393 41 44 43 21              6         .ascii  "ADC!"
      009397                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009397                       4251 ADCSTOR:
      009397 5C               [ 1] 4252         INCW    X
      009398 F6               [ 1] 4253         LD      A,(X)
      009399 5C               [ 1] 4254         INCW    X
      00939A A4 0F            [ 1] 4255         AND     A,#0x0F
      00939C C7 54 00         [ 1] 4256         LD      ADC_CSR,A       ; select channel
      00939F 72 16 54 02      [ 1] 4257         BSET    ADC_CR2,#3      ; align ADC to LSB
      0093A3 72 10 54 01      [ 1] 4258         BSET    ADC_CR1,#0      ; enable ADC
      0093A7 81               [ 4] 4259         RET
                                   4260 
                                   4261 ;       ADC@  ( -- w )
                                   4262 ;       start ADC conversion, read result
                                   4263 
      00131D                       4264         HEADER  ADCAT "ADC@"
                           000001     1         .ifeq   UNLINK_ADCAT
      0093A8 93 92                    2         .dw     LINK
                           00131F     3         LINK    = .
      0093AA 04                       4         .db      (102$ - 101$)
      0093AB                          5 101$:
      0093AB 41 44 43 40              6         .ascii  "ADC@"
      0093AF                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0093AF                       4265 ADCAT:
      0093AF 72 1F 54 00      [ 1] 4266         BRES    ADC_CSR,#7      ; reset EOC
      0093B3 72 10 54 01      [ 1] 4267         BSET    ADC_CR1,#0      ; start ADC
      0093B7 72 0F 54 00 FB   [ 2] 4268 1$:     BTJF    ADC_CSR,#7,1$   ; wait until EOC
      0093BC 90 CE 54 04      [ 2] 4269         LDW     Y,ADC_DRH       ; read ADC
      0093C0 CC 84 28         [ 2] 4270         JP      YSTOR
                                   4271         .endif
                                   4272 
                                   4273 ;===============================================================
                           000000  4274         .ifne   WORDS_EXTRASTACK
                                   4275 
                                   4276 ;       SP!     ( a -- )
                                   4277 ;       Set data stack pointer.
                                   4278 
                                   4279         HEADER  SPSTO "sp!"
                                   4280 SPSTO:
                                   4281         LDW     X,(X)   ;X = a
                                   4282         RET
                                   4283 
                                   4284 ;       SP@     ( -- a )        ( TOS STM8: -- Y,Z,N )
                                   4285 ;       Push current stack pointer.
                                   4286 
                                   4287         HEADER  SPAT "sp@"
                                   4288 SPAT:
                                   4289         LDW     Y,X
                                   4290         JP      YSTOR
                                   4291 
                                   4292 ;       RP@     ( -- a )     ( TOS STM8: -- Y,Z,N )
                                   4293 ;       Push current RP to data stack.
                                   4294 
                                   4295         HEADER  RPAT "rp@"
                                   4296 RPAT:
                                   4297         LDW     Y,SP            ; save return addr
                                   4298         JP      YSTOR
                                   4299 
                                   4300 ;       RP!     ( a -- )
                                   4301 ;       Set return stack pointer.
                                   4302 
                                   4303         HEADFLG RPSTO "rp!" COMPO
                                   4304 RPSTO:
                                   4305         POPW    Y
                                   4306         LDW     YTEMP,Y
                                   4307         LDW     Y,X
                                   4308         INCW    X               ; fixed error: TOS not consumed
                                   4309         INCW    X
                                   4310         LDW     Y,(Y)
                                   4311         LDW     SP,Y
                                   4312         JP      [YTEMP]
                                   4313 
                                   4314         .endif
                                   4315 
                                   4316 ;===============================================================
                                   4317 
                           000001  4318         .ifne   WORDS_EXTRAEEPR
                                   4319 ;       ULOCK  ( -- )
                                   4320 ;       Unlock EEPROM (STM8S)
                                   4321 
      001338                       4322         HEADER  ULOCK "ULOCK"
                           000001     1         .ifeq   UNLINK_ULOCK
      0093C3 93 AA                    2         .dw     LINK
                           00133A     3         LINK    = .
      0093C5 05                       4         .db      (102$ - 101$)
      0093C6                          5 101$:
      0093C6 55 4C 4F 43 4B           6         .ascii  "ULOCK"
      0093CB                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0093CB                       4323 ULOCK:
      0093CB 35 AE 50 64      [ 1] 4324         MOV     FLASH_DUKR,#0xAE
      0093CF 35 56 50 64      [ 1] 4325         MOV     FLASH_DUKR,#0x56
      0093D3 72 07 50 5F FB   [ 2] 4326 1$:     BTJF    FLASH_IAPSR,#3,1$    ; PM0051 4.1 requires polling bit3=1 before writing
      0093D8 81               [ 4] 4327         RET
                                   4328 
                                   4329 
                                   4330 ;       LOCK  ( -- )
                                   4331 ;       Lock EEPROM (STM8S)
                                   4332 
      00134E                       4333         HEADER  LOCK "LOCK"
                           000001     1         .ifeq   UNLINK_LOCK
      0093D9 93 C5                    2         .dw     LINK
                           001350     3         LINK    = .
      0093DB 04                       4         .db      (102$ - 101$)
      0093DC                          5 101$:
      0093DC 4C 4F 43 4B              6         .ascii  "LOCK"
      0093E0                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      0093E0                       4334 LOCK:
      0093E0 72 17 50 5F      [ 1] 4335         BRES    FLASH_IAPSR,#3
      0093E4 81               [ 4] 4336         RET
                                   4337         .endif
                                   4338 
                                   4339 
                           000001  4340         .ifne   (HAS_CPNVM + WORDS_EXTRAEEPR)
                                   4341 ;       ULOCKF  ( -- )
                                   4342 ;       Unlock Flash (STM8S)
                                   4343 
                           000001  4344         .ifne   WORDS_EXTRAEEPR
      0093E5                       4345         HEADER  UNLOCK_FLASH "ULOCKF"
                           000000     1         .ifeq   UNLINK_UNLOCK_FLASH
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "ULOCKF"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   4346         .endif
      00135A                       4347 UNLOCK_FLASH:
      0093E5 35 56 50 62      [ 1] 4348         MOV     FLASH_PUKR,#0x56
      0093E9 35 AE 50 62      [ 1] 4349         MOV     FLASH_PUKR,#0xAE
      0093ED 72 03 50 5F FB   [ 2] 4350 1$:     BTJF    FLASH_IAPSR,#1,1$    ; PM0051 4.1 requires polling bit1=1 before writing
      0093F2 81               [ 4] 4351         RET
                                   4352 
                                   4353 
                                   4354 ;       LOCKF  ( -- )
                                   4355 ;       Lock Flash (STM8S)
                                   4356 
                           000001  4357         .ifne   WORDS_EXTRAEEPR
      0093F3                       4358         HEADER  LOCK_FLASH "LOCKF"
                           000000     1         .ifeq   UNLINK_LOCK_FLASH
                                      2         .dw     LINK
                                      3         LINK    = .
                                      4         .db      (102$ - 101$)
                                      5 101$:
                                      6         .ascii  "LOCKF"
                                      7 102$:
                                      8         .endif
                                      9 ;'Label:
                                   4359         .endif
      001368                       4360 LOCK_FLASH:
      0093F3 72 13 50 5F      [ 1] 4361         BRES    FLASH_IAPSR,#1
      0093F7 81               [ 4] 4362         RET
                                   4363         .endif
                                   4364 
                           000001  4365         .ifne  HAS_CPNVM
                                   4366 
                                   4367 ;       Test if CP points doesn't point to RAM
      0093F8                       4368 NVMQ:
      0093F8 B6 6A            [ 1] 4369         LD      A,USRCP
      0093FA A4 F8            [ 1] 4370         AND     A,#0xF8
      0093FC 81               [ 4] 4371         RET
                                   4372 
                                   4373 
                                   4374 ;       Helper routine: swap USRCP and NVMCP
      0093FD                       4375 SWAPCP:
      0093FD BE 6A            [ 2] 4376         LDW     X,USRCP
      0093FF 45 6E 6A         [ 1] 4377         MOV     USRCP,NVMCP
      009402 45 6F 6B         [ 1] 4378         MOV     USRCP+1,NVMCP+1
      009405 BF 6E            [ 2] 4379         LDW     NVMCP,X
      009407 51               [ 1] 4380         EXGW    X,Y
      009408 81               [ 4] 4381         RET
                                   4382 
                                   4383 ;       NVM  ( -- )
                                   4384 ;       Compile to NVM (enter mode NVM)
                                   4385 
      00137E                       4386         HEADER  NVMM "NVM"
                           000001     1         .ifeq   UNLINK_NVMM
      009409 93 DB                    2         .dw     LINK
                           001380     3         LINK    = .
      00940B 03                       4         .db      (102$ - 101$)
      00940C                          5 101$:
      00940C 4E 56 4D                 6         .ascii  "NVM"
      00940F                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      00940F                       4387 NVMM:
      00940F AD E7            [ 4] 4388         CALLR    NVMQ
      009411 26 09            [ 1] 4389         JRNE    1$           ; state entry action?
                                   4390         ; in NVM mode only link words in NVM
      009413 51               [ 1] 4391         EXGW    X,Y
      009414 BE 74            [ 2] 4392         LDW     X,NVMCONTEXT
      009416 BF 6C            [ 2] 4393         LDW     USRLAST,X
      009418 AD E3            [ 4] 4394         CALLR   SWAPCP
      00941A AD C9            [ 4] 4395         CALLR   UNLOCK_FLASH
      00941C                       4396 1$:
      00941C 81               [ 4] 4397         RET
                                   4398 
                                   4399 
                                   4400 ;       RAM  ( -- )
                                   4401 ;       Compile to RAM (enter mode RAM)
                                   4402 
      001392                       4403         HEADER  RAMM "RAM"
                           000001     1         .ifeq   UNLINK_RAMM
      00941D 94 0B                    2         .dw     LINK
                           001394     3         LINK    = .
      00941F 03                       4         .db      (102$ - 101$)
      009420                          5 101$:
      009420 52 41 4D                 6         .ascii  "RAM"
      009423                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009423                       4404 RAMM:
      009423 AD D3            [ 4] 4405         CALLR   NVMQ
      009425 27 18            [ 1] 4406         JREQ    1$
                                   4407 
      009427 51               [ 1] 4408         EXGW    X,Y
      009428 BE 72            [ 2] 4409         LDW     X,USRVAR
      00942A CF 82 37         [ 2] 4410         LDW     COLDCTOP,X
      00942D BE 6A            [ 2] 4411         LDW     X,USRCP
      00942F CF 82 3B         [ 2] 4412         LDW     COLDNVMCP,X
      009432 BE 74            [ 2] 4413         LDW     X,NVMCONTEXT
      009434 CF 82 39         [ 2] 4414         LDW     COLDCONTEXT,X
      009437 BE 76            [ 2] 4415         LDW     X,USRCONTEXT
      009439 BF 6C            [ 2] 4416         LDW     USRLAST,X
      00943B AD C0            [ 4] 4417         CALLR   SWAPCP          ; Switch back to mode RAM
      00943D AD B4            [ 4] 4418         CALLR   LOCK_FLASH
      00943F                       4419 1$:
      00943F 81               [ 4] 4420         RET
                                   4421 
                                   4422 
                                   4423 ;       RESET  ( -- )
                                   4424 ;       Reset Flash dictionary and 'BOOT to defaults and restart
                                   4425 
      0013B5                       4426         HEADER  RESETT "RESET"
                           000001     1         .ifeq   UNLINK_RESETT
      009440 94 1F                    2         .dw     LINK
                           0013B7     3         LINK    = .
      009442 05                       4         .db      (102$ - 101$)
      009443                          5 101$:
      009443 52 45 53 45 54           6         .ascii  "RESET"
      009448                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009448                       4427 RESETT:
      009448 AD 9B            [ 4] 4428         CALLR   UNLOCK_FLASH
      0013BF                       4429         DoLitW  UDEFAULTS
      00944A 83               [ 9]    1         TRAP
      00944B 82 3D                    2         .dw     UDEFAULTS
      0013C2                       4430         DoLitW  UBOOT
      00944D 83               [ 9]    1         TRAP
      00944E 82 2B                    2         .dw     UBOOT
      0013C5                       4431         DoLitC  (ULAST-UBOOT)
      009450 83               [ 9]    1         TRAP
      009451 00 12                    2         .dw     (ULAST-UBOOT)
      009453 CD 88 87         [ 4] 4432         CALL    CMOVE           ; initialize user area
      009456 AD 9B            [ 4] 4433         CALLR   LOCK_FLASH
      009458 CC 80 EF         [ 2] 4434         JP      COLD
                                   4435 
                                   4436 
                                   4437 ;       SAVEC ( -- )
                                   4438 ;       Minimal context switch for low level interrupt code
                                   4439 ;       This should be the first word called in the interrupt handler
                                   4440 
      0013D0                       4441         HEADER  SAVEC "SAVEC"
                           000001     1         .ifeq   UNLINK_SAVEC
      00945B 94 42                    2         .dw     LINK
                           0013D2     3         LINK    = .
      00945D 05                       4         .db      (102$ - 101$)
      00945E                          5 101$:
      00945E 53 41 56 45 43           6         .ascii  "SAVEC"
      009463                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009463                       4442 SAVEC:
      009463 90 85            [ 2] 4443         POPW    Y
      009465 BE 7E            [ 2] 4444         LDW     X,YTEMP
      009467 89               [ 2] 4445         PUSHW   X
      009468 AE 03 30         [ 2] 4446         LDW     X,#(ISPP)       ; init data stack for interrupt ISPP
      00946B 90 FC            [ 2] 4447         JP      (Y)
                                   4448 
                                   4449 
                                   4450 ;       IRET ( -- )
                                   4451 ;       Restore context and return from low level interrupt code
                                   4452 ;       This should be the last word called in the interrupt handler
                                   4453 
      0013E2                       4454         HEADER  RESTC "IRET"
                           000001     1         .ifeq   UNLINK_RESTC
      00946D 94 5D                    2         .dw     LINK
                           0013E4     3         LINK    = .
      00946F 04                       4         .db      (102$ - 101$)
      009470                          5 101$:
      009470 49 52 45 54              6         .ascii  "IRET"
      009474                          7 102$:
                                      8         .endif
                                      9 ;'Label:
      009474                       4455 RESTC:
      009474 85               [ 2] 4456         POPW    X               ; discard CALL return address
      009475 85               [ 2] 4457         POPW    X
      009476 BF 7E            [ 2] 4458         LDW     YTEMP,X         ; restore context
      009478 80               [11] 4459         IRET                    ; resturn from interrupt
                                   4460 
                                   4461         .endif
                                   4462 
                                   4463 ;===============================================================
                           0013E4  4464         LASTN   =       LINK    ;last name defined
                                   4465 
                                   4466         .area CODE
                                   4467         .area INITIALIZER
                           000000  4468         END_SDCC_FLASH = .
                                   4469         .area CABS (ABS)
